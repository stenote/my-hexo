title: Mysql修改现有表相关信息
tags:
- MySQL
date: "1999-11-30 16:00:00"
---

<div>修改现有表名称使用rename</div>
<div>
</div>
<div>alter table foo rename bar;</div>
<div>alter table old_table_name rename new_table_name;</div>
<div>
</div>
<div>alter用于对表进行修改。</div>
<div>常用的alter配合使用的还有</div>
<div>add</div>
<div>drop</div>
<div>change</div>
<div>modify</div>
<div>
</div>
<div>change可直接对某列进行名称的修改</div>
<div>alter table table_name change User user varchar(50) not null;</div>
<div>alter table table_name change column User user varchar(50) not null;</div>
<div>
</div>
<div>modify是针对某列进行修改，不可修改名称</div>
<div>alter table table_name modify User varchar(50) not null;</div>
<div>alter table table_name modify column User varchar(50) not null;</div>
<div>
</div>
<div>drop用于删除某列</div>
<div>alter table table_name drop User;</div>
<div>alter table table_name drop column User;</div>
<div>column可加可不加。</div>
<div>
</div>
<div>add用于增加某列</div>
<div>alter table table_name add column foobar varchar(50) not null;</div>
<div>alter table table_name add foobar varchar(50) not null;</div>
<div>
</div>
<div>add增加after，设定增加行位置</div>
<div>alter table table_name add column foo varchar(50) not null after bar;</div>
<div>alter table table_name add foo varchar(50) not null after bar;</div>
<div>
</div>
<div>
</div>
<div>
</div>
