title: Git rev-parse
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>git rev-parse 通常是一个用来获取commit-id的命令</div>
<div>
</div>
<div>git rev-parse HEAD</div>
<div>
</div>
<div>
</div>
<div>git rev-parse master </div>
<div>git rev-parse refs/heads/master</div>
<div>
</div>
<div>git rev-parse 6652</div>
<div>6652为一个commit-id</div>
<div>
</div>
<div>A为一个tag，tag指向一个commit</div>
<div>git rev-parse A</div>
<div>git rev-parse refs/tags/A</div>
<div>获取tag的commit-id</div>
<div>
</div>
<div>git rev-parse A^</div>
<div>git rev-parse A^0</div>
<div>Atag指向的commit-id</div>
<div>
</div>
<div>git rev-parse A^1</div>
<div>Atag指向的commit-id的第一个parent提交</div>
<div>git rev-parse A^2</div>
<div>Atag指向的commit-id的第二个parent提交    </div>
<div>git rev-parse B^0</div>
