title: Git 回到历史
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>假如我们在某个库上开发，开发到1这个时候我就继续开发，直到2这个状态，我们想要回到1这个状态，可以使用git reset 和 git branch等相关命令进行相关操作。达到git 回到历史的做法</div>
<div>
</div>
<div>首先，我们假设当前开发的分支为develop分支</div>
<div>首先对当前分支进行一个备份</div>
<div>$ git branch bak_develop</div>
<div>$ git checkout bak_develop</div>
<div>$ git merge develop</div>
<div>
</div>
<div>完成对本地的备份</div>
<div>然后我们把本地备份的在远程服务器进行一次备份</div>
<div>$ git push origin bak_develop</div>
<div>
</div>
<div>在本地执行 git reset 将当前develop的开发状态reset到1这个状态时(可通过git log 查看相关commit-id)</div>
<div>$ git reset   11df052c52 //这个id实际使用中不同的</div>
<div>
</div>
<div>$ 删除远程的develop 分支</div>
<div>$ git push origin :develop</div>
<div>
</div>
<div>将现有的develop分支push到origin上的develop分支</div>
<div>$ git push origin develop:develop</div>
<div>
</div>
<div>所有人都删除自己的develop分支</div>
<div>$ git checkout master</div>
<div>$ git brand -D develop</div>
<div>
</div>
<div>做操作的主端进行如下操作</div>
<div>$ git push origin develop:develop</div>
<div>$ git config branch.develop.remote origin</div>
<div>$ git config branch.develop.merge refs/heads/develop</div>
<div>
</div>
<div>其他所有用户重新</div>
<div>$ git pull</div>
<div>$ git checkout develop</div>
<div>即可完成对一个分支的回到历史操作。</div>
<div>这个是比较笨的一种做法需要所有人都进行操作</div>
<div>
</div>
