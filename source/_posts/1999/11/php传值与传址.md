title: php传值传址
tags:
- PHP
date: "1999-11-30 16:00:00"
---

<div>每个变量都有一个地址</div>
<div>正常情况下，变量赋值</div>
<div>$a = 1;</div>
<div>我们可以理解为，将1这个数值存储到了$a的地址中去</div>
<div>$a = 2;</div>
<div>我们可以理解为，讲$a的地址内的数据给删除了，然后存储进了2</div>
<div>$b = $a;</div>
<div>我们可以理解为，创建一个新的地址给$b, 然后把$a内的数值放到$b；这个时候$b的数值是2</div>
<div>$b = 3;</div>
<div>将$b的地址内的值修改为3，但是$a不会修改，他们之间没有关系</div>
<div>
</div>
<div>$c = &amp;$a ，讲$c 和$a 的数值存储地址关联，</div>
<div>修改$c的同时也修改$a;</div>
<div>$c = 6;</div>
<div>echo $a, 这个时候$a也就是6了；但是$b还是3；</div>
<div>
</div>
<div>函数传值function test($a) {</div>
<div>$a ++;</div>
<div>}</div>
<div>在外面，$a依然是原来的值</div>
<div>
</div>
<div>函数传址 function test(&amp;$a) {</div>
<div>$a ++;</div>
<div>} </div>
<div>在外面,$a 加1 了；</div>
<div>
</div>
<div>函数返回值</div>
<div>假设$a = 1;</div>
<div>
</div>
<div>$b = function($a) {return $a+1};//$b = 2;</div>
<div>$b = 5;</div>
<div>$a没变化</div>
<div>
</div>
<div>$b = gaga($a);</div>
<div>function &amp;gaga(&amp;$a) {</div>
<div>    $a++;</div>
<div>    return $a;</div>
<div>}</div>
<div>//这时候$b = 12 $a 是2</div>
<div>$b = 5; <a>//给$b赋值，$a依然是2.</a></div>
<div>
</div>
<div>
</div>
<div>$a = 1;</div>
<div>$b = &amp;gaga($a);</div>
<div>function &amp;gaga($a) {</div>
<div>return $a+1;</div>
<div>}</div>
<div>$b = 2;</div>
<div>$a 依然是1；</div>
<div>
</div>
<div>
</div>
<div>$a = 1;</div>
<div>$b = &amp;gaga($a);</div>
<div>function &amp;gaga(&amp;$a) {</div>
<div>$a ++;</div>
<div>return $a;</div>
<div>}</div>
<div>
</div>
<div>$b = $a = 1;</div>
<div>$b = 3;</div>
<div>echo $a;</div>
<div><a>//$a</a> = 3;</div>
