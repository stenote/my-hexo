title: Git init
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>$ git init 可对当前目录进行git初始化设定，初始化设定时，会在当前目录创建.git目录。用户git管理</div>
<div>初始化后的repository没有branch，需要进行一次add 、commit后才会创建master分支。</div>
<div>
</div>
<div>$ git branch </div>
<div>$ touch index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'init'</div>
<div>$ git branch </div>
<div>* master</div>
<div>
</div>
<div>初始化后</div>
<div>建议设置git相关属性，详见git config</div>
<div>
</div>
<div>
</div>
