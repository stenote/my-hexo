title: 多remote协同开发
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>假设现在有3个人，其中两两可见，不可能三个人都在一起。如何进行同时开发？</div>
<div>假设这三个人是A、B、C</div>
<div>
</div>
<div>首先，在A、C上创建两个repository</div>
<div>A：</div>
<div>$ mkdir reposiory1</div>
<div>$ cd repository1</div>
<div>$ git init</div>
<div>$ touch index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'init'</div>
<div>$ git checkout -b branch_a</div>
<div>$ git merge master</div>
<div>$ git config receive.denyCurrentBranch ignore //设置其他用户可以pull和push</div>
<div>
</div>
<div>C:</div>
<div>$ mkdir repository2</div>
<div>$ cd repository2</div>
<div>$ git init</div>
<div>$ touch index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'init'</div>
<div>$ git checkout -b branch_c</div>
<div>$ git merge master</div>
<div>$ git config receive.denyCurrentBanch ignore //设置其他用户可以pull和push</div>
<div>
</div>
<div>B用户的操作</div>
<div>$ gti clone username@hostofa.com:/path/to/repository1 repository //clone A的repository1到本地repository</div>
<div>$ cd repository</div>
<div>$ git checkout branch_a //切换到branch_a ，使B本地repository有branch_a</div>
<div>$ git checkout -b branch_c //创建branch_c，使B本地repository有branch_c</div>
<div>修改.git/config</div>
<div>[remote &quot;A&quot;]</div>
<div>fetch = +refs/heads/branch_a:refs/remotes/A/branch_a</div>
<div>url = user@hostofa.com:/path/to/repository1/</div>
<div>[remote &quot;C&quot;]</div>
<div>fetch = +refs/heads/branch_c:refs/remotes/C/branch_c</div>
<div>url = user@hostofc.com:/path/to/repository2/</div>
<div>[branch 'branch_a&quot;]</div>
<div>remote = A</div>
<div>merge = refs/heads/branch_a</div>
<div>[branch &quot;branch_c&quot;]</div>
<div>remote =C</div>
<div>merge = refs/heads/branch_c</div>
<div>
</div>
<div>设定完成</div>
<div>A和C可以在本地重新开发clone自己的库，然后自行开发，需要开发的代码push到自己所在的库上自己的branch_a/c上去。</div>
<div>B的操作相对比较繁琐</div>
<div>B如果需要抓取C的代码，需要做的是</div>
<div>$ git checkout branch_c</div>
<div>$ git pull C branch_c</div>
<div>
</div>
<div>B同样可获取A的代码</div>
<div>$ git checkout branch_a</div>
<div>$ git pull A branch_c</div>
<div>
</div>
<div>获取后A和C的代码都在不同分支，可以使用merge进行代码合并</div>
<div>假设当前在branch_a</div>
<div>$ git branch</div>
<div>* branch_a</div>
<div>   branch_c</div>
<div>   master</div>
<div>
</div>
<div>$ git merge branch_c</div>
<div>如果出现冲突，手动修复后commit</div>
<div>$ git push A branch_a:branch_a   //将 branch_a push到A的branch_a分支</div>
<div>$ git checkout branch_c</div>
<div>$ git merge branch_a</div>
<div>$ git push C branch_c:branch_c //merge branch_a的代码，然后将branch_c push到C的branch_c分支，达到A、C代码交互。</div>
<div>
</div>
<div>当然B在master分支可也开发自己的代码，也可正常在其他分支merge master的内容，使B的代码分支branch_a  branch_c不要偏离太远，适度merge并push，才可维持这种情况。</div>
<div>
</div>
<div>其实就是想在此总结一下branch remote的要点。</div>
<div>
</div>
