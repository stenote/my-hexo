title: 过去历史commit 剔除命令
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>有些时候，我们需要对过去的历史commit进行一次删除。我们通常的想法是，reset到需要剔除的前一个commit后，再重新进行开发。但是这样的结果是之后的提交不会重新链接到现有的master分支上。其实git中已经提供了一个对过去分支进行删除的命令。应该说不是对过去的分支进行删除的命令，而是多个命令的组合使用，来达到对过去commit删除</div>
<div>1、创建4个commit，从早到晚，分别为commit打上标签为A、B、C、D</div>
<div>2、假设我们需要从中删除B这个tag对应的commit，先直接checkout到A上 git checkout A，进入断头模式</div>
<div>3、使用git cherry-pick commit-id来补充C、D两个commit</div>
<div>4、记录当下的 commit-id ,git log --oneline -1</div>
<div>5、切换到 master分支， 离开断头模式 ，git checkout master</div>
<div>6、git reset --hard 最后记录的commit-id，重置master到最后的commit-id</div>
<div>7、查看git log --oneline</div>
<div>以上操作安成了对B这个commit的删除</div>
<div>
</div>
<div>我们回复到A、B、C、D都存在时的状态</div>
<div>1、git reflog show master，查看一下master分支的log，可以找到之前updating前的commit-id</div>
<div>2、git reset --hard commit-id恢复之前状态</div>
<div>3、git rebase --onto A B D，从A开始，删除B 最后到D</div>
<div>4、记录当前commit-id，然后在master分支上reset到当前commit-id即可</div>
<div>
</div>
<div>1、git reflog show master，查看master分支log，查看并记录之前 updating前的commit-id</div>
<div>2、git checkout A，进入断头模式</div>
<div>3、git rebase --onto HEAD B master</div>
<div>从HEAD开始，删除B，链接到master</div>
<div>
</div>
<div>1、回复之前状态</div>
<div>2、git rebase -i A</div>
<div>3、删除B提交一行后保存 </div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
