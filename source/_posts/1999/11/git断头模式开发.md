title: git submodule 断头模式开发
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>主库main、submodule</div>
<div>clone后的库</div>
<div>main-&gt;main</div>
<div>submodule-&gt;submodule</div>
<div>foobar-&gt;main</div>
<div>
</div>
<div>进入clone后的main库</div>
<div>$ git submodule add /path/to/submodule </div>
<div>$ git submodule init</div>
<div>$ git submodule update</div>
<div>$ cd submodule </div>
<div>$ git log</div>
<div>//记录下最后的log的commit-id</div>
<div>$ git checkout 1325aec23cbde3da629cd01f2ab74a342d4a6bdb</div>
<div>//设定submodule进入断头模式</div>
<div>$ cd ../</div>
<div>$ git submodule init</div>
<div>$ git add ./</div>
<div>$ git commit -m 'add submodule'</div>
<div>$ git push</div>
<div>
</div>
<div>进入foobar</div>
<div>$ cd /path/to/foobar</div>
<div>$ git pull</div>
<div>//提示有submodule的修改</div>
<div>$ git submodule init</div>
<div>$ git submodule update即可同步submodule的文件了</div>
<div>
</div>
<div>更新submodule</div>
<div>进入clone后的submodule，正常提交文件</div>
<div>$ cd submodule</div>
<div>$ touch bootstrap.php</div>
<div>$ git add bootstrap.php</div>
<div>$ git commit -m 'add bootstrap.php'</div>
<div>$ git push</div>
<div>
</div>
<div>
</div>
<div>进入main</div>
<div>$ cd main</div>
<div>$ cd submodule</div>
<div>$ git checkout master</div>
<div>$ git pull</div>
<div>$ git log</div>
<div>//记录最后一次log的commit-id</div>
<div>$ git checkout commit-id切换到最后的commit-id上，进入断头模式</div>
<div>$ cd ../</div>
<div>$ git submodule init</div>
<div>$ git submodule</div>
<div>$ git commit -m 'update submodule'</div>
<div>$ git push</div>
<div>
</div>
<div>进入foobar</div>
<div>$ git pull</div>
<div>$ git submodule update即可</div>
<div>
</div>
<div>
</div>
<div>断头模式可以使用户无法对submodule进行提交，但是可以获取。其实是可以提交的，需要在submodule中checkout到对应的branch中。然后commit再提交。但是在主库中可以设定用户不可提交，需要提交时可临时设定可提交，提交submodule后再关闭，即完成对submodule的更新。</div>
<div>现在公司就是使用这种模式进行开发，而且我们也不可直接对submodule进行修改，submodule就是处于断头模式，即使checkout到master后提交，也提示错误。</div>
<div>CTO需要对submodule修改得话，需要进入主库后git config receive.denyCurrentBranch ignore后，在submodule clone后的库中，进行修改然后push，再删除git config中的 receive.denyCurrentBranch设置。git config --unset receive.denyCurrentBranch。然后再在开发的库中（已增加submodule），讲submodule切换到master，然后pull下来刚修改的代码，将submodule checkout到最后的提交commit上，然后进入上层目录，git submodule init后，commit后push，其他用户pull下来后git submodule init,git submodule update即可，或者直接删除掉submodule目录重新git submodule init git submodule update也可。</div>
<div>
</div>
