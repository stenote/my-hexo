title: Git add
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>git add 主要是一个将修改后的文件和未跟踪的文件增加到暂存区中的命令，当然，文件删除之后为了增加暂存区，也可是改用该命令</div>
<div>
</div>
<div>修改若干文件后，git status查看状态，会提示未跟踪的文件和未staged（未增加到暂存区）的文件，可能结果类似如下：</div>
<div>
</div>
<div>

may@Mint:~/workspace/git$ git status
# On branch develop
# Changes to be committed:
# (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
#
# modified: config.php
#
# Changes not staged for commit:
# (use &quot;git add/rm &lt;file&gt;...&quot; to update what will be committed)
# (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)
#
# deleted: hello.php
# modified: index.php
#
# Untracked files:
# (use &quot;git add &lt;file&gt;...&quot; to include in what will be committed)
#
# modules/
may@Mint:~/workspace/git$

通过git status，查看到了已增加至暂存区，但死未commit的文件、未增加的文件、未跟踪的文件、以及删除了的文件。

首先，增加index.php进暂存区，使用命令

$ git add index.php

增加未跟踪的文件进暂存区，同样使用命令

$ git add modules/

增加删除的文件进暂存区

$ git add hello.php

$ git statsu 查看一下状态

may@Mint:~/workspace/git$ git status
# On branch develop
# Changes to be committed:
# (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
#
# modified: config.php
# modified: index.php
# new file: modules/bootstrap.php
#
# Changes not staged for commit:
# (use &quot;git add/rm &lt;file&gt;...&quot; to update what will be committed)
# (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)
#
# deleted: hello.php
#
may@Mint:~/workspace/git$

发现删除了的文件hello.php没有在git add hello.php的情况下直接增加到暂存区。网上搜索之后发现可在使用该命令的时候增加参数

$ git add -u hello.php

$ git status

查看状态

may@Mint:~/workspace/git$ git status
# On branch develop
# Changes to be committed:
# (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)
#
# modified: config.php
# deleted: hello.php
# modified: index.php
# new file: modules/bootstrap.php
#
may@Mint:~/workspace/git$

 发现已经增加至暂存区了，直接进行后续的git commit等其他操作。

对于已删除并正确add后文件进行reset，需要使用

$ git reset -- file_name

来reset该文件。或者使用

$ git reset HEAD file_name

对于某个我们想删除的文件，无需进行add可直接增加到暂存区中，我们可以使用

$ git rm file_name 进行删除

批量文件增加至暂存区，可直接使用

$ git add directory_name/进行增加

$ git add `find  ./ -name '*.php'` 查找当前目录下所有文件名后缀为.php的文件，增加至暂存区

</div>
