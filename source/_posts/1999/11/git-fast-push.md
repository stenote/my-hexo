title: git fast push
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>正常情况下，多人协同开发时，可能遇到在push的时候提示push失败，大部分情况下可能是由于个人原因导致用户确实很想将当前文件push上去，可以使用git push -f，强制将当前commit push到origin 的repository中去，但是这样会导致其他用户pull的时候，将其他用户强制push上的文件pull下来，由下一个人进行修正该问题。</div>
<div>
</div>
<div>为了防止出现这种情况，可以在主库中设定</div>
<div>git config receive.denyNonFastForwards true</div>
