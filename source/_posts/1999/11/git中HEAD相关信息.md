title: Git 中HEAD相关内容
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>git 中，我们有时候进行reset等操作的时候，会提示HEAD被重置了，其他一些相关的文档、书籍内提示HEAD为一个指针类的东西，我们可以理解为游标。下面对HEAD进行相关学习和记录</div>
<div>.git/HEAD内，设定了HEAD指向的地址</div>
<div>$ git branch </div>
<div>   develop</div>
<div>* master</div>
<div>当前，我们所在的branch为master，我们去查看一下HEAD</div>
<div>$ cat .git/HEAD</div>
<div>ref: refs/heads/master</div>
<div>ref是<span>reference的缩写，就是说，参考refs/heads/master的内容</span></div>
<div>如果我们切换一下branch，再看看HEAD</div>
<div>$ git checkout develop</div>
<div>$ cat .git/HEAD</div>
<div>ref: refs/heads/develop</div>
<div>就是说参考 refs/heads/develop的内容，我们查看一下refs/heads/develop的内容</div>
<div>$ cat .git/refs/heads/develop</div>
<div>0196d532a17b91ad6408320911f5ecf52148bb70</div>
<div>发现是SHA1字符串</div>
<div>查看一下类型和内容</div>
<div>$ git cat-file -t 0196d532a17b91ad6408320911f5ecf52148bb70</div>
<div>commit</div>
<div>$ git cat-file commit f0196d532a17b91ad6408320911f5ecf52148bb70</div>
<div>

tree 7e2b2eb31b5077b424c8ba3030daa03f2b1838f2
author global &lt;rui.ma@geneegroup.com&gt; 1329737841 +0800
committer global &lt;rui.ma@geneegroup.com&gt; 1329737841 +0800

init

发现HEAD指向为一个commit，我们查看git log后，发现就是当前分支的最上端的commit

也就是说，通常情况下，HEAD都指向当前分支的最后提交的那个commit

HEAD-&gt;refs/heads/branch_name-&gt;commit-id-&gt;commit

</div>
