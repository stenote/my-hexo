title: MySql克隆表
tags:
- MySQL
date: "1999-11-30 16:00:00"
---

<div>create table new_table_name like original_table_name;</div>
<div>for example :</div>
<div>create table user2 like user;</div>
<div>create table temp_labs like labs;</div>
<div>
</div>
<div>**克隆只是对于表机构的克隆，不是克隆表数据。**</div>
<div>**而且克隆表的结构的时候primary_key等不会克隆过来。**</div>
<div>
</div>
<div>同时克隆数据，使用进行数据克隆，使用如下命令：</div>
<div>insert into new_table_name select * from original_table_name;</div>
<div>当然，在数据克隆的时候可以设定只克隆部分数据：</div>
<div>insert into new_table_name select * from original_table_name where id &gt; 20;</div>
<div>
</div>
<div>**克隆的同时数据复制：**</div>
<div>
</div>
<div>create table new_table_name select * from original_table_name;</div>
<div>
</div>
<div>如果在create一个新的table的时候还需要增加新的属性，可以使用：</div>
<div>
</div>
<div>create table new_table_name (</div>
<div>id int(4) not null auto_increment,</div>
<div>primary key (id)</div>
<div>)</div>
<div>select * from original_table_name;</div>
<div>
</div>
<div>
</div>
