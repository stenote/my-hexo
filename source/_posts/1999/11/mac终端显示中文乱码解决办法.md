title: Mac 终端显示中文乱码解决办法
tags:
- Linux
date: "1999-11-30 16:00:00"
---

<div>

vi /Users/USERNAME/.inputrc
添加如下内容并保存

set meta-flag on
set convert-meta off
set input-meta on
set output-meta on

vi /etc/profile

添加下列内容退出,(放在/Users/USERNAME/.profile 也一样)

export LANG=zh_CN.UTF-8

</div>
