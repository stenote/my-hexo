title: bootstrap
tags:
- framework
date: "1999-11-30 16:00:00"
---

<div>bootstrap一般为index.php直接加载，通常位于system(core、libraries)中，kohana中bootstrap在application（modules、projects）中。</div>
<div>
</div>
<div>spl_autoload_register(Bootstrap::auto_load)</div>
<div>
</div>
<div>abstract class Bootstrap {</div>
<div>public $base_url;</div>
<div>public $path_info;</div>
<div>public $current_project;</div>
<div>public $project_dir;</div>
<div>
</div>
<div>
</div>
<div>//启动函数</div>
<div>public static function setup() {</div>
<div>
</div>
<div>}</div>
<div>
</div>
<div>//class自动加载函数</div>
<div>public static function auto_load($class) {</div>
<div>
</div>
<div>}</div>
<div>
</div>
<div>//获取path_info函数</div>
<div>public static function get_path_info() {</div>
<div>
</div>
<div>}</div>
<div>
</div>
<div>public static function set_project($project) {</div>
<div>define(INITIAL_PROJECT, $project);</div>
<div>self::$project = $project;</div>
<div>self::$project_dir = DIR_LIB. $project. DIRECTORY_SEPARATOR;</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>}</div>
<div>
</div>
<div>}</div>
<div>
</div>
<div>
</div>
<div>通过获取pathinfo和系统中现有projects继续比较，获取当前project并更新pathinfo，set_projects，设置当前project， 设定$project_config， 同时加载core核心，设定projects，加载$include_dir。等相关信息。调用Core的setup()</div>
