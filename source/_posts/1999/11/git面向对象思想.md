title: Git 面向对象思想
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>Git很好得使用了面向对象的思想对文件、commit进行管理</div>
<div>使用一下几个简单的命令就可以看出来</div>
<div>首先，我们创建一个新的库，并产生相关commit</div>
<div>$ mkdir myGit</div>
<div>$ cd myGit</div>
<div>$ git init</div>
<div>$ echo '&lt;?php ' &gt; index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'add index.php'</div>
<div>//以上为创建repository和增加index.php文件相关代码</div>
<div>$ mkdir config</div>
<div>$ echo '&lt;?php $config = NULL;' &gt; config/config.php </div>
<div>$ git add config</div>
<div>$ git commit -m 'add config'</div>
<div>//增加新目录config以及config目录下的config.php文件</div>
<div>//增加至repository中去</div>
<div>
</div>
<div>$ git log --oneline</div>
<div>c6f9f23 add config</div>
<div>bb08f6c add index.php</div>
<div>我们可以看到这两个commit-id</div>
<div>先查看第一个commit-id相关信息</div>
<div>$ git cat-file -t c6f9f23</div>
<div>commit</div>
<div> //获取第一个commit-id 对应的类型，提示类型为commit，即为一次提交 cat-file 为获取文件内容，-t参数为获取type，即类型</div>
<div>
</div>
<div>$ git cat-file commit c6f9f23</div>
<div>//查看这个commit 的相关内容</div>
<div>

tree 19de6039e22c7e19011f5c1e49632f52062776ca
parent bb08f6cd25a4cdfabb98b9124708f1d6532e1aae
author global &lt;rui.ma@geneegroup.com&gt; 1329655970 +0800
committer global &lt;rui.ma@geneegroup.com&gt; 1329655970 +0800

add config

</div>
<div>//以上内容显示了一个commit指向一个tree，一个parent，并且这个commit包含了相关的author committer等。以及commit的时候提交的commit message。//commit信息。</div>
<div>
</div>
<div>我们看一下parent，发现parent指向的也是40位的hash字符串。仔细比对后发现和第一个commit的commit-id相同（前9位相同，其实真的是相同的，只不过查看log的时候用的是 --oneline，会自动截取前9位而已）</div>
<div>查看一下parent是不是指向上一个 commit</div>
<div>$ git cat-file -t bb08f6c</div>
<div>commit</div>
<div>$ git cat-file commit bb08f6c</div>
<div>
</div>
<div>

tree 0b1f852200468dbce415877d81f280c291b0480b
author global &lt;rui.ma@geneegroup.com&gt; 1329655776 +0800
committer global &lt;rui.ma@geneegroup.com&gt; 1329655776 +0800

add index.php 

</div>
<div>显示内容如上：</div>
<div>发现commit message和第一个commit相同。这就说明了</div>
<div>1、每次commit都有一个parent属性，这个parent指向上一次（不是说时间史的上一个，就是由某个或多个commit merge、继续开发产生了当前commit ）commit (s)</div>
<div>
</div>
<div>我们再看看 tree是什么东西</div>
<div>
</div>
<div>$ git cat-file -t 19de6039</div>
<div>tree</div>
<div>提示这是一个tree，到底什么是tree，继续看下去</div>
<div>$ git ls-tree 19de6039</div>
<div>

040000 tree 5403102af0d8c495fe65e5a7ea4e4afde426dbe1 config
100644 blob dc84e23eee4f2bb27933347ab6d9f352f393134e index.php

我们使用ls-tree来查看一个tree对象的相关内容，cat-file用来查看commit blob 等相关对象内容。页面 提示，当前tree对应了另外一个tree和一个blob。我们发现blob后面跟着一个index.php文件名称。难道这个blob对象就是一个文件吗？尝试查看一下这个blob的内容

$ git cat-file blob dc84e23

&lt;?php

页面提示内容和index.php内容一致，说明blob就是一个文件对象的别称，如果这么说，那我们可以猜想，tree对应的为一个目录，刚才的那个tree就是config目录，如果这么说得话，ls-tree这个tree，应该有一个名称为config.php的blob，并且这个blob内容和config目录下的config.php内容相同

$ git ls-tree 5403102

100644 blob 1d81c43a10211e4a5320311aab34940763158a78    config.php

我们看到了这个blob，第一步猜想正确

$ git cat-file blob 1d81c43

&lt;?php $config = NULL;

内容和config目录下的config.php内容相同，也就是说tree对象其实就是对应一个目录

总结

2、一个commit如果在提交了新目录的情况下，会有tree这个属性，tree为一个目录对象的别称，可以通过ls-tree这个目录，查看这个tree下的blob（文件）

我们再做一个尝试

//我们修改index.php的第二行为hello后commit -a -m 'modify index.php on master'

//checkout到foobar branch上，修改index.php第二行内容为world，然后commit -a -m 'modify index.php on foobar'

//切换到master，但是不merge，修改index.php第二行内容为nice，commit -a -m 'modify index.php on master'

//然后我们merge，系统提示错误，修改index.php 第二、三行内容分贝为nice 和world，然后commit -a -m 'merge foobar'

这个时候我们看一下最后的commit-id的相关属性

$ git log --oneline 

685ca0d merge foobar
15908a1 modify index.php on branch
dac22cb modify index.php on foobar
1659405 modify index.php on master
c6f9f23 add config
bb08f6c add index.php

$ git cat-file commit 685ca0d

tree d4e84b12563da39f56633e29ad29277d05d4e5c0
parent 15908a1a343cdef1b2b46cc126a7fa2e880464f6
parent dac22cbcd81a7f0a1a2930d5361a379eb342248b
author global &lt;rui.ma@geneegroup.com&gt; 1329659410 +0800
committer global &lt;rui.ma@geneegroup.com&gt; 1329659410 +0800

merge foobar

我们发现这个commit有两个parent，就说明这个commit是两次commit merge得到的。这两个parent均为两次commit，分别是HEAD^1 HEAD^2（请自行比对commit-id）

同样，我们看一下tree

$ git ls-tree d4e84b

040000 tree 5403102af0d8c495fe65e5a7ea4e4afde426dbe1 config
100644 blob 159f1c4a088c797ff6ba3e0bb28474251f1acf87 index.php

同样为当前repository的文件和目录对应的blob 和tree对象。

git中，所有的文件和目录都会转换为自身相应的对象，文件对应blob对象，目录对应tree对象。

我们add并commit的时候，实际上是对这些blob和tree进行操作，然后产生新的commit对象。commit对象包含上次的commit以及commit相关的tree和blob

</div>
<div>
</div>
<div>我们进入repository的.git/objects目录看一下，发现目录下有很多文件。调一个</div>
<div>c3目录下有个文件名称为e4cfc298be6b6f12eaf244c4ccc26fad5ddd3c</div>
<div>$ git cat-file -t e4cfc298be6b6f12eaf244c4ccc26fad5ddd3c</div>
<div>提示出错，我们尝试和目录名称链接起来试试</div>
<div>$ git cat-file c3e4cfc298be6b6f12eaf244c4ccc26fad5ddd3c</div>
<div>tree</div>
<div>$ git ls-tree c3e4cfc298be6b6f12eaf244c4ccc26fad5ddd3c</div>
<div>

100644 blob e69de29bb2d1d6434b8b29ae775ad8c2e48c5391 foobar.php

$ git cat-file blob e69de29bb2d1d6434b8b29ae775ad8c2e48c5391

内容为空，估计是某次commit时的foobar.php当时的blob对象的相关信息。

//不知道上述说法是否正确。TODO

所有的对象会以对象名前两位为目录名称，其余为文件名称，保存在.git/objects/目录下。不信可以自己去试

</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
