title: Git reset
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>git reset命令通常对于已经commit后的信息进行重置使用，通常有一下四种用法：</div>
<div>git reset --hard</div>
<div>git reset --soft</div>
<div>git reset --mixed</div>
<div>git reset --</div>
<div>1、git reset --hard</div>
<div> 一次commit后我们对这次commit进行reset的时候附加--hard参数，会使这次commit的所有的文件回复到commit时的状态，这次commit后的文件都会被删除，workspace index中都不会存在</div>
<div>2、git reset --mixed</div>
<div>一次commit后，使用reset进行重置时附加--mixed参数，会将commit重置到上次文件中，workspace中文件不变</div>
<div>3、git reset --soft</div>
<div>一次commit后，使用reset进行重置时附加--soft参数，会将commit重置到index这步，直接进行commit就能直接进行push了。</div>
<div>4、如果已经通过git add将文件加到index了，可以通过第四种方法，从index中将文件撤销出来</div>
<div>git reset -- file_name</div>
<div>
</div>
<div>未修改 0 workspace1 index 2 repository 3</div>
<div>git reset --hard 文件从3reset到0</div>
<div>git reset --mixed 文件从3reset到1</div>
<div>git reset --soft 文件从3reset到2</div>
<div>git reset -- 文件从2reset到1</div>
<div>git checkout 文件从1reset到0</div>
