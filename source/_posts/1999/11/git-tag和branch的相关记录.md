title: Git tag和branch相关记录
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>我们可以通过git log来查看某个commit的相关commit-id，然后在对某个位置进行相关标记。</div>
<div>相当于说。我们对此提交定位为一个特定的点，这个点的软件版本为v1</div>
<div>$ git log</div>
<div>//查看log</div>
<div>$ git tag v1 0f32fe</div>
<div>//</div>
<div>可以直接通过git show 0f32fe查看这次commit-id对应的提交，或者直接通过git show v1来查看</div>
<div>
</div>
<div>我们在0f32fe这时候打了tag为v1，之后继续开发。同时我们将该tagpush到origin的repository上去。之后其他用户pull下来该tag后重新创建用于发布的branch，进行发布</div>
<div>
</div>
<div>以上大概可理解为</div>
<div>$ git log    //查看log</div>
<div>$ git tag v1 0f32fe  //对某个commit 打上标记为v1</div>
<div>$ git push --tags //push所有的tag</div>
<div>
</div>
<div>以下为第二个用户的操作</div>
<div>$ git pull  //获取所有的tag</div>
<div>$ git branch version_v1 v1  //创建一个基于v1 tag branch</div>
<div>$ git checkout version_v1 //切换到version_v1分支上，然后打包发布</div>
<div>
</div>
