title: Git面向对象思想（1）tag blob commit tree
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>git 很好得使用了面向对象思想，将git内的文件抽象成了blob，目录树抽象成了tree，一次commit又是一个对象，tag指向一个commit，tag也是一个新的对象。</div>
<div>对象存储位置</div>
<div>每一个对象都是用40位的SHA1来进行存储，git将其拆分了两部分，一部分为2位的目录，一部分为后38位的文件名称，将这个</div>
<div>2位的目录和38位的文件存放在当前git目录下的.git/objects中。</div>
<div>假如有一个SHA1为b406bccf047e597135f5744fac2212e7a6050764的对象，我们可以在.git/objects下的目录b4中找到一个名称为06bccf047e597135f5744fac2212e7a6050764的文件，这个文件就对应的是b406bccf047e597135f5744fac2212e7a6050764这个对象。</div>
<div>
</div>
<div>git中查看一个SHA1的类型和内容，可以使用git cat-file增加不同参数来进行实现，我们就使用该命令，尝试对commit进行相关分析</div>
<div>
</div>
<div>1、commit</div>
<div>首先我们从git log中获取到一个commit的SHA1值</div>
<div>$ git log --oneline</div>
<div>b406bcc</div>
<div>//--oneline，增加该参数，会使log显示只显示所需的SHA1值，通常为前几位。但是调用结果和40位的SHA1值相同。无任何影响，所以之后我们都用前位的SHA1值进行代替</div>
<div>
</div>
<div>我们使用git cat-file -t 查看该SHA1的类型</div>
<div>$ git cat-file -t b406bcc</div>
<div>commit</div>
<div>//git提示这个SHA1的类型为一个commit，</div>
<div>
</div>
<div>我们接着往下找</div>
<div>$ git cat-file -p b406bcc</div>
<div>//git cat-file commit b406bcc也可实现相同效果</div>
<div>tree a415400a2c6a2157a620802f4b0c455632e54f63</div>
<div>parent 073fd6332ae6fc0235d3af4d516fee944fe261d7</div>
<div>author may &lt;xjmarui@gmail.com&gt; 1328795643 +0800</div>
<div>committer may &lt;xjmarui@gmail.com&gt; 1328795643 +0800</div>
<div>
</div>
<div>页面返回该commit的相关细信息，</div>
<div>我们发现该commit具有parent、tree、author、commiter等信息。</div>
<div>我们可以猜测tree应该就是tree对象，author和committer就是这次提交的属性，但是parent显示的也是SHA1，到底这是什么。我们用cat-file重新进行一次检测。</div>
<div>$ git cat-file -t 073fd6</div>
<div>commit</div>
<div>$ git cat-file -p 073fd6</div>
<div>tree 6d19ab1dff69838d446f2eb8c4d1f02dd082068b</div>
<div>parent ef33e3f3a20b10e49249a4e761d30b1e3875ff4a</div>
<div>author may &lt;xjmarui@gmail.com&gt; 1328787090 +0800</div>
<div>committer may &lt;xjmarui@gmail.com&gt; 1328787090 +0800</div>
<div>update bootstrap</div>
<div>
</div>
<div>也是一个commit，git log查看后发现，是HEAD的commit的上一个commit，即HEAD^</div>
<div>
</div>
<div>我们推测commit对象的属性，commit对象具有parent属性，parent也是另外一个commit，由于有merge的存在，可能commit对象的parent为多个，author、committer即为当时commit提交者的相关信息。commit还有一个message属性，保存commit提交时的相关描述信息。我们再看一个commit的tree属性</div>
<div>
</div>
<div>2、tree</div>
<div>$ git cat-file -t a415400</div>
<div>tree</div>
<div>提示为tree</div>
<div>$ git cat-file -p a415400</div>
<div>//查看该tree的相关信息，也可使用git ls-tree命令</div>
<div>100755 blob bfb5a81d1b3abab7d7377caa03a994af6dd74ccf .htaccess</div>
<div>100644 blob a58ef11241c281ce02d80fe7ea5a9fd990f32959 config.php</div>
<div>100644 blob 1a124f6e0a1d3a10b694a9f8bea90a4d1f72f394 index.php</div>
<div>040000 tree 7bb37f5bdef8b6282625df5d9ff3767bae5fc65d libraries</div>
<div>040000 tree d175b24632dfc5614dcf5076623b510ac281552c projects</div>
<div>040000 tree d59bdd1200c24ef0426785efd12d48b2c28ce87a shell</div>
<div>040000 tree 6648723260208038dea26d89f60a16db88ad5f32 update</div>
<div>我们可以看到，一个tree可以有多个tree和多个blob对象。</div>
<div>我们ls查看当前目录</div>
<div>$ ls -al</div>
<div>.htaccess config.php index.php libraries projects shell update</div>
<div>我们发现，当前目录下的文件和目录，就是这个tree，只不在git中将文件抽象为blob对象，将目录抽象为tree对象。每个tree对象一般都有多个blob对象和tree对象，但是blob对象不可能有tree对象。</div>
<div>我们查看blob对象相关信息</div>
<div>
</div>
<div>3、blob</div>
<div>$ git cat-file -p bfb5a81d</div>
<div>RewriteEngine On</div>
<div>RewriteRule ^~([a-zA-Z0-9-_]+)~(.*)$ &quot;projects/$1/wwwroot/$2&quot; [L]</div>
<div>RewriteRule ^(?!(?:wwwroot/|index.php|projects/[a-zA-Z0-9-_]+/wwwroot/)).+$ wwwroot/$0 [PT,NS]</div>
<div>RewriteCond %{REQUEST_FILENAME} !-f</div>
<div>RewriteCond %{REQUEST_FILENAME} !-l</div>
<div>RewriteCond %{REQUEST_FILENAME} !-d</div>
<div>RewriteRule wwwroot/(.*)$ index.php/$1 [PT,L]</div>
<div>
</div>
<div>$ cat .htaccess</div>
<div>RewriteEngine On</div>
<div>RewriteRule ^~([a-zA-Z0-9-_]+)~(.*)$ &quot;projects/$1/wwwroot/$2&quot; [L]</div>
<div>RewriteRule ^(?!(?:wwwroot/|index.php|projects/[a-zA-Z0-9-_]+/wwwroot/)).+$ wwwroot/$0 [PT,NS]</div>
<div>RewriteCond %{REQUEST_FILENAME} !-f</div>
<div>RewriteCond %{REQUEST_FILENAME} !-l</div>
<div>RewriteCond %{REQUEST_FILENAME} !-d</div>
<div>RewriteRule wwwroot/(.*)$ index.php/$1 [PT,L]</div>
<div>与当前内容一致</div>
<div>
</div>
<div>我们删除.htaccess内容后进行提交</div>
<div>提交后commit为aee5233024fcaa66447ff0e1ff5fbf23f544317a</div>
<div>该commit内的.htaccess对应的blob对象的SHA1为e69de29bb2d1d6434b8b29ae775ad8c2e48c5391</div>
<div>我们发现现在.htaccess这文件对应的blob对象名称发生了变化，获取内容发现为空</div>
<div>$ git cat-file -p e69de29bb2d1</div>
<div>
</div>
<div>我们可以总结，同一个文件只要发生修改，会在文件的新状态下产生一个blob对象，该blob对象指向该文件当时的一种状态，包含了文件的各种属性，我们可以通过git cat-file获取该blob的内容，即当时文件的内容。</div>
<div>
</div>
<div>4、tag</div>
<div>tag是一个特殊形式的文件，它可指向任意的上述三个对象，所以git中将它归为git中的一个对象，其实我觉得它不是对象，应该是相当于一个refs，不然为啥它没SHA1值，而且文件是存储在.git/refs/tags下的。</div>
<div>获取tag的SHA1（tag对应的对象的SHA1值）命令为</div>
<div>$ git rev-parse tag_name</div>
<div>其实可以直接进入.git/refs/tags/查看tag_name对应的名称相同的文件的内容，内容为SHA1</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
