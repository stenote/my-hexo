title: git submodule
tags:
- Git
date: "1999-11-30 16:00:00"
---

<div>
<div>git submodule 可以在主库下增加一个新的二级module，便于在当前库开发时候和某些其他文件分离开发，通常使用在某个项目中，Core（system）核心和module区分开发。公司现在就是这样做的。</div>
<div>进行submodule开发，需要有两个库，一个是主库，一个是submodule库</div>
<div>$ mkdir main</div>
<div>$ mkdir submodule</div>
<div>$ cd main </div>
<div>$ git init</div>
<div>$ touch main.php</div>
<div>$ git add main.php</div>
<div>$ git commit -m 'main init'</div>
<div>$ git config receive.denyCurrentBranch ignore</div>
<div>$ cd ../submodule</div>
<div>$ git init </div>
<div>$ touch submodule.php</div>
<div>$ git add submodule.php</div>
<div>$ git commit -m 'submodule init'</div>
<div>$ git config receive.denyCurrentBranch ignore</div>
<div>
</div>
<div>如上，我们创建了两个git库，分别为main和submodule，现在进行合并submodule库为main库的submodule</div>
<div>首先克隆main库、submodule库</div>
<div>$ cd /a/new/path</div>
<div>$ git clone git@localhost:/path/to/main</div>
<div>blablablabla</div>
<div>$ ls</div>
<div>main</div>
<div>克隆成功</div>
<div>$ git clone git@localhost:/path/to/submodule</div>
<div>$ ls</div>
<div>main submodule</div>
<div>这样，submodule库也clone下来了。</div>
<div>
</div>
<div>第二</div>
<div>在main库中增加submodule库</div>
<div>$ cd main</div>
<div>$ git submodule add git@localhost:/path/to/submodule </div>
<div>blablabla</div>
<div>$ ls</div>
<div>submodule main.php</div>
<div>可以看到submodule目录和main.php文件了</div>
<div>$ git status</div>
<div># On branch master</div>
<div># Changes to be committed:</div>
<div># (use &quot;git reset HEAD &lt;file&gt;...&quot; to unstage)</div>
<div>#</div>
<div># new file: .gitmodules</div>
<div># new file: submodule</div>
<div>
</div>
<div>新增了新的文件和目录</div>
<div>$ git commit -m 'add submodule'</div>
<div>$ git push</div>
<div>//push到remote的repository上去</div>
<div>
</div>
<div>$ cd ../</div>
<div>$ git clone git@localhost:/path/to/main test</div>
<div>blablabla</div>
<div>$ cd test</div>
<div>$ git pull</div>
<div>balbalbal</div>
<div>$ ls </div>
<div>main submodule</div>
<div>$ git submodule</div>
<div>-e5530773377c8301fa471468dc6c075d9f0909e5 submodule</div>
<div>可以看到submodule</div>
<div>
</div>
<div>下面我们尝试更新submodule</div>
<div>$ cd ../submodule //切换到submodule repository下，并非test或main repository的submodule目录下</div>
<div>$ touch bootstrap.php</div>
<div>$ git add bootstrap.php</div>
<div>$ git commit -m 'add bootstrap.php'</div>
<div>$ git push</div>
<div>blablabla</div>
<div>//创建了一个bootstrap.php的文件进行了上传</div>
<div>
</div>
<div>在main中获取新的submodule代码</div>
<div>$ cd ../main //进入main 的repository</div>
<div>$ git pull</div>
<div>直接进行pull，发现无任何数据下来</div>
<div>$ cd submodule</div>
<div>$ git pull</div>
<div>页面提有文件pull下来</div>
<div>$ cd ../</div>
<div>$ git status</div>
<div> modified submodule(new commit)</div>
<div>$ git add submodule</div>
<div>$ git commit -m 'update submodule add bootstrap.php'</div>
<div>$ git push</div>
<div>blabalbal提示push成功</div>
<div>
</div>
<div>$ cd ../test</div>
<div>$ git pull</div>
<div>//提示pull下来数据</div>
</div>
