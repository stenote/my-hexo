title: C字符串长度获取
tags:
- C
date: "2013-10-07 23:27:42"
---

简单的进行字符串长度获取函数：

如下，使用3种方法进行获取：

<!--more-->

1、使用char str[]作为参数进行获取：

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

int len(char str[]);

void main() {
    printf(&quot;%d\n&quot;, len(&quot;hello world&quot;));
}

int len(char str[]) {
    int len = 0;
    while (str[len]) ++len;
    return len;
}
</pre>

2、使用系统自带strlen函数
<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;
#include &lt;string.h&gt;

void main() {
    printf(&quot;%d\n&quot;,strlen(&quot;hello world&quot;));
}
</pre>

3、使用char *str

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

int len(char *str);

void main() {
    printf(&quot;%d\n&quot;,len(&quot;hello world&quot;));
}

int len(char *str) {
    int len = 0;
    while (*str ++) len++;
    return len;
}

</pre>
