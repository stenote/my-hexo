title: C程序设计语言(#1.8)
tags:
- C
date: "2013-10-06 23:06:33"
---

> 练习 1-8 编写一个统计空格、制表符与换行符的程序

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

int main() {

    //char 由于有可能有EOF，故设定为int
    int c;

    int nt = 0, nn = 0, nb = 0;
    //nt number of tab
    //nn number of newline
    //nb number of blank

    while((c = getchar()) != EOF) {

        switch(c) {
            case &#039;\n&#039; :
                ++ nn;
                break;
            case &#039;\t&#039; :
                ++ nt;
                break;
            case &#039; &#039; :
                ++ nb;
                break;
            default :
                break;
        }
    }

    printf(&quot;tab: \t%d\n&quot;, nt);
    printf(&quot;newline: \t%d\n&quot;, nn);
    printf(&quot;blank: \t%d\n&quot;, nb);

    return 0;
}
</pre>
