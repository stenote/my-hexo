title: C程序设计语言(单词计数)
tags:
- C
date: "2013-10-07 00:12:57"
---

> P#14 统计行数、单词数与字符数

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

int main() {

    int nc = 0, nw = 0, nl = 0, c;
    //nc number of count
    //nw number of word
    //nl number of line

    while((c = getchar()) != EOF) {
        ++ nc;
        switch(c) {
            case &#039;\n&#039; :
                ++ nl;
            case &#039; &#039; :
            case &#039;\t&#039; :
                ++ nw;
                break;
        }
    }

    printf(&quot;line: %d  word: %d  count: %d\n&quot;, nl, nw, nc);
    return 0;
}
</pre>