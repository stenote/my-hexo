title: yaml_emit
tags:
- PHP
- yaml
date: "2013-10-01 10:59:46"
---

yaml在php中使用之前，需安装，见[YAML (安装PHP扩展)](http://www.mmmmm.io/2013/09/yaml/)
<!--more-->

使用yaml_emit进行一个数组处理，代码如下：

```
#!/usr/bin/env php
<?php

$arr = array(
    'key'=> 'value',
    'value',
    'bar',
    array(
        'key'=> 'value' ,
        'value'=> array(
            'value'=>'key'
        ),
        'foo'=> array(
            'a',
            'b'
        )
    )
);


echo yaml_emit($arr);

```

返回结果如下：

```
---
key: value
0: value
1: bar
2:
  key: value
  value:
    value: key
  foo:
  - a
  - b
...
```

yaml_emit 进行数据处理需要遵循以下几点：
1、`---` 起始，`...` 结束
2、数组没有key，则会以- 代替。如上述的
```
array(
    'value',
    'key'
);
```
处理结果为：
```
---
- value
- key
...
```
3、如果 array 有 key，则处理为 key: value，如下：

```
array(
    'value'=> 'key',
    'bar'
);
```
处理结果：

```
---
value: key
0: bar
...
```
第0个元素 key 为 value，value 为 key，故处理结果为 `value: key`，第1个元素的 key 为 0，value 为 bar，故处理为 `0: bar`

4、针对二维、多维数组，进行缩进，并同时遵循上述1、2、3规则

```
array(
    'value'=> array(
        'hello'=>'world'
    )
);
```

处理结果如下：

```
---
value:
  hello: world
...
```
