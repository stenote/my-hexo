title: yaml_emit_file无法使用问题解决办法
tags:
- C
- PHP
- yaml
date: "2013-10-01 13:58:58"
---

yaml_emit_file作用：

> Generate a YAML representation of the provided data in the filename.

官方链接：[yaml_emit_file](http://cn2.php.net/manual/en/function.yaml-emit-file.php)

但是实际使用过程中出现如下错误：
<!--more-->

```
PHP Warning:  yaml_emit_file(): not yet implemented in /home/may/yaml/yaml.php on line 24
```

git clone了pecl中yaml的源码进行查看，发现 yaml.c 中关于 `yaml_emit_file` 源码如下：

```

PHP_FUNCTION(yaml_emit_file)
{
    char *filename = { 0 };
    int filename_len = 0;
    php_stream *stream = { 0 };
    FILE *fp = { 0 };
    zval *data = { 0 };
    const char *encoding = { 0 };
    int encoding_len = 0;
    const char *linebreak = { 0 };
    int linebreak_len = 0;
    zval *zcallbacks = { 0 };
    HashTable *callbacks = { 0 };

    yaml_emitter_t emitter = { 0 };

#ifdef IS_UNICODE
    if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s&z/|ssa/",
            &filename, &filename_len,
            ZEND_U_CONVERTER(UG(filesystem_encoding_conv)), &data,
            &encoding, &encoding_len, &linebreak, &zcallbacks,
            &linebreak_len)) {
        return;
    }
#else
    if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sz/|ssa/",
            &filename, &filename_len, &data, &encoding,
            &encoding_len, &linebreak, &zcallbacks,
            &linebreak_len)) {
        return;
    }
#endif

    php_error_docref(NULL TSRMLS_CC, E_WARNING, "not yet implemented");
    RETURN_FALSE;

    if (NULL == (stream = php_stream_open_wrapper(filename, "wb",
            IGNORE_URL | ENFORCE_SAFE_MODE | REPORT_ERRORS | STREAM_WILL_CAST,
            NULL))) {
        RETURN_FALSE;
    }

    if (FAILURE == php_stream_cast(
            stream, PHP_STREAM_AS_STDIO, (void **) &fp, 1)) {
        php_stream_close(stream);
        RETURN_FALSE;
    }

    yaml_emitter_initialize(&emitter);
    yaml_emitter_set_output_file(&emitter, fp);
    yaml_emitter_set_encoding(&emitter, (yaml_encoding_t) encoding);
    yaml_emitter_set_break(&emitter, (yaml_break_t) linebreak);
    yaml_emitter_set_canonical(&emitter, YAML_G(output_canonical));
    yaml_emitter_set_indent(&emitter, YAML_G(output_indent));
    yaml_emitter_set_width(&emitter, YAML_G(output_width));
    yaml_emitter_set_unicode(&emitter, YAML_ANY_ENCODING != encoding);

    RETVAL_BOOL((SUCCESS == php_yaml_write_impl(
            &emitter, data, YAML_ANY_ENCODING, callbacks TSRMLS_CC)));

    yaml_emitter_delete(&emitter);
    php_stream_close(stream);
}
/* }}} yaml_emit_file */
```

如上，第34行，发现提示了错误，直接进行进行了返回。

```
php_error_docref(NULL TSRMLS_CC, E_WARNING, "not yet implemented");
RETURN_FALSE;
```

进行注释后重新编辑，功能正常

已在github上进行pull request，见：[这里](https://github.com/php/pecl-file_formats-yaml/pull/1)
