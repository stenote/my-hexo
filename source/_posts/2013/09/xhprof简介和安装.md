title: xhprof简介和安装
tags:
  - PHP
date: "2013-09-19 21:54:33"
---

xhprof github简介：

> XHProf is a function-level hierarchical profiler for PHP and has a simple HTML based user interface. The raw data collection component is implemented in C (as a PHP extension). The reporting/UI layer is all in PHP. It is capable of reporting function-level call counts and inclusive and exclusive wall time, CPU time and memory usage. Additionally… [
> 
> http://pecl.php.net/package/xhprof](http://pecl.php.net/package/xhprof "http://pecl.php.net/package/xhprof")
xhprof php官网简介：

> XHProf is a light-weight hierarchical and instrumentation based profiler. During the data collection phase, it keeps track of call counts and inclusive metrics for arcs in the dynamic callgraph of a program. It computes exclusive metrics in the reporting/post processing phase, such as wall (elapsed) time, CPU time and memory usage. A functions profile can be broken down by callers or callees. XHProf handles recursive functions by detecting cycles in the callgraph at data collection time itself and avoiding the cycles by giving unique depth qualified names for the recursive invocations.

github中xhprof地址：[https://github.com/facebook/xhprof](https://github.com/facebook/xhprof "https://github.com/facebook/xhprof")
php官网手册地址：[http://cn2.php.net/xhprof](http://cn2.php.net/xhprof "http://cn2.php.net/xhprof")

<!--more-->

## xphrof安装

建议使用pecl安装，简单。
高玩请绕行
1、手动编译源码安装
①下载源码
```
$ git clone https://github.com/facebook/xhprof.git
```

进行phpize
```
$ cd xhporf/extension/
$ sudo phpize --with-php-config=/usr/local/bin/php-config
```

结果如下：
```
Configuring for:
PHP Api Version:         20090626
Zend Module Api No:      20090626
Zend Extension Api No:   220090626
```

当前目录生成configure相关文件。
进行configure、make、make test、make install
```
# ./configure && make&& make test && make install
```

最终显示：
```
Installing shared extensions:     /usr/lib/php5/20090626+lfs/
```

查看/usr/bin/php5/20090626+lfs/发现增加了xhprof.so文件，完成对xhprof的安装。

2、pecl辅助安装

①先pecl进行xhprof检索：
```
$ sudo pecl search xhprof
```
结果如下：
```
Retrieving data...0%
Matched packages, channel pecl.php.net:
=======================================
Package Stable/(Latest) Local
xhprof  0.9.3 (beta)          XHProf: A Hierarchical Profiler for PHP
```

查找到了0.9.3的beta版本的xhprof。
②使用pecl进行安装
由于pecl中xhprof非stable版，故安装时需制定版本号，如下：

```
$ sudo pecl install xhprof-0.9.3
```
安装过程如下：
```
may@server:~$ sudo pecl install xhprof-0.9.3
downloading xhprof-0.9.3.tgz ...
Starting to download xhprof-0.9.3.tgz (841,836 bytes)
........................................................................................................................................................................done: 841,836 bytes
11 source files, building
running: phpize
Configuring for:
PHP Api Version:         20090626
Zend Module Api No:      20090626
Zend Extension Api No:   220090626
building in /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3
running: /tmp/pear/temp/xhprof/extension/configure
checking for grep that handles long lines and -e... /bin/grep
checking for egrep... /bin/grep -E
checking for a sed that does not truncate output... /bin/sed
checking for cc... cc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables...
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether cc accepts -g... yes
checking for cc option to accept ISO C89... none needed
checking how to run the C preprocessor... cc -E
checking for icc... no
checking for suncc... no
checking whether cc understands -c and -o together... yes
checking for system library directory... lib
checking if compiler supports -R... no
checking if compiler supports -Wl,-rpath,... yes
checking build system type... i686-pc-linux-gnu
checking host system type... i686-pc-linux-gnu
checking target system type... i686-pc-linux-gnu
checking for PHP prefix... /usr
checking for PHP includes... -I/usr/include/php5 -I/usr/include/php5/main -I/usr/include/php5/TSRM -I/usr/include/php5/Zend -I/usr/include/php5/ext -I/usr/include/php5/ext/date/lib -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
checking for PHP extension directory... /usr/lib/php5/20090626+lfs
checking for PHP installed headers prefix... /usr/include/php5
checking if debug is enabled... no
checking if zts is enabled... no
checking for re2c... no
configure: WARNING: You will need re2c 0.13.4 or later if you want to regenerate PHP parsers.
checking for gawk... no
checking for nawk... nawk
checking if nawk is broken... no
checking whether to enable xhprof support... yes, shared
checking how to print strings... printf
checking for a sed that does not truncate output... (cached) /bin/sed
checking for fgrep... /bin/grep -F
checking for ld used by cc... /usr/bin/ld
checking if the linker (/usr/bin/ld) is GNU ld... yes
checking for BSD- or MS-compatible name lister (nm)... /usr/bin/nm -B
checking the name lister (/usr/bin/nm -B) interface... BSD nm
checking whether ln -s works... yes
checking the maximum length of command line arguments... 1572864
checking whether the shell understands some XSI constructs... yes
checking whether the shell understands "+="... yes
checking how to convert i686-pc-linux-gnu file names to i686-pc-linux-gnu format... func_convert_file_noop
checking how to convert i686-pc-linux-gnu file names to toolchain format... func_convert_file_noop
checking for /usr/bin/ld option to reload object files... -r
checking for objdump... objdump
checking how to recognize dependent libraries... pass_all
checking for dlltool... no
checking how to associate runtime and link libraries... printf %s\n
checking for ar... ar
checking for archiver @FILE support... @
checking for strip... strip
checking for ranlib... ranlib
checking for gawk... (cached) nawk
checking command to parse /usr/bin/nm -B output from cc object... ok
checking for sysroot... no
checking for mt... mt
checking if mt is a manifest tool... no
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking for dlfcn.h... yes
checking for objdir... .libs
checking if cc supports -fno-rtti -fno-exceptions... no
checking for cc option to produce PIC... -fPIC -DPIC
checking if cc PIC flag -fPIC -DPIC works... yes
checking if cc static flag -static works... yes
checking if cc supports -c -o file.o... yes
checking if cc supports -c -o file.o... (cached) yes
checking whether the cc linker (/usr/bin/ld) supports shared libraries... yes
checking whether -lc should be explicitly linked in... no
checking dynamic linker characteristics... GNU/Linux ld.so
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... no
configure: creating ./config.status
config.status: creating config.h
config.status: executing libtool commands
running: make
/bin/bash /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/libtool --mode=compile cc  -I. -I/tmp/pear/temp/xhprof/extension -DPHP_ATOM_INC -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/include -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/main -I/tmp/pear/temp/xhprof/extension -I/usr/include/php5 -I/usr/include/php5/main -I/usr/include/php5/TSRM -I/usr/include/php5/Zend -I/usr/include/php5/ext -I/usr/include/php5/ext/date/lib -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64  -DHAVE_CONFIG_H  -g -O2   -c /tmp/pear/temp/xhprof/extension/xhprof.c -o xhprof.lo
libtool: compile:  cc -I. -I/tmp/pear/temp/xhprof/extension -DPHP_ATOM_INC -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/include -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/main -I/tmp/pear/temp/xhprof/extension -I/usr/include/php5 -I/usr/include/php5/main -I/usr/include/php5/TSRM -I/usr/include/php5/Zend -I/usr/include/php5/ext -I/usr/include/php5/ext/date/lib -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -DHAVE_CONFIG_H -g -O2 -c /tmp/pear/temp/xhprof/extension/xhprof.c  -fPIC -DPIC -o .libs/xhprof.o
In file included from /usr/include/php5/main/php.h:33:0,
                 from /tmp/pear/temp/xhprof/extension/xhprof.c:27:
/usr/include/php5/main/php_config.h:2426:0: warning: "_GNU_SOURCE" redefined [enabled by default]
/tmp/pear/temp/xhprof/extension/xhprof.c:24:0: note: this is the location of the previous definition
/tmp/pear/temp/xhprof/extension/xhprof.c:238:1: warning: 'visibility' attribute ignored [-Wattributes]
/tmp/pear/temp/xhprof/extension/xhprof.c:242:28: warning: 'visibility' attribute ignored [-Wattributes]
/bin/bash /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/libtool --mode=link cc -DPHP_ATOM_INC -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/include -I/tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/main -I/tmp/pear/temp/xhprof/extension -I/usr/include/php5 -I/usr/include/php5/main -I/usr/include/php5/TSRM -I/usr/include/php5/Zend -I/usr/include/php5/ext -I/usr/include/php5/ext/date/lib -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64  -DHAVE_CONFIG_H  -g -O2   -o xhprof.la -export-dynamic -avoid-version -prefer-pic -module -rpath /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules  xhprof.lo
libtool: link: cc -shared  -fPIC -DPIC  .libs/xhprof.o    -O2   -Wl,-soname -Wl,xhprof.so -o .libs/xhprof.so
libtool: link: ( cd ".libs" && rm -f "xhprof.la" && ln -s "../xhprof.la" "xhprof.la" )
/bin/bash /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/libtool --mode=install cp ./xhprof.la /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules
libtool: install: cp ./.libs/xhprof.so /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules/xhprof.so
libtool: install: cp ./.libs/xhprof.lai /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules/xhprof.la
libtool: finish: PATH="/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/sbin" ldconfig -n /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules
----------------------------------------------------------------------
Libraries have been installed in:
   /tmp/pear/temp/pear-build-rootoR7EHU/xhprof-0.9.3/modules

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR`
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH` environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR` linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf`

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------

Build complete.
Don't forget to run 'make test'.

running: make INSTALL_ROOT="/tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3" install
Installing shared extensions:     /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr/lib/php5/20090626+lfs/
running: find "/tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3" | xargs ls -dils
277374  4 drwxr-xr-x 3 root root  4096 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3
277396  4 drwxr-xr-x 3 root root  4096 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr
277397  4 drwxr-xr-x 3 root root  4096 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr/lib
277398  4 drwxr-xr-x 3 root root  4096 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr/lib/php5
277399  4 drwxr-xr-x 2 root root  4096 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr/lib/php5/20090626+lfs
277395 84 -rwxr-xr-x 1 root root 82245 Sep 19 22:10 /tmp/pear/temp/pear-build-rootoR7EHU/install-xhprof-0.9.3/usr/lib/php5/20090626+lfs/xhprof.so

Build process completed successfully
Installing '/usr/lib/php5/20090626+lfs/xhprof.so'
install ok: channel://pecl.php.net/xhprof-0.9.3
configuration option "php_ini" is not set to php.ini location
You should add "extension=xhprof.so" to php.ini
```

## 增加xhprof相关配置

### php.ini中增加如下

```
extension=xhprof.so
[xhprof]
xhprof.output_dir=/home/zhangy/xhprof
#设定xhprof结果存放目录
```

### 检查是否安装成功

通常可使用extension_loaded函数进行检测
```
$ php -r "var_dump(extension_loaded('xhprof'));"
```
返回结果为true表示安装成功。
或者

```
$ php -m | grep xhprof
```

如果有匹配结果也表示安装成功了


或者 web 中调用 phpinfo 查看是否成功安装 xhprof（需要重启php服务，如果独立的php-fpm，需重启php-fpm，如果以 module 挂载在 web 服务器上，需重启 web 服务器）
