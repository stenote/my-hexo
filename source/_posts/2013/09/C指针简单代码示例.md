title: C指针简单代码示例
tags:
- C
date: "2013-09-25 14:05:14"
---

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

void main() {

    char str[] = &quot;hello&quot;;

    unsigned int *pstr = (unsigned int *) str;

    unsigned int **ppstr = &amp;pstr;

    printf(&quot;0x%X\n&quot;, *pstr);

    printf(&quot;0x%X\n&quot;, **ppstr);

    printf(&quot;%c\n&quot;, *str);
}
</pre>
