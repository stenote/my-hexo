title: ubuntu上安装mariadb
tags:
- Linux
- MySQL
date: "2013-09-18 23:07:16"
---

mariadb是一个向后兼容、替代MySQL的数据库服务器。

官方介绍：
> MariaDB is a drop-in replacement for MySQL.> 
> 
> MariaDB strives to be the logical choice for database professionals looking for a robust, scalable, and reliable SQL server. To accomplish this, Monty Program works to hire the best and brightest developers in the industry, work closely and cooperatively with the larger community of users and developers in the true spirit of Free and open source software, and release software in a manner that balances predictability with reliability.> 
> 
> Here are some important resources to get you started. All links will redirect you to external sites, noted in parentheses.
<!--more-->
安装mariadb：

**
0、mysql数据库备份：**
可使用mysqldump进行备份，具体备份方法请自行google

**1、增加apt source.list
**
可进入[
https://downloads.mariadb.org/mariadb/repositories/]( https://downloads.mariadb.org/mariadb/repositories/ " https://downloads.mariadb.org/mariadb/repositories/")选择对应的操作系统、操作系统版本代号、mariadb版本号、镜像源地址后页面会显示相关信息：以ubuntu 12.04为例。选择5.5版mariadb（10.0版本mariadb暂时无法使用php进行链接，pdo貌似也不可，而5.5版本mariadb可直接使用mysql_connect等原生mysql数据库操作函数使用）

选择后页面学进行如下操作：

<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
sudo apt-get install python-software-properties
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
sudo add-apt-repository &#039;deb http://mirrors.hustunique.com/mariadb/repo/5.5/ubuntu precise main&#039;

</pre>
也可直接在/etc/apt/source.list.d/中创建问题：mariadb.list（文件需以.list结尾）
写入页面提示的source.list文件，如下：

<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
# MariaDB 5.5 repository list - created 2013-09-18 15:17 UTC
# http://mariadb.org/mariadb/repositories/
deb http://mirrors.hustunique.com/mariadb/repo/5.5/ubuntu precise main
deb-src http://mirrors.hustunique.com/mariadb/repo/5.5/ubuntu precise main
</pre>
**
2、系统update**

<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
sudo apt-get update
</pre>
**
3、安装**
<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
sudo apt-get install mariadb-server-5.5
</pre>
**
4、注意事项：**
①安装过程中会删除系统中现有的mysql数据库，建议备份数据
②安装完成后可能需要重新安装php5-mysql
③安装完成后可直接使用mysql命令登录mariadb，执行语句如下：查看mariadb版本：
<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
MariaDB [(none)]&gt; SELECT VERSION();
+------------------------------+
| VERSION()                    |
+------------------------------+
| 5.5.32-MariaDB-1~precise-log |
+------------------------------+
1 row in set (0.00 sec)

MariaDB [(none)]&gt;
</pre>

至此，mariadb安装完毕。

拓展阅读：

> 官方网站： [https://mariadb.org/](https://mariadb.org/ "https://mariadb.org/")
> 百度百科：[http://baike.baidu.com/view/2521908.htm](http://baike.baidu.com/view/2521908.htm "http://baike.baidu.com/view/2521908.htm")
> 维基百科：[http://en.wikipedia.org/wiki/MariaDB](http://en.wikipedia.org/wiki/MariaDB "http://en.wikipedia.org/wiki/MariaDB")
