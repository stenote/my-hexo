title: C 变量值交换
tags:
- C
date: "2013-09-24 22:16:29"
---

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
#include &lt;stdio.h&gt;

void swap(char *ap, char *bp);

void main() {
    char a = &#039;a&#039;, b = &#039;b&#039;;

    printf(&quot;before swap:\n \ta = %c\t b = %c\n&quot;, a, b);

    swap(&amp;a, &amp;b);

    printf(&quot;after swap:\n \ta = %c\t b = %c\n&quot;, a, b);
}

void swap(char *ap, char *bp) {

    char tmp = *ap;

    *ap = *bp;
    *bp = tmp;
}
</pre>
1、加载标准I/O头文件
2、预先定义swap函数
3、main函数运行
    ①定义变量a b 并赋值
    ②输出交换前变量值
    ③交换
    ④现实交换后变量值

4、swap函数

逻辑：

1、调用swap，传入a, b 变量的地址
2、swap函数中获取a, b 变量地址绑定到ap, bp两个char的指针上
3、进行指针指向地址存储值交换，达到a, b 变量值交换

晚安
