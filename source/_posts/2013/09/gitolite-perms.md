title: gitolite用户、组、库、分支权限管理
date: "2013-09-03 22:48:55"
tags: 
  - Git
---

gitolite配置、简介请挪步[这里](www.mmmmm.io/2013/08/gitolite/)

gitolite提供了丰富的权限管理，可针对单一的用户、群组、分支、库进行设定，来达到不同权限设定。

本文包括如下方面：
1、gitolite组设定
2、gitolite用户、组、分支权限设定
3、公司可能的gitolite配置
4、gitolite权限设置在vim下的配合使用

<!--more-->

我们先列举一个简单的gitolite的config文件，然后再讲解：

<pre class="brush: diff; gutter: true; first-line: 1; highlight: []; html-script: false">
@admin = xiaopei.li cheng.liu jia.huang
@dev = @admin rui.ma jinlin.li linsheng.wu yu.li xuan.zhang

repo lims2
    RWCD   feature/.* @dev
    RW+  master       @master
    RWCD  hotfix      @dev
    RWCD develop      @all

repo test
    RW+               @all
</pre>

1、gitolite组设定

gitolite有如下特性：

①gitolite的组名称需要使用@作为前缀，后跟组名称，通常和程序变量一样，使用非数字作为开头，例如@foo， @bar等均为合法名称
②gitolite系统默认@all表示所有用户。
③gitolite的组成员使用 "=" 进行赋值，成员名称使用空格进行分隔，例如：

<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
@dev = rui.ma jinlin.li yu.li 
</pre>

如上，设定了一个名为@dev的组，包含了名称为rui.ma, jinlin.li, yu.li 三个用户

④一个组中也可包含其他的组，如下：

<pre class="brush: bash; gutter: true; first-line: 1; highlight: []; html-script: false">
@dev = rui.ma jinlin.li yu.li 
@staff = @dev jia.huang cheng.liu
</pre>

如上，初始化了包含rui.ma, jinlin.li, yu.li三个用户的@dev组
@staff组包含了@dev组内的所有成员之外，还包括了jia.huang cheng.liu两个用户

我们查看本文最开始的gitolite配置文件中发现，这个配置文件中配置了两个组（除@all之外），分别为@admin组和@dev组
@admin组中包括了xiaopei.li, cheng.liu, jia.huang三位用户
@dev组中的成员除了@admin组的成员之外，还增加了rui.ma, jinlin.li, linsheng.wu, yu.li, xuan.zhang等几位用户。

2、gitolite用户、组、分支权限设定
gitolite中的权限均是针对repo进行设定的，一个repo开始后到下一个repo之间为这个repo的所有用户、组、分支权限设定。

查看本文最开始的gitolite配置文件，可看到针对名为lims2这个repo共有4条权限设定，而针对test这个repo只有一条权限设定。

针对用户、组、分支的权限需要根据系统提供关键字进行设定，gitolite提供如下几种设定关键字：

C, R, RW, RW+, RWC, RW+C, RWD, RW+D, RWCD, RW+CD

通常使用到的只有C， R， RW, RWD这几种

仅进行简单简介
C: C 为代表创建
R： R 为只读权限
RW： RW 为读写权限
RWD： RWD 为读、写、删权限

举例，我们设定一个用户对master分支只能读取，如下：

<pre class="brush: diff; gutter: true; first-line: 1; highlight: []; html-script: false">
repo test
    R master rui.ma
</pre>
如上，rui.ma这个用户对于test这个repo的master分支只能进行pull操作进行读取，而不能push进行写操作。

困了，晚安。
