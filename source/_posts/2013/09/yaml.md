title: yaml (安装php扩展)
tags:
- PHP
- yaml
date: "2013-09-29 23:29:34"
---

yaml简介：

> YAML: YAML Ain't Markup Language
> 
> 
> What It Is: YAML is a human friendly data serialization
> 
>   standard for all programming languages.

yaml的官方链接：[http://yaml.org/](http://yaml.org/ "http://yaml.org/")

<!--more-->

php中yaml扩展的安装：

```
#使用pecl进行查找
may@server:~$ pecl search yaml
Retrieving data...0%
Matched packages, channel pecl.php.net:
=======================================
Package Stable/(Latest) Local
yaml    1.1.0 (stable)        YAML-1.1 parser and emitter
```

如上，查找到了stable的yaml扩展，安装之前需要安装libyaml-dev，如下：

```
may@server:~$ sudo apt-get install libyaml-dev
```

使用pecl安装yaml的扩展

```
may@server:~$ sudo pecl install yaml
```

安装完成后修改php.ini，增加如下配置：

```
extension=yaml.so
```

PHP官方手册：[PHP:Yaml - Manual](http://cn2.php.net/yaml)
