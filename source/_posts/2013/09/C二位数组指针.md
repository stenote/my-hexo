title: C二维数组指针
tags:
- C
date: "2013-09-25 22:12:30"
---

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">

#include &lt;stdio.h&gt;

void main(int argc, char *argv[]) {

    int i;

    for(i = 1; i &lt; argc; i++) {
        printf(&quot;%s\n&quot;, *(argv + i));
    }
}

</pre>

如上，argc为int值，用于存储arguments的数量
argv为一个二维数组的指针

通过 (argv + i) 获取二维数组的一个数组的指针，使用*获取数组的第一个位置的值，使用%s进行整个数组字符串输出。

结果如下：

<pre class="brush: c; gutter: true; first-line: 1; highlight: []; html-script: false">
$ ./arguments foo bar
foo
bar
</pre>
