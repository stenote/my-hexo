title: php-lua 安装笔记
tags:
- C
- PHP
date: "2013-12-05 23:51:44"
---

php-lua简介：
> "Lua is a powerful, fast, light-weight, embeddable scripting language."> 
> php-lua extension embeds the lua interpreter and offers an OO-API to lua variables and functions.
> 

[php-lua作者在github上开源地址](https://github.com/laruence/php-lua/)

[php-lua在pecl上链接地址](http://pecl.php.net/package/lua)

[php-lua作者博客中对lua有关的博文](http://www.laruence.com/?s=lua&amp;x=-1158&amp;y=-31)

[php-lua作者简单文档](http://www.laruence.com/lua/)

今天对php-lua进行了安装，安装过程中对遇到的部分问题进行了总结，见此

<!--more-->

## 1. 安装pecl、 lua

```
may@Client:~$ sudo apt-get install php-pear lua5.2 liblua5.2-dev
```

如上，
安装 php-pear 用于安装 pecl，便于安装 php 扩展
安装 lua5.2，用于进行 lua 测试
liblua5.2-dev，用于安装 lua5.2 源码文件，编译使用

## 2. 尝试 pecl 安装 php-lua

先进行 php-lua 的检查

```
may@Client:~$ sudo pecl search lua
Retrieving data...0%
Matched packages, channel pecl.php.net:
=======================================
Package Stable/(Latest) Local
lua     1.1.0 (beta)          Embedded lua interpreter
```

如上，检查到 php-lua 的最后版本为 1.1.0，但是是 beta 版本。查看 pecl 网站，发现有 stable 版本，为 1.0.0

直接安装1.1.0beta版本（由于本地php版本为5.4.6，而php-lua的stable 1.0.0版本最多只支持php5.4.0，所以直接安装php-lua 1.1.0）

```
may@Client:~$ sudo pecl install lua-1.1.0
downloading lua-1.1.0.tgz ...
Starting to download lua-1.1.0.tgz (14,896 bytes)
.....done: 14,896 bytes
9 source files, building
running: phpize
Configuring for:
PHP Api Version:         20100412
Zend Module Api No:      20100525
Zend Extension Api No:   220100525
building in /tmp/pear/temp/pear-build-rootMWzytR/lua-1.1.0
running: /tmp/pear/temp/lua/configure
checking for grep that handles long lines and -e... /bin/grep
checking for egrep... /bin/grep -E
checking for a sed that does not truncate output... /bin/sed
checking for cc... cc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables...
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether cc accepts -g... yes
checking for cc option to accept ISO C89... none needed
checking how to run the C preprocessor... cc -E
checking for icc... no
checking for suncc... no
checking whether cc understands -c and -o together... yes
checking for system library directory... lib
checking if compiler supports -R... no
checking if compiler supports -Wl,-rpath,... yes
checking build system type... i686-pc-linux-gnu
checking host system type... i686-pc-linux-gnu
checking target system type... i686-pc-linux-gnu
checking for PHP prefix... /usr
checking for PHP includes... -I/usr/include/php5 -I/usr/include/php5/main -I/usr/include/php5/TSRM -I/usr/include/php5/Zend -I/usr/include/php5/ext -I/usr/include/php5/ext/date/lib -DLARGEFILESOURCE -DFILEOFFSET_BITS=64
checking for PHP extension directory... /usr/lib/php5/20100525+lfs
checking for PHP installed headers prefix... /usr/include/php5
checking if debug is enabled... no
checking if zts is enabled... no
checking for re2c... no
configure: WARNING: You will need re2c 0.13.4 or later if you want to regenerate PHP parsers.
checking for gawk... no
checking for nawk... nawk
checking if nawk is broken... no
checking for lua support... yes, shared
checking for lua in default path... not found
configure: error: Please reinstall the lua distribution - lua.h should be in <lua-dir>/include/
ERROR: `/tmp/pear/temp/lua/configure' failed
may@Client:~$
```

如上，系统报错，遇到了第一个问题， lua.h 在 lua-dir中没有找到，看来不能依赖apt-get和pecl进行php-lua的安装。
通过git clone下来源码，手动尝试进行安装，如下：

```
may@Client:~$ git clone https://github.com/laruence/php-lua.git
Cloning into 'php-lua'...
remote: Counting objects: 115, done.
remote: Compressing objects: 100% (68/68), done.
remote: Total 115 (delta 64), reused 96 (delta 47)
Receiving objects: 100% (115/115), 30.21 KiB, done.
Resolving deltas: 100% (64/64), done.
```

进入php-lua目录，查看config.m4用于初始化配置的文件

发现错误所在地方，php会尝试去 /usr/local 和 /usr目录下查找 include/lua/lua.h文件，没有找到的情况下，停止后续运行。

```
  if test -r $PHPLUA/include/lua.h; then
    LUADIR=$PHPLUA
  else
    ACMSGCHECKING(for lua in default path)
    for i in /usr/local /usr; do
      if test -r $i/include/lua/lua.h; then
        LUADIR=$i
        ACMSGRESULT(found in $i)
        break
      fi
    done
  fi


if test -z "$LUADIR"; then
    ACMSGRESULT(not found)
    ACMSGERROR(Please reinstall the lua distribution - lua.h should be in <lua-dir>/include/)
  fi
```


查看本地/usr/local 和 /usr 目录，发现/usr/include下存在lua5.2的目录，应该属于lua的源码，但是为了区分lua版本，后缀了版本号
做一个软链接继续尝试

```
may@Client:/usr/include$ sudo ln -s lua5.2/ lua
```

重新进行php-lua编译，**执行phpize，创建configure等编译使用文件后执行 ./configure**，出现新的错误提示，如下：

```
checking for lua in default path... found in /usr
checking for lua library in default path... not found
configure: error: Please reinstall the lua distribution - lua library should be in <lua-dir>/lib/
```



提示发现错误发生了变化，lua已经在/usr中找到了，但是 lua-dir/lib中没有找到lua library，查看config.m4文件，如下：

```
 LUALIBNAME=liblua


if test -r $PHPLUA/$PHPLIBDIR/${LUALIBNAME}.${SHLIBSUFFIXNAME} -o -r $PHPLUA/$PHPLIBDIR/${LUALIBNAME}.a; then
    LUALIBDIR=$PHPLUA/$PHPLIBDIR
  else
    ACMSGCHECKING(for lua library in default path)
    for i in /usr/$PHPLIBDIR /usr/lib /usr/lib64; do
      if test -r $i/${LUALIBNAME}.${SHLIBSUFFIXNAME} -o -r $i/${LUALIBNAME}.a; then
        LUALIBDIR=$i
        ACMSG_RESULT(found in $i)
        break
      fi
    done
  fi
```

系统会尝试在/usr/lib usr/lib64目录中查找liblua.so和liblua.a文件。
查看本地/usr/lib目录，发现没有liblua.so文件，但是目录 /usr/lib/i386-linux-gnu中包含 liblua5.2.so文件，同样，做一个软链接

```
may@Client:/usr/lib$ sudo ln -s i386-linux-gnu/liblua5.2.so ./liblua.so
```

尝试重新进行 `./configure`
功能正常

## 3. make && make install
make时，提示如下错误：

```
/home/may/php-lua/php_lua.h:23:17: fatal error: lua.h: No such file or directory
compilation terminated.
make: *** [lua.lo] Error 1
```

考虑直接复制lua.h等相关文件

```
may@Client:~/php-lua$ sudo cp /usr/include/lua/* ./
```

重新make没有发现问题，make install后提示如下：

```
may@Client:~/php-lua$ sudo make install
Installing shared extensions:     /usr/lib/php5/20100525+lfs/
```

至此，安装php-lua完成，后期简单进行 php-lua的ini配置即可。
