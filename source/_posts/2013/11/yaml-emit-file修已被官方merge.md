title: yaml_emit_file修正已被php官方merge
tags:
- PHP
date: "2013-11-17 21:56:42"
---

2个月前写了一个简单的博文，如下：

> [yaml_emit_file无法使用问题解决办法](http://www.mmmmm.io/2013/10/yaml_emit_file%E6%97%A0%E6%B3%95%E4%BD%BF%E7%94%A8%E9%97%AE%E9%A2%98%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95/ "yaml_emit_file无法使用问题解决办法")

今天登录github后发现，已被官方进行了merge，官方merge的commit地址，[见这里](https://github.com/php/pecl-file_formats-yaml/commit/f13af4248caa6a7c62e7a0fe4bb2086e0280f7e9)，我进行pull request地址，[见这里](https://github.com/php/pecl-file_formats-yaml/pull/1)。

并且官方增加了一个phpt的单元测试文件：[tests/yaml_emit_file_basic.phpt](https://github.com/php/pecl-file_formats-yaml/blob/master/tests/yaml_emit_file_basic.phpt)，用于对该函数进行测试。

查看了pecl上的[yaml库](http://pecl.php.net/package/yaml)，发现未更新，等官方同步吧。

_
#官方于11月18日进行了更新
[http://pecl.php.net/package/yaml](http://pecl.php.net/package/yaml)
_
