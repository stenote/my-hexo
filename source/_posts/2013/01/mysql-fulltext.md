title: MySQL fulltext
tags:
- MySQL
date: "2013-01-15 05:49:38"
---

<div>mysql本身提供了全局索引。是通过一个类型为fulltext的key进行创建索引，然后进行匹配的。如下，我们创建一个新的表：</div>
<div>mysql &gt; create table user (</div>
<div>id int not null auto_increment primary key, //创建一个int的列，为主键，自动增加</div>
<div>name varchar(20) not null default '',</div>
<div>title varchar(20) not null default '',</div>
<div>fulltext name(name), //第一个name的fulltext索引</div>
<div>fulltext title(title),  //第二个title的fulltext索引</div>
<div>fulltext name_and_title(name, title)  //第三个name_and_title的索引</div>
<div>) engine = myisam default charset = utf8; //需要创建引擎为myisam</div>
<div>
</div>
<div>正常我们进行一列的搜索是使用通配符，如下：</div>
<div>mysql &gt; select * from user where title like '%usertitle%';</div>
<div>mysql &gt; select * from user where name like '%usertitle%'; </div>
<div>mysql &gt; select * from user where title like '%usertitle%' or name like '%usertitle%';</div>
<div>
</div>
<div>或者使用fulltext</div>
<div>mysql &gt; select * from user where match(title) against('usertitle');</div>
<div>mysql &gt; select * from user where match(name) against('usertitle');</div>
<div>mysql &gt; select * from user where match(title, name) against('usertitle');</div>
<div>
</div>
<div>全文索引特性如下：</div>
<div>

<span>排除不完整单词</span>

<span>排除长度少于</span>4<span>个字符（</span>3<span>个或者更少）的词</span>

<span>排除在一半以上的行中都出现的词（这意味着至少要有</span>3<span>行）</span>

<span>用连字号连起来的词</span>(Hyohenated)<span>做为两个词对待</span>

<span>结果行按照优先级降序排列</span>

需要注意的是，如果要单一对一列进行fulltext match，该列需要有单一的fulltext index，如果是多列，则这多列也应该有合并的fulltext index，如上，我们有title name的单一的fulltext index，还有title和name合并在一起的fulltext index，但是对于id，没有fulltext index，所以无法进行fulltext的match操作。

</div>
