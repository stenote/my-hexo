title: MySQL limit
tags:
- MySQL
date: "2013-01-13 01:20:05"
---

<div>mysql limit可进行结果的截取</div>
<div>
</div>
<div>mysql &gt; select * from user;</div>
<div>检索出所有user</div>
<div>mysql &gt; select * from user limit 1;</div>
<div>检索出user中的一个结果</div>
<div>mysql &gt; select * from user limit 0, 1;</div>
<div>检索出user中的一个结果，是第0位那个user</div>
<div>mysql &gt; select * from user limit 1 offset 0;</div>
<div>和上面结果一样</div>
