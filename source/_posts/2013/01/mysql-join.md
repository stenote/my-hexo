title: MySQL join
tags:
- MySQL
date: "2013-01-15 04:03:53"
---

<div>mysql中通常提供3种join，但是书写上是4中join，分别是left join, right join, join, inner join</div>
<div>在mysql中inner join和join的结果是一样的</div>
<div>
</div>
<div>如下两个表：</div>
<div></div>
<div>执行普通join结果如下：</div>
<div></div>
<div><span style="color: #ff6600">join和inner join 会把所有匹配的结果进行保留并显示出来</span></div>
<div>
</div>
<div>如下为left join：</div>
<div></div>
<div>发现会保留deparment的全部数据，然后用employee按照on后的条件进行匹配，如果部分结果未匹配上，则显示为null</div>
<div>
</div>
<div>如下为right join:</div>
<div></div>
<div>保留了emplyee的所有数据，然后用department按照on后的条件进行匹配，如果部分结果未匹配上，则显示null</div>
