title: MySQL字符集、collation
tags:
- MySQL
date: "2013-01-08 05:27:20"
---

<div>mysql中对于字符串会进行存储类型进行分类，分为二进制、非二进制，通常二进制存储的都是纯二进制文本，例如mp3、图像文件等，通常设定类型为blob、binary、varbinary</div>
<div>也可存储非二进制文本，通常类型为varchar、char、text</div>
<div>
</div>
<div>通常对于二进制文本的存储，会设计到mysql字符集和collation，下面是字符集和collation的简单介绍</div>
<div>
</div>
<div>字符集即为二进制文本的存储类型，通常可通过</div>
<div>mysql &gt; show character set;</div>
<div>mysql &gt; show charset;</div>
<div>进行查看</div>
<div>系统会列出所有支持的编码</div>
<div>当然，我们也可查看某个表的character set（字符集）</div>
<div>mysql &gt; show create table user;</div>
<div>

CREATE TABLE `user` (
 `User` char(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
 `Host` char(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1

latin1即为user table 的character set(charset)

</div>
<div>对于字符集的存储后，我们可能需要对字符进行排序，这个时候就用到了collation</div>
<div>
</div>
<div>collaction是针对字符集来存在的，通常排序有3种不同的方式，ci cs bin </div>
<div>ci capital ignore  无视大写</div>
<div>cs captial sensitive 大写敏感</div>
<div>bin 二进制排序</div>
<div>可以使用</div>
<div>mysql &gt; show table statusG;</div>
<div>查看collation</div>
<div>

Name: user
 Engine: MyISAM
 Version: 10
 Row_format: Fixed
 Rows: 6
 Avg_row_length: 229
 Data_length: 1374
Max_data_length: 64457769666740223
 Index_length: 1024
 Data_free: 0
 Auto_increment: NULL
 Create_time: 2012-12-29 15:54:58
 Update_time: 2012-12-29 15:54:58
 Check_time: NULL
 Collation: latin1_swedish_ci
 Checksum: NULL
 Create_options: 
 Comment:

可以看到它的collation为latin1_swedish_ci，由于collation命名中包含了charset，我们也可知道该表的charset为latin1;

我们可以使用

mysql &gt; show collation

查看所有collation

或者使用

mysql &gt; show collation like 'latin1%';

查看latin1的collation

通常我们进行order by的时候，都是使用默认collation，如上，使用了默认的 latin1_swedish_ci

我们可以对order by的时候增加特定的collation，如下：

mysql &gt; select * from t order by c collate latin1_general_cs;

我们在对t搜索时使用c的latin1_general_cs进行排序。

我们可以子创建表时候设定表的charset和collate

mysql &gt; create table user (

name varchar(10) character set utf8 collate utf8_general_ci

) engine = innodb default charset = utf8

需要注意的是，我们在设定某个table中column的charset collate时，需要注意table的charset；

我们可以使用collation() charset() 两个方法进行collation character set的获取

可以使用convert进行charset的修改

如下：

mysql &gt; select collation(user());

mysql &gt; select charset(user());

mysql &gt; select convert(user() using binary);

由于convert后会直接显示在终端或返回字符串，可使用如下命令进行验证

mysql &gt; select charset(convert(user() using utf8));

mysql &gt; select collation(convert(user() usiing utf8));

修改表的character set

alter database test character set utf8;

修改表的character set

alter table employee character set utf8;

修改列character set

alter table employee modify name varchar(50) character set utf8;

</div>
