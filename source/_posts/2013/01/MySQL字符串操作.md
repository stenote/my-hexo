title: MySQL字符串操作相关函数
tags:
- MySQL
date: "2013-01-04 06:18:00"
---

<div>mysql提供了部分用于操作字符串的函数，如下：</div>
<div>mid()</div>
<div>left()</div>
<div>right()</div>
<div>substring()</div>
<div>substring_index()</div>
<div>concat()</div>
<div>locate()</div>
<div>lenght()</div>
<div>char_length()</div>
<div>
</div>
<div>left、right函数功能相似，用于进行字符串截取，区别是一个从左侧开始截取，一个从右侧开始截取，如下：</div>
<div>mysql &gt; select left('hello world', 5);</div>
<div>hello</div>
<div>mysql &gt; select right('hello world', 5);</div>
<div>world</div>
<div>
</div>
<div>left、right的第一个参数是用于进行截取的字符串，第二个参数是截取的字符串长度</div>
<div>
</div>
<div>mid、substring是功能完全相同的两个函数，实际上，mid是substring的别名，该函数也用于字符串截取，需要传入需截取的字符串、其实字符位置，截取字符长度</div>
<div>
</div>
<div>例如：</div>
<div>mysql &gt; mid('hello world', 5, 6);</div>
<div>o worl</div>
<div>结果如上，可验证第三个参数为字符串长度，而非截取字符串结束位置</div>
<div>
</div>
<div>concat()函数会将传入的结果进行连接</div>
<div>mysql &gt; concat('hello', 'world');</div>
<div>helloworld</div>
<div>可以理解为</div>
<div>function concat() {</div>
<div>    return (string) implode('', func_get_args());</div>
<div>}</div>
<div>
</div>
<div>substring_index()用于截取字符串中某个字符左侧或者右侧的所有字符串</div>
<div>例如：</div>
<div>mysql &gt; select substring_index('hello world', 'e', 1);</div>
<div>h</div>
<div>mysql &gt; select substring_index('hello world, 'e', -1);</div>
<div>llo world</div>
<div>如上，第三个参数设定为1则返回左侧字符串，设定为-1则返回右侧的字符串</div>
<div>
</div>
<div>如果第二个参数在字符串中不存在，则返回所有字符串</div>
<div>

mysql &gt; select substring_index('hello world', 'o', 1);

hell 

</div>
<div>select substring_index('hello world', 'o', -1);</div>
<div>rld  </div>
<div>
</div>
<div>如上，可发现，对于一个字符多次在字符串中出现，则会根据第三个参数，取最左侧或者最右侧的字符位置作为用于截取字符串的位置；</div>
<div>
</div>
<div>locate()用于对字符串进行查找</div>
<div>mysql &gt; select locate('wo', 'hello world');</div>
<div>1</div>
<div>mysql &gt; select locate('he', hello world' ,4);</div>
<div>0</div>
<div>第一个参数为要查找的字符串</div>
<div>第二个参数为被查找的字符串</div>
<div>第三个参数是开始进行查找的起始位置</div>
<div>
</div>
<div>length()、char_length()都会返回字符串长度，不过不同的是，length不会按照字符编码进行换算，char_length会进行换算，如下：</div>
<div>mysql &gt; select lenght('中国');</div>
<div>6</div>
<div>mysql &gt; select char_length('中国');</div>
<div>2</div>
