title: git tag
tags:
- Git
date: "2013-01-03 20:11:34"
---

<div>git 中 tag是通常用于对某个提交进行人为标识的命令，通常执行方法为</div>
<div>git tag tag_name SHA1</div>
<div>例如：</div>
<div>git tag foobar c326020</div>
<div>即可为c326020的位置添加一个名称为foobar的tag</div>
<div>
</div>
<div>我们可以进入.git/refs/tags/中查看到所有的tag，其中存在foobar这个文件，文件内容为c326020d24e6459cd3345a2cbeff605a2af6c673，即为c326020的完整名称</div>
<div>
</div>
<div>可以使用git tag -l查看所有的tag，-l即为list的意思，可以考虑不使用-l命令，直接进行查看当前系统中所有的tag</div>
<div>
</div>
<div>对于已经创建了的tag，我们需要进行删除，可使用</div>
<div>git tag -d tag_name来删除</div>
<div>例如：</div>
<div>git tag -d foobar</div>
<div>会删除名称为foobar的tag</div>
<div>
</div>
<div>创建一个有备注信息的tag，可使用如下命令：</div>
<div>git tag -a tag_name SHA1 -m 'message'</div>
<div>这样会创建一个名称为tag_name指向SHA1的tag，备注信息为'message'</div>
<div>
</div>
<div>如果已经设定了core.editor，我们可以使用</div>
<div>git tag -a tag_name SHA1，然后创建tag后，会自动打开editor，让用户填写tag的message</div>
<div>
</div>
<div>查看某个tag的详细信息，可以使用git show tag_name</div>
<div>
</div>
<div>如果创建一个tag时，未设定tag的message，则会以tag对应的commit的message作为tag的message</div>
<div>
</div>
<div>git tag -n可查看到所有的tag以及tag对应的message信息</div>
<div>
</div>
<div>tag提交，可以使用</div>
<div>git push --tags</div>
<div>
</div>
<div>tag同步，可以使用</div>
<div>git fetch --tags</div>
