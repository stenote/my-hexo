title: MySQL事务
tags:
- MySQL
date: "2013-01-16 04:41:51"
---

<div>mysql部分引擎支持事务，通常支持事务所使用的引擎为innodb</div>
<div>1、查看一个表的引擎：</div>
<div>可直接使用show create table table_name命令，直接查看表的引擎</div>
<div>2、修改一个表的引擎：</div>
<div>alter table table_name engine = innodb;</div>
<div>如上，会设定table_name这个table 的engine为innodb</div>
<div>需要注意的是，修改table engine时，可能由于key的问题导致修改失败，必要时请查看相关错误提示（例如myisam的fulltext key 就不能在innodb中使用，可以考虑删除fulltext index)</div>
<div>
</div>
<div>
</div>
<div>事务通常包括如下一个操作：</div>
<div>1、开启事务</div>
<div>2、执行事务内操作</div>
<div>3、保存事务</div>
<div>4、回滚事务</div>
<div>5、保存回滚点</div>
<div>
</div>
<div>通常执行为</div>
<div>mysql &gt; begin; //start transaction;也行</div>
<div>开启了一个事务</div>
<div>mysql &gt; savepoint start;</div>
<div>创建了一个回滚点</div>
<div>mysql &gt; insert into user values('mario');</div>
<div>插入一条数据</div>
<div>mysql &gt; savepoint insert_a;</div>
<div>mysql &gt; insert into user values('may');</div>
<div>插入第二条数据</div>
<div>mysql &gt; savepoint insert_b;</div>
<div>mysql &gt; insert into user values('foobar');</div>
<div>插入第三条数据</div>
<div>mysql &gt; savepoint insert_c;</div>
<div>mysql &gt; rollback to insert_b;</div>
<div>这样，新插入的foobar就会被删除</div>
<div>mysql &gt; rollback to insert_a</div>
<div>新插入的may会被删除</div>
<div>此时不能再rollback to insert_b</div>
<div>只能继续rollback或者commit</div>
<div>
</div>
<div>需要注意的是，事务的rollback，是针对数据的rollback，如果表的机构发生变化，那么无法rollback回去了</div>
<div>mysql 中，truncate语句的显示形式是清空了表数据，实际上是进行了表的drop，然后重新创建了表，所以表内的数据是空的，此时使用rollback，尝试将数据恢复是不可能的了，但是如果使用delete from table_name,则会将所有的记录删除，再在不修改表结构的情况下，使用rollback命令，可恢复之前已经delete的数据了。</div>
<div>
</div>
<div>commit可设定为自动commit，如果发生数据变更，则立即更新commit，通常设定为：</div>
<div>set autocommit = 0;即可</div>
<div>不建议使用autocommit</div>
<div>
</div>
