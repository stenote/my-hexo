title: Git flow 简易架设和简易内容提交
tags:
- Git
date: "2013-01-06 00:51:40"
---

<div>Git flow 是一种先进的git 版控制模型，可以针对各种类型项目开发进行管理</div>
<div>git flow 相关网站：[https://github.com/nvie/gitflow](https://github.com/nvie/gitflow)</div>
<div>
</div>
<div>1、安装git flow</div>
<div>
</div>
<div>首先，在A（server）、B（custom）、C（custom）上安装git flow再进行其他步骤</div>
<div>安装方法</div>
<div>$ git clone https://github.com/nvie/gitflow.git    //下载git flow</div>
<div>$ cd gitflow                                                        //切换至git flow 目录</div>
<div>$ git submodule init                                            //submodule 初始化</div>
<div>$ git submodule update                                       //submode  升级</div>
<div>$ sudo make install                                             //安装git flow</div>
<div>$ sudo cp git-flow /etc/bash_completion.d/         //设定git flow 的 completion</div>
<div>
</div>
<div>或者使用</div>
<div>$ sudo apt-get install git-flow</div>
<div>
</div>
<div>2、本地进行git flow 开发</div>
<div>
</div>
<div>在A、B、C端都安装git flow 后，在A服务端git init后git flow init -d ，创建默认git flow</div>
<div>B、C端 git clone后，再在本地git flow init -d</div>
<div>
</div>
<div>$ git branch 查看B端branch</div>
<div>* develop</div>
<div>   master</div>
<div>可见，现在所在的branch为develop，即开发分支</div>
<div>在该分支上直接进行开发后尝试push，可正常push</div>
<div>在C端pull一下尝试，可正常将develop branch上的数据pull下来</div>
<div>
</div>
<div>B端进行新功能开发</div>
<div>$ git flow feature start update_eq</div>
<div>$ git branch</div>
<div>    develop</div>
<div>*  feature/update_eq</div>
<div>    master</div>
<div>
</div>
<div>blablabla....进行相关开发后add修改的文件并commit</div>
<div>
</div>
<div>$ git flow feature finish update_eq</div>
<div>blabla相关git flow 的提示信息，大致内容为，已经切换至branch分支，并合并branch develop 和 feature/update_eq</div>
<div>
</div>
<div>$ git push</div>
<div>相关push正常的提示信息</div>
<div>
</div>
<div>我们用C端进行git pull，发现可正常pull下来相关数据，并且代码正常</div>
<div>
</div>
<div>但是这样做，是直接将代码放入了develop branch中，有些时候，可能某些代码需要team leader进行相关review后才可进行merge到develop 分支中。git flow提供了一个类似功能</div>
<div>
</div>
<div>$ git flow feature start new</div>
<div>$ git branch </div>
<div>    develop</div>
<div>*  feature/new</div>
<div>    master</div>
<div>
</div>
<div>blablabla 进行相关开发后add修改的文件并commit</div>
<div>
</div>
<div>$ git flow feature publish new</div>
<div>
</div>
<div>页面提示，远程端已创建新的branch feature/new，并且代码已正常push上去</div>
<div>
</div>
<div>我们使用C端pull一下后查看远程branch</div>
<div>$ git pull</div>
<div>$ git branch -a</div>
<div>发现有 feature/new 这个branch</div>
<div>
</div>
<div>但是查看develop这个log，发现没有new上次的修正</div>
<div>我们切换到feature/new 这个branch</div>
<div>$ git checkout feature/new</div>
<div>$ git log </div>
<div>我们可以发现刚在B端在feature/new这个branch上的开发的代码</div>
<div>team leader进行review后</div>
<div>$ git checkout develop</div>
<div>$ git merge feature/new</div>
<div>提示merge成功，如果不成功手动修正冲突代码</div>
<div>完成后。在B、C端删除本地和远程多余分支</div>
<div>本地删除</div>
<div>$ git branch -d feature/new</div>
<div>远程</div>
<div>$ git push origin :feature/new</div>
<div>git push的时候需要当前没有任何未提交的commit </div>
<div>此时执行git push其实就是push空的提交到远程的:feature/new这个branch</div>
<div>相当于删除该branch</div>
<div>
</div>
<div>
</div>
<div>
</div>
