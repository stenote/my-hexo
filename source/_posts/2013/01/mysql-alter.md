title: MySQL alter
tags:
- MySQL
date: "2013-01-05 07:56:59"
---

<div>mysql alter 用于修改表的相关列类型、名称、键等相关信息</div>
<div>如下：</div>
<div>修改表名称</div>
<div>alter table user rename foobar;</div>
<div>rename table user to foobar;</div>
<div>这两个命令效果相同</div>
<div>
</div>
<div>增加一列</div>
<div>alter table user add id int not null default 0;</div>
<div>
</div>
<div>在开头增加一列</div>
<div>alter table user add id0 int not null default 0 first;</div>
<div>
</div>
<div>在某列后增加一列</div>
<div>alter table user add school_id not null defaullt 0 after id;</div>
<div>
</div>
<div>删除某列</div>
<div>alter table user drop id;</div>
<div>alter table user drop school_id</div>
<div>
</div>
<div>修改某列</div>
<div>alter table user modify school_name varchar(20);</div>
<div>alter table user change id uid int not null;</div>
<div>change id 为uid int not null;</div>
<div>
</div>
