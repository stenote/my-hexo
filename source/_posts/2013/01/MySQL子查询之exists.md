title: MySQL 子查询之 exists
tags:
- MySQL
date: "2013-01-15 04:03:47"
---

<div>[http://www.cnblogs.com/zemliu/archive/2012/10/12/2722004.html](http://www.cnblogs.com/zemliu/archive/2012/10/12/2722004.html)</div>
<div>[http://www.nowamagic.net/librarys/veda/detail/639](http://www.nowamagic.net/librarys/veda/detail/639)</div>
<div>
</div>
<div>exists的执行，相当与对select的结果，每个都进行子查询，判断是否存在，存在则返回结果，不存在抛弃</div>
<div>select * from user </div>
<div>where exists (select * from school where school.id = user.id);</div>
<div>相当于，将user的每个结果都获取出来，然后传入select * from school where school.id = user.id（此时user.id)已被替换为新的值进行判断，是否有结果，如果有结果，则把user的这次结果放入最终结果中去，如果没有，则不放入最终结果</div>
<div>该查询可以理解为left join</div>
<div>select * from user left join school on (school.id = user.id);</div>
