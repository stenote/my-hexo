title: MySQL group by
tags:
- MySQL
date: "2013-01-13 01:13:23"
---

<div>mysql的order by 是进行排序的，group by 是进行分组的</div>
<div>分组后也可通过having进行分组结果过滤，也支持or 、and 等多条件过滤</div>
<div>
</div>
<div>如下：有数据信息：</div>
<div></div>
<div>我们需要进行d_id的分组，可进行如下操作：</div>
<div></div>
<div>我们按照d_id进行分组，用count(d_id)进行标记分类后该类目下的数据。</div>
<div></div>
<div>如图，又增加了having 进行分组后结果的的过滤</div>
<div>
</div>
<div>我们如下，分组后进行or过滤</div>
<div></div>
<div>如图，检索出了count(d_id)为2，或者num为1的结果；</div>
<div>
</div>
<div>我们进行and检索如下：</div>
<div></div>
<div>按照d_id进行分组，然后count进行分组结果，最后进行count(d_id)的过滤，num的过滤</div>
<div>
</div>
