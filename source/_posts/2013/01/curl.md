title: curl
tags:
- PHP
date: "2013-01-27 07:52:16"
---

<div>php提供了一些列用于进行curl操作的函数。通常用户rpc请求。</div>
<div>curl安装：</div>
<div>$ sudo apt-get install php5-curl</div>
<div>安装完成后查看/etc/php5/conf.d/20-curl.ini发现加载了extension=curl.so</div>
<div>
</div>
<div>curl的函数操作通常包括如下几个方面：</div>
<div>1、初始化</div>
<div>2、设定参数</div>
<div>3、执行操作</div>
<div>4、释放</div>
<div>
</div>
<div>初始化：</div>
<div>$ch = curl_init();</div>
<div>通过curl_init()函数初始化curl操作，该函数返回curl操作句柄</div>
<div>
</div>
<div>设定curl相关参数</div>
<div>
</div>
<div>curl_setopt($curl_handler, $option_name, $option_value);</div>
<div>curl_setopt_array($curl_handler, array($option_name=&gt; $option_value);</div>
<div>例如，初始化:</div>
<div>$ch = curl_init();</div>
<div>curl_setopt($ch, CURLOPT_URL, 'http://wordpres'); //请求url</div>
<div>curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/curl.cookie');  //存储的cookie文件，便于多次发送curl请求</div>
<div>curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/curl.cookie'); //加载cookie文件，通常用于第二次curl请求</div>
<div>curl_setopt($ch, CURLOPT_POST, TRUE); //设定开启post请求</div>
<div>curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'name'=&gt;'mario', 'password'=&gt; 'foobar')); //设定post数据</div>
<div>curl_setopt($ch, CURLOPT_TIMEOUT, 5); //设定超时时长。</div>
<div>curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); //设定模拟的浏览器</div>
<div>curl_setopt($ch, CURLOPT_CONNECTIONTIMEOUT, 5); //设定连接超时时长</div>
<div>
</div>
<div>curl设定参数可以在</div>
<div>[http://cn2.php.net/manual/zh/function.curl-setopt.php](http://cn2.php.net/manual/zh/function.curl-setopt.php)</div>
<div>查看。通常还需设定</div>
<div>
</div>
<div>
</div>
<div>执行操作</div>
<div>curl_exec($ch);</div>
<div>通常情况下，curl_exec()后会直接显示返回结果。但是可通过</div>
<div>curl_setopt($ch, CURLOPT_RETURNTRANSFER， TRUE) ，让返回结果不直接输出，而返回字符串。</div>
<div>
</div>
<div>执行结束后，curl_exec会根据不同的设定类型返回不同的结果（或者直接以标准输出相关信息），我们可以使用</div>
<div>curl_errno curl_error来获取相关error。</div>
<div>
</div>
<div>
</div>
<div>释放curl_close()</div>
<div>直接传入创建的curl句柄即可</div>
<div>curl_close($ch);</div>
<div>
</div>
<div>
</div>
