title: MySQL primary key
tags:
- MySQL
date: "2013-01-05 07:31:27"
---

<div>mysql中对于单一的表，我们可设定该表具有多个主键（primary key)</div>
<div>主键唯一、不能为空</div>
<div>创建主键：</div>
<div>create table user(</div>
<div>id int(10) not null primary key,</div>
<div>name varchar(20)</div>
<div>);</div>
<div>多主键</div>
<div>create table user(</div>
<div>id int(10),</div>
<div>name varchar(20),</div>
<div>primary key (id, name)</div>
<div>);</div>
<div>primary key 会自动增加not null 和default</div>
<div>primary key 不能重复</div>
<div>
</div>
<div>删除primary key </div>
<div>
</div>
<div>alter table drop primary key;</div>
<div>不能对单一的primary key 进行删除，修改表时，只能删除所有的primary key</div>
<div>
</div>
<div>增加primary key</div>
<div>alter table user add primary key(id);</div>
<div>alter table user add primary key (id, name);</div>
<div>
</div>
<div>需要注意的是，drop primary key 时，如果primary key为auto_increment，则需要删除auto_increment，可以使用</div>
<div>
</div>
<div>alter table user modify id int(10);</div>
<div>alter table user drop primary key;</div>
<div>
</div>
<div>在添加primary key时，如果原始数据本身存在了相同的值，则会导致无法正常添加primary key;</div>
<div>
</div>
<div>
</div>
