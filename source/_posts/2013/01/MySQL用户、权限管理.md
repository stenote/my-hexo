title: MySQL 用户、权限管理
tags:
- MySQL
date: "2013-01-04 01:40:33"
---

<div>mysql可针对不同的用户设定针对不同的数据库、表进行操作的权限</div>
<div>可针对用户根据不同的访问方式，设定的不同的密码</div>
<div>
</div>
<div>用户存储在</div>
<div>mysql.user表中</div>
<div>权限等相关信息存储在information_schema中</div>
<div>
</div>
<div>通常针对不同的用户曾加，需要设定不同的权限</div>
<div>通常设定层级包括</div>
<div style="margin-left: 30px">用户级</div>
<div style="margin-left: 30px">数据库级</div>
<div style="margin-left: 30px">表级</div>
<div style="margin-left: 30px">行列级</div>
<div style="margin-left: 30px"></div>
<div>例如，如果我们设定某个用户可进行登录mysql服务器，某个用户不可登录mysql服务器，某个用户只能通过某个host登录mysql服务器，这类的操作通常属于用户级的权限设定。通常设定包括如下几个方面:</div>
<div>1、用户创建</div>
<div>2、用户加密</div>
<div>3、用户更名</div>
<div>4、用户删除</div>
<div>
</div>
<div>1、用户创建</div>
<div>mysql中用户需要设定用户名称、登录用户所存储的host地址，可以为某个hostname，ip地址，也可为%全局匹配。</div>
<div>创建一个用户时候，至少需要用户名称，也可同时设定用户hostname</div>
<div>mysql &gt; create user 'may'@'192.168.1.15';</div>
<div>创建了一个可以从192.168.1.15以名称为may的用户访问当前mysql数据库的用户，由于该用户没有设定密码，所以用户可使用空密码访问系统</div>
<div>
</div>
<div>mysql &gt; create user 'anyone';</div>
<div>创建了一个可从任意地址以anyone访问当前mysql数据库的用户，该用户未设定hostname，则系统会以%来表示用户的hostname，在mysql中，%匹配任意字符串，即可匹配任意hostname，由于用户未设定密码，所以该用户也可使用空密码访问系统</div>
<div>该命令于如下命令作用一样；</div>
<div>mysql &gt; create user 'anyone'@'%';</div>
<div>
</div>
<div>2、用户加密</div>
<div>上述用户创建后，由于未设定用户密码，由于某些未设定密码的用户可能可对数据库进行相关操作，所以可能导致用户帐号冒用无密码用户登录服务器，进行部分恶意操作，为安全起见，需要设定用户密码。</div>
<div>用户密码设定分为三个方面：</div>
<div style="margin-left: 30px">1、创建用户时设定用户密码</div>
<div style="margin-left: 60px">mysql &gt; create user 'may'@'192.168.1.15' identified by 'password';</div>
<div style="margin-left: 60px">创建一个可从192.168.1.15以用户为may，密码为password访问的用户。</div>
<div style="margin-left: 30px">2、为已创建的用户设定密码</div>
<div style="margin-left: 60px">mysql &gt; create user 'may'@'192.168.1.15';</div>
<div style="margin-left: 60px">mysql &gt; set passrod for 'may'@'192.168.1.15' = password('new_password');</div>
<div style="margin-left: 60px">修改'may'@'192.168.1.15'的密码为new_password，password()函数是用于进行mysql用户密码加密的函数</div>
<div style="margin-left: 60px">mysql &gt; set password = password('new_password');</div>
<div style="margin-left: 60px">如果未指定用户信息，则会按照当前用户进行密码修改</div>
<div style="margin-left: 60px">mysql &gt; set password = password('');</div>
<div style="margin-left: 60px">如上可设定当前用户密码为空</div>
<div style="margin-left: 30px">3、为用户设定权限时设定密码</div>
<div style="margin-left: 30px">该命令见下；</div>
<div>
</div>
<div>3、用户更名</div>
<div>由于管理员用户名称填写错误、用户名称变更、用户hostname变更等相关情况存在，所以需要对用户名称、hostname进行修改。</div>
<div>通常修改使用如下命令：</div>
<div>mysql &gt; rename user 'may'@'192.168.1.15' to 'mario'@'192.168.1.15';</div>
<div>修改用户后用户原有的权限不会发生变化</div>
<div>
</div>
<div>4、用户删除</div>
<div>用户删除和创建用户时命令类似，如下：</div>
<div>mysql &gt; drop user 'mario'@'192.168.1.15';</div>
<div>删除对192.168.1.15访问的名称为'mario'的用户的授权</div>
<div>
</div>
<div>数据库级、表级、列级的用户权限的设定都属于用户权限设定，只不过针对设定的操作层级不同进行了分类，以下进行统一归纳：</div>
<div>用户权限设定通常使用grant命令进行设定，大致格式如下：</div>
<div><span>grant</span><span> 权限 </span><span>on</span><span> 数据库对象 </span><span>to</span><span> 用户</span></div>
<div>例如：</div>
<div>grant all privileges on *.* to 'may'@'192.168.1.15';</div>
<div>为'may'@'192.168.1.15'设定具有针对任意数据库的任意表有任意权限：</div>
<div>
</div>
<div>设定数据库层级的用户权限如下：</div>
<div>grant all privileges on db.* to 'may'@'192.168.1.15';</div>
<div>为‘may'@'192.168.1.15'设定用户具有针对db数据库的任意表具有任意权限；</div>
<div>
</div>
<div>设定表层级的用户权限如下：</div>
<div>grant all privileges on db.table to 'may'@'192.168.1.15';</div>
<div>为'may'@'192.168.1.15'设定用户具有针对db的table表的具有任意权限；</div>
<div>
</div>
<div>设定列层级的用户权限如下：</div>
<div>grant select(Host) on mysql.user to 'may'@'192.168.1.15';</div>
<div>为'may'@'192.168.1.15'设定用户具有针对mysql的user表具有select host的权限；</div>
<div>
</div>
<div>设定用户权限后，用户可通过</div>
<div>mysql &gt; show grants;</div>
<div>命令查看当前用到权限，或者</div>
<div>mysql &gt; show grants for 'mario'@'192.168.1.15';</div>
<div>
</div>
<div>设定用户权限后需要注意以下几点：</div>
<div style="margin-left: 30px">1、设定权限给某个用户后，如果用户在系统中不存在，会创建新用户</div>
<div style="margin-left: 30px">2、设定权限后需要flush privileges</div>
<div style="margin-left: 30px">3、如果被设定权限的用户已登录在服务器，则需要用户重新登录，新权限才会生效</div>
<div style="margin-left: 30px"></div>
<div>用户权限设定后需要删除用户权限，可使用revoke命令，如下；</div>
<div>mysql &gt; revoke all on *.* from 'mairo'@'192.168.1.15';</div>
<div>revoke的命令和grant命令相似，只需要修改to为from即可。</div>
<div>
</div>
<div>常用可进行设定的权限如下：</div>
<div>
</div>
<div>
<div>
<table width="553" border="1" cellspacing="0" cellpadding="0"><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span>权限</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>描述</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">ALL PRIVILEGES</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响除<span lang="EN-US">WITH GRANT OPTION</span>之外的所有权限</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">ALTER</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">ALTER TABLE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">ALTER ROUTINE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响创建存储例程的能力</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">CREATE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">CREATE TABLE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">CREATE ROUTINE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响更改和弃用存储例程的能力</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">CREATE TEMPORARY TABLES</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">CREATE TEMPORARY TABLE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">CREATE USER</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响创建、弃用；重命名和撤销用户权限的能力</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">CREATE VIEW</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">CREATE VIEW</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">DELETE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">DELETE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">DROP</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">DROP TABLE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">EXECUTE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响用户运行存储过程的能力</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">EVENT</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响执行事件的能力（从<span lang="EN-US">MySQL 5.1.6</span>开始）</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">FILE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SELECT INTO OUTFILE</span>和<span lang="EN-US">LOAD DATA INFILE</span>的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">GRANT OPTION</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响用户委派权限的能力</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">INDEX</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">CREATE INDEX</span>和<span lang="EN-US">DROP INDEX</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">INSERT</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">INSERT</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">LOCK TABLES</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">LOCK TABLES</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">PROCESS</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SHOW PROCESSLIST</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">REFERENCES</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>未来<span lang="EN-US">MySQL</span>特性的占位符</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">RELOAD</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">FLUSH</span>命令集的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">REPLICATION CLIENT</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响用户查询从服务器和主服务器位置的能力</span>

</td></tr></table>
</div>
<div>
<table width="553" border="1" cellspacing="0" cellpadding="0"><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span>权限</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>描述</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">REPLICATION SLAVE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>复制从服务器所需的权限</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">SELECT</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SELECT</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">SHOW DATABASES</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SHOW DATABASES</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">SHOW VIEW</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SHOW CREATE VIEW</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">SHUTDOWN</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">SHUTDOWN</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">SUPER</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响管理员级命令的使用，如<span lang="EN-US">CHANGE</span>、<span lang="EN-US">MASTER</span>、<span lang="EN-US">KILL thread</span>、<span lang="EN-US">mysqladmin debug</span>、<span lang="EN-US">PURGE MASTER LOGS</span>和<span lang="EN-US">SET GLOBAL</span></span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">TRIGGER</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响执行触发器的能力（从<span lang="EN-US">MySQL5.1.6</span>开始）</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">UPDATE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>影响<span lang="EN-US">UPDATE</span>命令的使用</span>

</td></tr><tr><td colspan="1" rowspan="1" valign="top" width="187">

<span lang="EN-US">USAGE</span>

</td><td colspan="1" rowspan="1" valign="top" width="366">

<span>只连接，不授予权限</span>

</td></tr></table>
</div>
</div>
