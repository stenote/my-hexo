title: MySQL index
tags:
- MySQL
date: "2013-01-06 07:30:01"
---

<div>mysql index（索引）用于加速检索</div>
<div>
</div>
<div>1、创建索引</div>
<div>create table user(</div>
<div>id int not null,</div>
<div>name varchar(20),</div>
<div>key id(id)</div>
<div>);</div>
<div>2、修改列，增加索引</div>
<div>alter table user add index name(name);</div>
<div>create index name on user(name);</div>
<div>3、删除索引</div>
<div>alter table user drop index name;</div>
<div>4、查看索引</div>
<div>show index from user;</div>
<div>
</div>
<div>通常索引的部分操作如上：</div>
<div>以上的index都未加相关参数，所以为普通索引</div>
<div>还有唯一索引unique key</div>
<div>全文索引fulltext</div>
<div>单列索引</div>
<div>多列索引</div>
<div>空间索引</div>
<div>
</div>
<div>唯一索引:</div>
<div>alter table user add unique key name(name);</div>
<div>
</div>
<div>全文索引:</div>
<div>fulltext</div>
<div>alter table user add fulltext key name(name);</div>
<div>
</div>
<div>fulltext需要表的engine为myisam;</div>
<div>单列索引：</div>
<div>
</div>
<div>alter table user add key name(name(2));</div>
<div>多列索引：</div>
<div>alter table user add key name_and_id(name, id);</div>
<div>
</div>
<div>空间索引</div>
<div>alter table user add spatial key name(name);</div>
<div>name需要为geometry类型才可</div>
