title: MySQL errors warnings
tags:
- MySQL
date: "2013-01-13 00:26:00"
---

<div>mysql &gt; show errors;</div>
<div>mysql &gt; show warnings;</div>
<div>用于显示之后一次的error和warning，如果之前没有error和warning，则显示为空</div>
<div>show errors对于错误的返回值，我们可以使用</div>
<div>perror命令，查看该错误代码对应的详细错误信息。</div>
