title: MySQL union
tags:
- MySQL
date: "2013-01-15 04:17:55"
---

<div>mysql中对于不同表的搜索结果可进行union（联合、合并）</div>
<div></div>
<div>union通常有两种不同的方法，分别为</div>
<div>union  联合所有，对相同结果过滤</div>
<div>union all 联合所有，对相同结果不过滤</div>
<div></div>
<div>union时，需要注意结果列数相同，如果列数目不同会提示错误</div>
<div></div>
<div>如下：</div>
<div>我们检索该数据库：</div>
[![](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201249.png "notesMySQLScreenshot-from-2013-01-14-201249.png")](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201249.png)
<div></div>
<div>一共有4条数据，分别为44  1 2 3</div>
<div>我们使用union进行链接：</div>
&nbsp;
<div>[![](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201353-300x191.png "notesMySQLScreenshot-from-2013-01-14-201353.png")](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201353.png)</div>
<div></div>
<div>我们发现结果和直接进行select一致。</div>
<div>实际上已经进行了union的操作，只不过由于结果是一致的，所有系统自动过滤相同结果。</div>
<div>如下，使用union all</div>
<div></div>
<div>[![](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201506-300x210.png "notesMySQLScreenshot-from-2013-01-14-201506.png")](http://marui.1kapp.com/wp-content/uploads/2013/01/notesMySQLScreenshot-from-2013-01-14-201506.png)</div>
<div></div>
<div>针对union，我们只需要记住：</div>
<div>1、合并检索结果列数要一致</div>
<div>2、union 会过滤相同结果集，union all 不会过滤相同结果集。</div>
<div>3、union 子句中的order by 不起作用。</div>
