title: MySQL 子查询之 any
tags:
- MySQL
date: "2013-01-15 04:03:37"
---

**参考表：employee**

**参考表：department**

 ANY关键字表示满足其中任一条件

<div>
<div><span><a title="复制代码"></a></span></div>
<pre>mysql&lt;span&gt;&amp;gt;&lt;/span&gt; &lt;span&gt;SELECT&lt;/span&gt; &lt;span&gt;*&lt;/span&gt; &lt;span&gt;FROM&lt;/span&gt;&lt;span&gt; employee
    &lt;/span&gt;&lt;span&gt;-&amp;gt;&lt;/span&gt; &lt;span&gt;WHERE&lt;/span&gt; d_id&lt;span&gt;!=ANY&lt;/span&gt;
    &lt;span&gt;-&amp;gt;&lt;/span&gt; (&lt;span&gt;SELECT&lt;/span&gt; d_id &lt;span&gt;FROM&lt;/span&gt;&lt;span&gt; department);
&lt;/span&gt;&lt;span&gt;+&lt;/span&gt;&lt;span&gt;--&lt;/span&gt;&lt;span&gt;----+------+--------+------+------+--------------------+&lt;/span&gt;
&lt;span&gt;|&lt;/span&gt; num  &lt;span&gt;|&lt;/span&gt; d_id &lt;span&gt;|&lt;/span&gt; name   &lt;span&gt;|&lt;/span&gt; age  &lt;span&gt;|&lt;/span&gt; sex  &lt;span&gt;|&lt;/span&gt; homeaddr           &lt;span&gt;|&lt;/span&gt;
&lt;span&gt;+&lt;/span&gt;&lt;span&gt;--&lt;/span&gt;&lt;span&gt;----+------+--------+------+------+--------------------+&lt;/span&gt;
&lt;span&gt;|&lt;/span&gt;    &lt;span&gt;1&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; &lt;span&gt;1001&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 张三   &lt;span&gt;|&lt;/span&gt;   &lt;span&gt;26&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 男   &lt;span&gt;|&lt;/span&gt; 北京市海淀区         &lt;span&gt;|&lt;/span&gt;
&lt;span&gt;|&lt;/span&gt;    &lt;span&gt;2&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; &lt;span&gt;1001&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 李四   &lt;span&gt;|&lt;/span&gt;   &lt;span&gt;24&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 女   &lt;span&gt;|&lt;/span&gt; 北京市昌平区         &lt;span&gt;|&lt;/span&gt;
&lt;span&gt;|&lt;/span&gt;    &lt;span&gt;3&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; &lt;span&gt;1002&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 王五   &lt;span&gt;|&lt;/span&gt;   &lt;span&gt;25&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 男   &lt;span&gt;|&lt;/span&gt; 湖南长沙市           &lt;span&gt;|&lt;/span&gt;
&lt;span&gt;|&lt;/span&gt;    &lt;span&gt;4&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; &lt;span&gt;1004&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; Aric   &lt;span&gt;|&lt;/span&gt;   &lt;span&gt;15&lt;/span&gt; &lt;span&gt;|&lt;/span&gt; 男   &lt;span&gt;|&lt;/span&gt; England            &lt;span&gt;|&lt;/span&gt;
&lt;span&gt;+&lt;/span&gt;&lt;span&gt;--&lt;/span&gt;&lt;span&gt;----+------+--------+------+------+--------------------+&lt;/span&gt;
&lt;span&gt;4&lt;/span&gt; rows &lt;span&gt;in&lt;/span&gt; &lt;span&gt;set&lt;/span&gt; (&lt;span&gt;0.00&lt;/span&gt; sec)</pre>
</div>

any为任意一个条件即可，d_id不为任意一个结果即可，由于department有一个d_id为1003，所有employee的d_id不为1003即可返回结果，满足条件，所有返回所有结果

mysql &gt; select * from employee

-&gt;where d_id = any(

select d_id from department);

结果为

<div></div>
<div>loop每一个结果，d_id满足其中一条结果即可，employee有个d_id为1004的记录，不在department中d_id列表中，所有，d_id为1004的记录结果被过滤，不显示。</div>
<div>
</div>
<div>

##### 带ALL关键字的子查询

ALL关键字表示满足其中所有条件

<div>
<pre>mysql&gt; SELECT * FROM employee
    -&gt; WHERE d_id&gt;=ALL
    -&gt; (SELECT d_id FROM department);
+------+------+------+------+------+----------+
| num  | d_id | name | age  | sex  | homeaddr |
+------+------+------+------+------+----------+
|    4 | 1004 | Aric |   15 | 男   | England  |
+------+------+------+------+------+----------+
1 row in set (0.00 sec)</pre>
<div><a title="复制代码"></a></div>
</div>
</div>
