title: gitolite简介和简单部署
tags:
- Git
- Linux
date: "2013-08-31 14:09:19"
---

gitolite是一个主流的git库管理软件，通常用于私人git库管理。其他主流的git库管理系统还包括gitosis，但是gitosis已停止开发，建议学习gitolite。

gitolite在github上的开源地址：[https://github.com/gitlabhq/gitolite](https://github.com/gitlabhq/gitolite "https://github.com/gitlabhq/gitolite")
本文包括以下内容

1、gitolite的安装
2、gitolite的配置
3、gitolite的使用3个方面

<!--more-->

1、gitolite的安装

针对debian及其衍生操作系统，可直接使用apt-get进行安装，如下：
<pre class="brush: shell;">$ sudo apt-get install gitolite</pre>
针对redhat及其衍生操作系统，可使用yum进行安装，如下：
<pre class="brush: shell; toolbar: false">$ sudo yum install gitolite</pre>
详细yum安装gitolite，请参考[安装gitolite](http://zengrong.net/post/1720.htm "安装gitolite")

以下以ubuntu(debian衍生系统为例)进行说明

安装完成后执行如下命令，查看gitolite是否安装完成：
<pre class="brush: shell;">$ sudo dpkg -L gitolite</pre>
如下，显示系统已安装了gitolite
<pre class="brush: shell;">/.
/usr
/usr/share
/usr/share/gitolite
/usr/share/gitolite/conf
/usr/share/gitolite/conf/VERSION
/usr/share/gitolite/conf/example.conf
/usr/share/gitolite/conf/example.gitolite.rc
/usr/share/gitolite/hooks
/usr/share/gitolite/hooks/common
/usr/share/gitolite/hooks/common/gitolite-hooked
/usr/share/gitolite/hooks/common/gl-pre-git.hub-sample
/usr/share/gitolite/hooks/common/post-receive.mirrorpush
/usr/share/gitolite/hooks/common/update
/usr/share/gitolite/hooks/common/update.secondary.sample
/usr/share/gitolite/hooks/gitolite-admin
/usr/share/gitolite/hooks/gitolite-admin/post-update
/usr/bin
/usr/bin/gl-setup
/usr/share/doc/gitolite/examples/adc/su-setperms
#以上省略了部分多余信息
</pre>

看发现gitolite创建了gl-setup命令，并且只有这一个gl-setup命令。该命令用于gitolite初始化。

我们需要进行如下操作，才可进行gitolite的安装：
1、创建独立的gitolite管理用户，并进行用户初始化
2、生成gitolite管理使用公钥
3、使用gl-setup进行gitolite初始化配置
4、进行gitolite的用户、库设定

如下：
1、创建独立的gitolite管理用户，并进行用户初始化
<pre class="brush: shell;">
$ sudo useradd gitolite
</pre>
创建gitolite用户home目录
<pre class="brush: shell;">
$ sudo mkdir /home/gitolite
</pre>
设定/home/gitolite的owner和group为gitolite
<pre class="brush: shell;">
$ sudo chown -R gitolite:gitolite /home/gitolite
</pre>
修改/etc/passwd,设定gitolite的home目录为/home/gitolite

如上，完成对gitolite的用户初始化，该用户无需生成密码

2、生成gitolite管理使用公钥
需要生成某个需要管理的用户的公钥供gitolite初始化使用，如果管理gitolite的用户已有公钥，直接复制到gitolite目录即可。
如下：
生成公钥、私钥对文件
<pre class="brush: shell;">
$ sudo ssh-kengen
#执行后提示公钥、私钥保存目录，默认保存即可到～/.ssh/目录，需小心覆盖已经公钥、私钥对文件
</pre>
复制公钥文件到gitolite目录
<pre class="brush: shell;">
$ sudo cp ~/.ssh/id_rsa.pub /home/gitolite/may.pub
</pre>
需要修改公钥文件名称，以 用户名.pub 为格式。这是因为，gitolite在初始化时，会把文件名称作为管理员名进行初始化

修改文件owner group
<pre class="brush: shell;">
$ sudo chown gitolite:gitolite /home/gitolite/may.pub
</pre>

3、使用gl-setup进行gitolite初始化配置
切换到gitolite用户，执行gitolite初始化命令

<pre class="brush: shell;">
$ sudo su --shell=/bin/bash gitolite
$ cd ~
$ gl-setup may.pub
</pre>
执行后，提示进入gitolite配置页面，默认使用vim编辑器打开
直接保存关闭即可

系统是初始化gitolite-admin、test两个库。
gitolite-admin库是用于gitolite管理的库
testing库只是用来练习使用库

4、进行gitolite的用户、库设定
退出当前用户到may（gitolite管理员用户）, 命令略去

尝试使用gitolite用户ssh链接到当前pc
<pre class="brush: shell;">
$ ssh gitolite@localhost
</pre>
系统提示如下：
<pre class="brush: shell;">
hello may, this is gitolite 2.2-1 (Debian) running on git 1.7.9.5
the gitolite config gives you the following access:
     R   W 	gitolite-admin
    @R_ @W_	testing
Connection to localhost closed.
</pre>
提示当前ssh登录用户中，可对gitolite-admin和testing两个库进行读写操作。

clone下gitolite-admin库，进入该目录
<pre class="brush: shell;">
$ git clone gitolite@locahost:gitolite-admin.git
$ ls gitolite-admin
</pre>

如下，查看目录中文件
<pre class="brush: shell;">
$ tree gitolite-admin
#显示如下：

gitolite-admin/
├── conf
│   └── gitolite.conf
└── keydir
    └── may.pub

2 directories, 2 files
</pre>
conf目录中gitolite.conf目录用于存储gitolite的库、用户、用户组、权限等配置文件
keydir用于存储用户公钥文件。

简单举例：

gitolite-admin/config/gitolite.conf目录中增加内容如下
<pre class="brush: shell;">
repo    fun
        RW+     =   may
        R       =   bar
</pre>
如上，创建了一个may用户可读写，bar用户可读的名为fun的库
复制bar用户的公钥文件到keydir目录为bar.pub

进行gitolite-admin库的commit后，push即可
<pre class="brush: shell;">
$ git add .*
$ git commit -m &#039;update gitolite-admin&#039;
$ git push
</pre>
提交后系统提示中显示：
<pre class="brush: shell;">
remote: Initialized empty Git repository in /home/gitolite/repositories/fun.git/
</pre>
初始化了fun库

切换到bar用户，尝试clone fun库
<pre class="brush: shell;">
$ sudo su --shell=/bin/bash bar
$ git clone gitoliet@localhost:fun.git
</pre>
成功clone fun库

简单进行修改后进行push操作，页面提示
<pre class="brush: shell;">
W access for fun DENIED to bar
(Or there may be no repository at the given path. Did you spell it correctly?)
fatal: The remote end hung up unexpectedly
</pre>
bar用户针对fun库，不具有写操作

尝试切换到may用户，对fun进行push操作，发现无问题，说明gitolite的权限配置设定正确。

gitolite拓展阅读：
http://www.ossxp.com/doc/git/gitolite.html

如上。可简单部署、了解并使用gitolite

晚安
