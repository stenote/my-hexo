title: yar
tags:
- Linux
- PHP
date: "2013-08-10 20:11:48"
---

yar简介：
> Yet Another RPC framework for PHP>
> 
> Light, concurrent RPC framework for PHP(c, java etc will be supported soon)
它是一个轻量级、可并发的用于php的RPC框架，后续会支持C java等主流语言。

yar官方链接：[https://github.com/laruence/yar](https://github.com/laruence/yar)

作者的关于yar的博文:[http://www.laruence.com/2012/09/15/2779.html](http://www.laruence.com/2012/09/15/2779.html)

pecl链接：[http://pecl.php.net/package/yar](http://pecl.php.net/package/yar)

<!--more-->
yar的安装：
<pre class="brush: shell; gutter: true">sudo apt-get install make
#安装make

sudo apt-get install php5-dev 
#安装php5-dev

sudo apt-get install php-pear
#安装php-pear</pre>
以上安装了基本的系统需要的pear和make，便于通过pecl进行yar的安装。
<pre class="brush: shell; gutter: true">sudo pecl upgrade
#进行pecl的升级

sudo pecl install msgpack
#系统提示错误，提示没有stable

sudo pecl install msgpack-0.5.5
#进行msgpack-0.5.5版本的安装</pre>
以上进行pecl的更新和对于msgpack的更新。

msgpack介绍
> It's like JSON.but fast and small.
官方链接： [http://msgpack.org](http://msgpack.org/)

安装完成msgpack后需增加到php.ini中

如下，进行msgpack在php中是否安装成功
<pre class="brush: shell; gutter: true">php -m | grep msgpack
#查看是否安装了msgpack这个php的模块

php -r &quot;var_dump(extension_loaded(&#039;msgpack&#039;));&quot;
#也可直接调用上述extension_loaded进行检测msgpack是否安装</pre>
安装yar
<pre class="brush: shell; gutter: true">sudo pecl install yar</pre>

yar服务端代码

<pre class="brush: php; gutter: true; first-line: 1; highlight: []; html-script: false">
&lt;?php

class API {

    /**
      * @params null
      * @return string &#039;hello&#039;
      * @brief just test api
      */
    public function hello() {
        return &#039;hello&#039;;
    }

    /**
      * @params
      * @return int
      * @brief get server current timestamp
      */
    public function time() {
        return time();
    }
}

$api = new API;

$service = new Yar_Server($api);

$service-&gt;handle();

</pre>

如上，我们创建了一个Yar_Server对象，传入了api实例化对象，然后handle()即可；
访问api地址，显示结果如下：
[![yar_server](http://www.mmmmm.io/wp-content/uploads/2013/08/Screen-Shot-2013-09-22-at-10.38.12-PM.png)](http://www.mmmmm.io/wp-content/uploads/2013/08/Screen-Shot-2013-09-22-at-10.38.12-PM.png)

如上，页面显示了可供调用的api方法，并且会将api方法注释显示出来。

yar客户端调用

yar客户端通常有两种调用方法，一种是单线程单一调用，另外一种是多线程并发调用。
单线程单一调用：

<pre class="brush: php; gutter: true; first-line: 1; highlight: []; html-script: false">
#!/usr/bin/env php
&lt;?php

$client = new Yar_Client(&amp;#039;http://kohana/api.php&amp;#039;);

$client-&amp;gt;setOpt(YAR_OPT_CONNECT_TIMEOUT, 1);
$client-&amp;gt;setOpt(YAR_OPT_TIMEOUT, 3);
$client-&amp;gt;setOpt(YAR_OPT_PACKAGER, &amp;#039;php&amp;#039;);

$result = $client-&amp;gt;hello();
var_dump($result);

$time = $client-&amp;gt;time();
var_dump($time);

</pre>
如上，执行如下几步：
①，创建Yar_Client对象
②，设定client相关option
③，调用远程方法

执行结果如下：

[![Yar_Client](http://www.mmmmm.io/wp-content/uploads/2013/08/Screen-Shot-2013-09-22-at-10.44.15-PM.png)](http://www.mmmmm.io/wp-content/uploads/2013/08/Screen-Shot-2013-09-22-at-10.44.15-PM.png)

并发调用：
客户端脚本如下：
<pre class="brush: php; gutter: true; first-line: 1; highlight: []; html-script: false">

#!/usr/bin/env php
&lt;?php
function callback($retval, $callinfo) {
     var_dump($retval);
}

function error_callback($type, $error, $callinfo) {
    error_log($error);
}

Yar_Concurrent_Client::call(&amp;#039;http://host/api/&amp;#039;, &amp;#039;api&amp;#039;, array(&amp;#039;parameters&amp;#039;), &amp;#039;callback&amp;#039;);
Yar_Concurrent_Client::call(&amp;#039;http://host/api/&amp;#039;, &amp;#039;api&amp;#039;, array(&amp;#039;parameters&amp;#039;));   
// if the callback is not specificed,                                                                                
// callback in loop will be used

Yar_Concurrent_Client::call(&amp;#039;http://host/api/&amp;#039;, &amp;#039;api&amp;#039;, array(&amp;#039;parameters&amp;#039;), &amp;#039;callback&amp;#039;, array(YAR_OPT_PACKAGER =&amp;gt; &amp;#039;json&amp;#039;));                                                    
//this server accept json packager

Yar_Concurrent_Client::call(&amp;#039;http://host/api/&amp;#039;, &amp;#039;api&amp;#039;, array(&amp;#039;parameters&amp;#039;), &amp;#039;callback&amp;#039;, array(YAR_OPT_TIMEOUT=&amp;gt;1));                                                                              
//custom timeout 

Yar_Concurrent_Client::loop(&amp;#039;callback&amp;#039;, &amp;#039;error_callback&amp;#039;); 
//send the requests, 
//the error_callback is optional
</pre>

执行如下几步：
①、通过Yar_Concurrent_Client::call函数进行并发设定
②、调用Yar_Concurrent_Client::loop进行并发rpc访问，同时绑定callback，error_callback(可选)
③、在callback中进行处理

拓展阅读：
[
https://github.com/laruence/yar](https://github.com/laruence/yar "https://github.com/laruence/yar")
