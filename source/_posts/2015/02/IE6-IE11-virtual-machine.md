title: IE6-11虚拟机
date: "2015-02-01 16:58:54"
tags: ~
---


最近遇到了一些IE不兼容的问题，网上找到一组windows虚拟机，覆盖IE6-11。

原文地址：http://osxdaily.com/2011/09/04/internet-explorer-for-mac-ie7-ie8-ie-9-free/


大概意思是：

1. 安装VirtualBox

2. 打开Terminal，将路径更改到/Applications/Utilities/

3. 运行命令：curl -s https://raw.githubusercontent.com/xdissent/ievms/master/ievms.sh | bash

此时开始下载安装虚拟机。

如果只需要安装制定版本可以运行如下代码

只需要修改 IEVMS_VERSIONS 为 6 - 11 的整数, 即可安装对应版本的 VM

```
export IEVMS_VERSIONS="10"
curl -s https://raw.githubusercontent.com/xdissent/ievms/master/ievms.sh | bash
```

**注意: 下载安装过程中需要输入一次管理员密码，（安装一个组件unar 时需要翻一下墙。Downloading unar from http://theunarchiver.googlecode.com/files/unar1.5.zip to unar1.5.zip
(attempt 1 of 3)。）之后就是漫长的下载安装过程。**

这些虚拟机中只安装了对应的IE，而且使用vm的快照方式，绕过微软的30天限制。
