title: Golang 编程百例 - 03
date: 2015-03-26 19:13:22
tags:
  - Go
---

> 题目：一个整数，它加上 100 后是一个完全平方数，再加上 168 又是一个完全平方数，请问 
该数是多少？ 

```
package main

import (
	"fmt"
	"math"

	//数值计算, 引入 math 包
	// http://docs.golang.org/pkg/math/
)

//常量
const MAX = 10000

func main() {

	var x, y, i int

	fmt.Println("满足条件的数有:")

	for i < MAX {
		x = int(math.Sqrt(float64(i + 100)))
		y = int(math.Sqrt(float64(i + 268)))

		// math.Sqrt 需要传入 float64

		if x*x == i+100 && y*y == i+268 {
			fmt.Println(i)
		}
		i++
	}
}
```
