title: 日志等级
date: "2015-03-22 00:18:11"
tags: ~
---

为明确程序中 Log 等级划分, 简单进行检索后整理后如下.

如下为 Linux Kernel 的等级划分, 已增加到现有项目中

* `0. EMERG` The system is unusable.
* `1. ALERT` Actions that must be taken care of immediately.
* `2. CRIT` Critical conditions.
* `3. ERR` Noncritical error conditions.
* `4. WARNING` Warning conditions that should be taken care of.
* `5. NOTICE` Normal, but significant events.
* `6. INFO` Informational messages that require no action.
* `7. DEBUG` Debugging messages, output by the kernel if the developer enabled debugging at compile time.
