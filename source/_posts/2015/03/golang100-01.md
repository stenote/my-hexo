title: Golang 编程百例(实际上只有 41 例) - 01
date: 2015-03-24 21:58:14
tags:
  - Go
---

本文初级篇内容依据《C语言经典算法100例》编写, 提高篇内容依据《C语言趣味编程100例》编写, 可以作为Go语言学习中的小测验. 
由于有些题目利用到C语言特性, 针对这些题目只能尽可能的替换成Go语言写法.

**程序 01：数字排列组合**

> 有 1、2、3、4 个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？

源码:

```golang
package main

//main 包, 表示编译后为可执行程序

import (
	"fmt"
	//引入 fmt 包, 标准化 I/O 包
)

func main() {

	// 定义 i j k
	var (
		i int = 1
		j int = 1
		k int = 1
	)

	// golang 中没有 while 循环, 只有 for
	for i < 5 {
		for j < 5 {
			for k < 5 {
				// golang 中 if 不需要 () 包裹
				if i != k && i != j && j != k {
					// 调用 fmt 包中的 Printf 函数
					fmt.Printf("%d%d%d\n", i, j, k)
				}
				k++
			}
			// k 值重置到 1
			k = 1
			j++
		}
		// j 值重置到 1
		j = 1
		i++
	}
}
```
---
