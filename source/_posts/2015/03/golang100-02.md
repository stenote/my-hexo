title: Golang 编程百例 - 02
date: 2015-03-25 21:05:01
tags:
  - Go
---


> 题目：企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提成10%；利润高于10万元，低于20万元，低于10万元的部分按10%提成，高于10万元的部分，可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？ 

```golang
package main

import (
	"fmt"
)

func main() {

	var (
		i  int
		p, fi  float32
	)

	//提示输入
	fmt.Println("请输入利润:")

	//输入, 赋值
	fmt.Scanf("%d", &i)

	fi = float32(i) / 100000

	// golang 中 switch 可不传入参数, case 会依次向下判断, 满足即执行
	switch {
	case fi > 10: //超过 100 万
		p = float32(i-1000000) * 0.01
		// 相乘, 请注意类型一致
		// http://www.oschina.net/question/561584_87752
		// golang 中默认自带 break
		// 如果想继续向下执行, 需要增加 fallthrough
		fallthrough
	case fi > 6: //超过 60 万
		p += float32(i-600000) * 0.015
		fallthrough
	case fi > 4: // 超过 40 万
		p += float32(i-400000) * 0.03
		fallthrough
	case fi > 2: // 超过 20 万
		p += float32(i-200000) * 0.05
		fallthrough
	case fi > 1: // 超过 10 万
		p += float32(i-100000) * 0.075
		fallthrough
	default: // 没超过 10 万
		p += float32(i) * 0.1
	}

	fmt.Printf("利润: %d 奖金: %.4f\n", i, p)
}
```
