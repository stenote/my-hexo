title: 数据迁移
date: "2015-03-14 11:09:08"
tags: ~
---

数据迁移

---

已从原有 wordpress 中迁移到 hexo 上, 部分非重要博文进行删除处理

部分旧博文内容简单修改中

迁移方法详见 [hexo 迁移文档](http://hexo.io/docs/migration.html)
