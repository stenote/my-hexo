title: 使用 tmux 让命令后台执行
date: 2015-07-31 21:32:06
tags:
---

某些情况下, 我需要让某些命令在 tmux 中执行, 但是我同时还需时不时去 tmux 中去查看执行情况, 如何解决?

tmux 提供了一个 command, 可通过增加参数让 command 在一个独立的 session 中执行. 当命令执行完成后, 该 session 自行销毁。如下:

```
$ tmux new-session -d 'until docker pull docker.mmmmm.io/genee/node:0.12; do echo -n; done'
```

使用 tmux 创建一个新的 session, session 中后台执行命令:

```
until docker pull docker.mmmmm.io/genee/node:0.12; do echo -n; done
```

由于该命令会一直重复执行, 直到正确返回结果, 所以, 下次重新进入 tmux, 依然可看到命令执行状态.


需要注意, 该命令如果执行完成, 该 session 会 **自行销毁**!
