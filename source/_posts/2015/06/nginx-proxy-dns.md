title: nginx 代理 dns 解析错误（慢）的解决办法
date: 2015-06-04 20:30:11
tags:
  - nginx
---

nginx 做反向代理的时候, 通常配置如下:

```
server {
    listen 80;

    index index.html;

    charset utf8;
    server_name *.mmmmm.io  mmmmm.io;

    location ~*^/ {
        proxy_pass http://may_web_1.docker;
        proxy_set_header Host $host;
    }
}
```

如上, 任何访问 `*.mmmmm.io` 的请求都会代理访问 `http://may_web_1.docker` 上. 如上本地使用 dnsmasq 进行了部署, `may_web_1.docker` 指向一个本地的 docker 容器, 如果 docker 容器进行了重启, 本地 dns 也需要同步进行一下更新。

**但是**, nginx 不会重新去获取新的 `may_web_1.docker` 的地址.

**但是**, nginx 不会重新去获取新的 `may_web_1.docker` 的地址.

**但是**, nginx 不会重新去获取新的 `may_web_1.docker` 的地址.

**重要的事情说三遍.**

`nginx -s reload` 也不行!

`nginx -s reload` 也不行!

`nginx -s reload` 也不行!

**重要的事情说三遍.**

此时, 你只能重启 **nginx** : `service nginx restart`

解决办法如下:

* nginx 增加 resolver
* 设置代理服务器变量
* proxy_pass 使用变量. 

如上配置可修改为: 

```
server {
    listen 80;

    index index.html;

    charset utf8;
    server_name *.mmmmm.io  mmmmm.io;
    
    resolver 172.17.42.1; #docker 地址. 如果 nginx 在容器中, 127.0.0.1 会错误解析

    set $backend "may_web_1.docker";

    location ~*^/ {
        proxy_pass http://$backend;
        proxy_set_header Host $host;
    }
}
```

修改后再怎么重启 docker 容器我都不怕了!

妈蛋, 这个问题折腾我 2 周了!
