title: 消息队列
date: 2015-06-07 21:23:09
tags:
  - MQ
---

天下消息队列是一家, 不是你家抄我家, 就是我家抄你家.

所有的消息队列内部逻都是 `Producer` 发送到 `Consumer` 模式

分享一下几个 MQ 的链接:

* [消息队列是什么](http://zh.wikipedia.org/wiki/%E6%B6%88%E6%81%AF%E9%98%9F%E5%88%97)
* [zmq](http://zeromq.org/)
* [rabbitmq](http://www.rabbitmq.com/)
* [beanstalkd](http://kr.github.io/beanstalkd/)
* [beanstalkd 一个高性能分布式内存队列系统](http://csrd.aliapp.com/?p=1201)
* [beanstalkd 协议](http://www.slideshare.net/hitkidnil/beanstalk-protocol)
* [卡夫卡](http://kafka.apache.org/)
