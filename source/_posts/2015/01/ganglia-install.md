title: ganglia 安装
date: "2015-01-25 23:53:33"
tags: ~
---

ganglia 简单安装，分为 gmetad 、ganglia-monitor 两方面

## 客户端

### 依赖包安装
客户端需要进行用户数据收集、发送即可。
ganglia-monitor 是用来进行数据收集的，只需要安装在 client 即可。

安装方式

```
    sudo apt-get install ganglia-monitr 
```

### 配置设置

安装好依赖包后, 我们只需要配置 **/etc/ganglia/gmond.conf** 文件即可，如下

```
cluster {
    name = "mmmmm.io"
    owner = "unspecified"
    latlong = "unspecified"
    url = "unspecified"
}

/* The host section describes attributes of the host, like the location */
host {
    location = "mmmmm.io"
}

```

如上, 通常只需要配置 `host.location` 和 `cluster.name` 即可

## 服务端

### 依赖包安装

服务端需要安装的的服务包括如下：

* gmetad 用来和 ganglia-monitor 进行用户数据收集
* rrdtool 用来对获取的数据转换为 png 图片的工具
* nginx 用于提供 ganglia 收集数据显示
* php（php-gd）进行数据图表显示

安装方式

```
    sudo apt-get install gmetad rrdtool nginx php5-fpm php5-gd
```

### 配置设置

安装好依赖包后, 我们只需要配置 **/etc/ganglia/gmetad.conf** 文件即可，如下

```
    data_source "programer.im" programer.im
    data_source "m.mmmmm.io" m.mmmmm.io
    data_source "mmmmm.io" mmmmm.io
```
