title: 基础技能
date: "2015-01-27 20:57:15"
tags:
---

以下为自己需要学习的一些基础的开发方面的辅助 "技能" (排名不分先后)

* [markdown](https://www.zybuluo.com/mdeditor?url=https://www.zybuluo.com/static/editor/md-help.markdown)
* [web sequence diagrams](http://www.websequencediagrams.com/examples.html)
* [语义化版本](http://semver.org/lang/zh-CN/)
* [中文文案排版指南](https://github.com/sparanoid/chinese-copywriting-guidelines)


如上更新 **2015/01/27 21:02:00** 

吐槽: *摇号又没中*
