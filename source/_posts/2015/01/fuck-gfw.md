title: 德国新教 马丁·尼莫拉牧师 的忏悔诗
date: "2015-01-25 23:49:46"
tags: ~
---

> 在德国, 起初他们追杀共产主义者, 我没有说话
> 因为我不是共产主义者;
> 接着, 他们追杀犹太人, 我没有说话
> 因为我不是犹太人;
> 后来, 他们追杀工会成员, 我没有说话
> 因为我不是工会成员;
> 此后, 他们追杀天主教徒, 我没有说话
> 因为我是新教徒;
> 最后, 他们奔我而来, 却再也没有人站起来为我说话了

---

> They first came for the Communists,
> and I didn't speak up
> because I wasn't a Communist.
> Then they came for the Jews,
> and I didn't speak up
> because I wasn't a Jew.
> Then they came for the trade unionists,
> and I didn't speak up
> because I wasn't a trade unionist.
> Then they came for the Catholics,
> and I didn't speak up
> because I was a Protestant.
> Then they came for me,
> but by that time,
> there was no one left to speak up.
