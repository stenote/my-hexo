title: go超时处理
date: 2015-05-01 22:12:12
tags:
  - Go
---

`golang` 在 `go` 操作, 有可能超时, 可以使用 `select` 来解决这个问题, 如下:

```
package main

import (
	"fmt"
	"time"
)
//设定了一个 "超时" 的函数
func timeout(c chan int) {

	time.Sleep(10 * time.Second)

	c <- 1
}

func main() {

	//创建两个 channel
	var c1 = make(chan int)
	var c2 = make(chan int)

	//调用一个超时的 go
	go timeout(c1)
	
	//解决超时
	go func() {
		time.Sleep(1 * time.Second)
		c2 <- 0
	}()

	select {
	case <-c1:
		fmt.Println("going!")	//正常通过 c1 获取数据
	case <-c2:
		fmt.Println("timeout") //避免超时
	}
}
```
