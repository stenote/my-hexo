title: curl 模仿 jsonrpc2.0 发送请求
date: 2015-05-21 21:34:40
tags:
---

```
$ curl -i -H 'Content-Type:application/json' -d \
	'{
		"jsonrpc":"2.0",
		"method":"Yiqikong\/Control\/getStatus",
		"params":	[
			"fcbcdd3fb97eba325178c750b3ec900ee666ebfd"
		],
		"id":"555d93c795deb"
	}' \
 http://yiqikong-control.wine.mmmmm.io/api
```
