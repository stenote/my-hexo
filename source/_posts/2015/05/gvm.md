title: gvm
date: 2015-05-19 22:07:38
tags:
  - Go
---

> # [gvm](https://github.com/moovweb/gvm)
> 
> GVM provides an interface to manage Go versions.
> 
> 

gvm 的几个坑:

* gvm 在每次使用的时候, 都需要进行 `gvm use golang1.4.2` 等操作, 选择对应的 golang 版本, 可考虑在 .bashrc 中增加如上命令

* gvm 的 $GOPATH 、$GOROOT 等均指向的对应版本的目录, 而且随着不同的 golang 版本切换, 该目录还会发生变动, 建议 .bashrc 中配置, 固定 $GOPATH 和 $GOROOT

* gvm 默认使用 https 进行 golang 代码 clone. 建议修改 gvm install 的 shell, 使用 ssh 进行 clone. 

如上


同类工具还有:

* [rvm](https://github.com/rvm/rvm)
* [phpenv](https://github.com/phpenv/phpenv)
* [pyenv](https://github.com/yyuu/pyenv)


李叔要离职了, 我开始有点不理智了.
