title: Vundle
date: 2015-05-27 21:30:12
tags:
  - vim
---

# Vundle = Vim + Bundle 

> Vundle, Vim 插件管理的插件 (用插件管理插件)

* 在接触 Vundle 之前, 你永远想不到 Vim.org 上的插件有多么丰富.
* 在接触 Vundle 之前, 你永远想不到 vim 配置也可以这么清爽
* 在接触 Vundle 之前, 你永远想不到 vim 才是最好的编辑器
* 在接触 Vundle 之前, 你永远想不到 vim 可以替代 IDE

gmarik, 让我赞美你吧. Bram Moolenaar 让我赞美你吧.

> Help poor children in Uganda !

![Children](children.jpg)
