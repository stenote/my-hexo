title: git status 汉字显示异常
date: 2015-05-20 15:47:41
tags:
  - Git
---

Git 中, 如果我们创建一个包含汉字的文件, git status, 可能显示效果如下:

```
user@host:~$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   class/Gini/Controller/API/YiQiKong/Record.php
	modified:   "designs/Lims-CF\345\222\214YiQiKong-Record\347\232\204debade\351\200\232\350\256\257\345\215\217\350\256\256.md"
	modified:   designs/api.md

```


如上, 一个中文文件修被转义了, 感觉不友好, 我们可以通过修改如下配置, 优化显示效果:

```
user@host:~$ git config core.quotepath false
```

重新进行 status 查看, 如下:

```
user@host:~$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   class/Gini/Controller/API/YiQiKong/Record.php
	modified:   designs/Lims-CF和YiQiKong-Record的debade通讯协议.md
	modified:   designs/api.md
```

嘿嘿! 今天心情指数 **5** !
