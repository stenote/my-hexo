title: go
date: 2015-04-26 21:23:39
tags:
  - Go
---

golang 中提供了 `go` 关键字进行并发执行.

举个如下的简单例子:

> 计算出 100 以内能被 2、3 整出的整数的总和

php 版本:

```php
<?php

$sum = $i = 0;

while($i < 100) {

	if (($i % 2 == 0 || $i % 3 == 0) && $i % 6 != 0)  {
		$sum += $i;
	}

	$i ++;
}

echo $sum;

```

通过从 0 开始逐一判断数值是否满足要求, 满足则累加

golang 版本:

```
package main

import (
	"fmt"
)

const MAX = 100

func test(step int, c chan int) {

	var (
		i   = 0
		sum = 0
	)

	for i < MAX {
		sum += i
		i += step
	}

	c <- sum

}

func main() {
	var chan1 = make(chan int, 2)
	var chan2 = make(chan int, 1)

	go test(2, chan1)
	go test(3, chan1)

	go test(6, chan2)

	fmt.Println(<-chan1 + <-chan1 - 2*<-chan2)
}
```

通过创建两个 chan, 然后并发计算得到结果
