title: go-install
date: 2015-04-22 10:04:07
tags:
  - Go
---


`go install` 是用于对开发完成的 go 代码进行编译、链接、安装到 **$GOPATH/bin** 的命令.

通常使用方法:

```
$ go install -x -a github.com/stenote/debade-courier-go


-x 显示详细信息
-a 对依赖包重新编译
```

由于依赖包的更新, 导致之前编译产生的 **.a** 文件存在的情况下, `git install` 可能会使用之前旧的 **.a** 文件, 从而出现异常情况, 所以建议每次使用增加 `-a` 参数
