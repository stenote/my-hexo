title: ansible 简单学习、分享
date: 2015-04-08 13:25:30
tags: 
  - ansible
---

# 1. 什么是 ansible

> Ansible is Simple IT Automation

# 2. 为什么选择 ansible 

* 学习门槛低
* 客户机无需安装额外软件

# 3. ansible 简单命令

```bash

# ping 各个节点
$ ansbile agent -m ping

# 安装 nginx
$ ansbile agent -m apt -a "name=nginx state=present"

# 上传文件
$ ansible agent -m file -a "path=/tmp/foo owner=www-data group=www-data"

# 查看 apt 模块
$ ansbile-doc apt 
```

# 4. ansible-playbook

```
$ ansible-playbook site.yml
```

# 5. 使用 ansible 部署 lemp 开发环境

[ansible-lemp-example](https://github.com/stenote/ansible-lemp-example)

# 6. ansible-galaxy 

[ansible-galaxy](http://galaxy.ansible.com)
