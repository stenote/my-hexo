title: gopm
date: 2015-04-25 20:15:31
tags:
  - Go
---


### [gopm](http://gopm.io/)

由于 **狗日的** 墙的存在, 国人开发的一个用于下载 golang 包的工具. 官方宣传口号:


> 无需 Git 和 Hg 等版本管理工具，就可以下载指定版本的 Go 语言包
> Download Go packages by version, without needing version control tools (eg Git, Hg, etc).


### FAQ

Q: 有了 go get, 为什么还要有 gopm

A:  因为 

* **狗日的** 墙
* go get 依赖于 git/svn/hg, **gopm** 不依赖 

---
 
Q: 网页版和命令行版有什么区别 ?

A: 网页版只可下载当前包, 不可同步下载依赖包

---

祝好
