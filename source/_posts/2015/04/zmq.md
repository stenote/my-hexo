title: php 的 ZMQ 扩展安装
date: 2015-04-08 17:40:19
tags: 
  - PHP
  - ZMQ
  - MQ
---

```bash
# 增加 ppa
$ add-apt-repository  ppa:chris-lea/zeromq

# update
$ apt-get update

# 安装 zmq
$ apt-get install libzmq3-dev libpgm-dev pkg-config

# 安装 php 的 zmq 扩展
$ pecl install zmq-beta

# 在 php 的 cli 和 fpm 中加入配置
$ cd /etc/php5/mods-available
$ echo 'extension=zmq.so' > zmq.ini

# 启用模块
$ php5enmod zmq
```
