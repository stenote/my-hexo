title: Git 提交目录
date: 2017-07-28 16:20:42
tags:
 - Git
---

在网上检索到的 Git 提交目录的解决办法只有下面一种方法:

> 在需要提交的目录中, 创建一个 `.gitkeep`、`.gitignore` 之类的用来让 git 可以追踪的文件, 然后通过提交这个文件来让 git 库追踪到这个文件的上层目录

但是有些情况下, 我真的很不喜欢提交 `.gitignore`、`.gitkeep` 文件, 理由如下:

* `.gitignore` 文件是在库的根目录用来让 git 库屏蔽一些不需要的文件的
* `.gitkeep` 这个文件是自己发明的, 就是为了追踪目录而创建的, 不喜欢


通过不懈的努力, 找到了一种解决方案: **通过提交一个 submodule（但是不提交 `.gitmodules` 文件, 来进行空目录提交）


```
git submodule add https://github.com/hiroton9/a.git path/to/empty-folder
```
如上, 我们用 git 创建了一个 `path/to/empty-folder` 的目录, 该目录为 `https://github.com/hiroton9/a.git` 的 submodule. 但是如果我们不提交 `.gitmodules` 文件, 那么这个目录会被提交, 而且是空的 :) 