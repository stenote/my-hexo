title: GMT/UTC/ISO 8601
date: 2017-06-27 09:40:34
tags:
  - Linux
---

### 名称区别

* GMT (Greenwich Mean Time)
* UTC (英：Coordinated Universal Time , 法：Temps Universel Coordonné)
* ISO 8601 (数据存储和交换形式·信息交换·日期和时间的表示方法)


### 简单说明

* GMT 是按照地球自转来决定的时间的, 目前由于自转速度不一致, 已经废弃 GMT, 使用更精准的 UTC 来计时
* UTC 是以原子时秒长为基础, 在时刻上尽量接近于世界时的一种时间计量系统,  是新的计时规则
* ISO 8601 是定义用来记录时间、日期的国际标准, 其中解决了时区的问题

可以理解为, GMT 和 UTC 时间是完全一致的, 只不过 UTC 更精准.

ISO 8601 只是用来规范怎么记录时间, 而不是用来度量时间。

### 问题解释

> 2017/04/02 12:22:04 到底是北京时间还是美国时间还是印度尼西亚时间。

由于上述时区无法限定, 我们应该表明是属于哪个时区的时间

ISO 8601 解决了这个问题, 一方面规范了日期、时间的写法, 另外一方面规范了如何表明时间记录时间所属时区

如上, 如果这个时间是北京时间, 我们可以表示为:

```
2017-04-22T12:22:04+08:00
```

表示, 位于 `+08:00` (也就是东八区, 相比 UTC 时间多了 8 个小时)的为 `2017年4月22日12点22点04分` 的时间。

此时此刻 UTC 的时间(按照 `ISO 8601` 标准表示)为 `2017-04-22T04:22:04Z` (其中 `Z` 表示 UTC 时间, 时区为 0)



