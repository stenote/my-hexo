title: Far better it is to dare mighty things
date: 2017-06-28 08:49:02
tags:
- 生活
---

> Far better it is to dare mighty things, to win glorious triumphs, even though checkered by failure, than to take rank with those poor spirits who neither enjoy much nor suffer much, because they live in the gray twilight that knows not victory or defeat.
>
> It is not the critic who counts, not the man who points out how the strong man stumbled, or whether the doer of the deeds could have done them better. The credit belongs to the man actually in the arena; whose face is marred by dust and sweat and blood; who strives valiantly; who errs and comes short again and again; who knows the great enthusiasms, the great devotions, and spends himself in a worthy cause; who, at best, knows in the end the triumph of high achievement; and who, at worst, if he fails, at least fails while daring greatly, so that his place shall never be with those cold and timid souls who know neither victory or defeat.