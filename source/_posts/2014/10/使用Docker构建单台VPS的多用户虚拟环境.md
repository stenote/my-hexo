title: 使用 Docker 构建单台 VPS 的多用户虚拟环境
tags:
- docker
- nginx
date: "2014-10-25 17:41:03"
---

由于vps有很多朋友、同事一起使用，而且每个人都具有root权限，由于人为操作，导致出现了诸多问题，故考虑对vps进行简单修改。

前期考虑，针对部分常用服务、软件包、用户信息等使用 **puppet** 进行管理，当人为操作错误等影响服务等其他资源后，**puppet** 自行进行修正。这样带来的问题是，可能会被具有 **root** 权限的其他人停掉puppet服务，但是如果通过去掉 **root** 权限来防止 **puppet** 服务不被停掉的问题，又会导致无法安装软件包、更新服务等。故该方法作废。

考虑到第二种解决办法，使用 **docker** 进行各个用户隔离，每个用户一个 **docker** 容器，自行在容器中进行任何操作。修改简易过程如下：

1.  安装docker
2.  创建各个用户web服务所使用docker image
3.  进行用户需要存储数据的规划
4.  使用fig编写不同的用户的docker container管理配置文件
5.  正常运行各个用户docker container
6.  通过dnsmasq来获取不同container的ip地址并更新到dns服务中
7.  修改现有vps上nginx配置，以反向代理的形式访问到docker container中
8.  docker container中使用原有nginx配置即可

以目前你所看到的博客为例。

**1. 安装 docker, 请参照[官方文档](http://docker.com)**

**2. 创建各个用户web服务所使用docker image, 根据具体需求, 已经构建了对应的 [docker image](https://github.com/docker-parasites/docker-lemp)**

**3、进行用户存储数据的规划**

我目前的用户数据存储结构如下：

```
.
├── fig.yml
├── home
├── mysql
└── nginx
```
fig.yml 为 docker 配置的 fig 文件
home 为家目录
mysql 为 mysql 数据库目录
nginx 为 nginx 的配置目录

**4、使用fig编写不同的用户的docker container管理配置文件**

fig.yml内容如下：

```
lnmp:
  image: lemp
  ports:
    - 5722:22
  volumes:
    - /data/may/home:/root
    - /data/may/mysql:/var/lib/mysql
    - /data/may/nginx:/etc/nginx/sites-enabled/
  hostname: may
  working_dir: /root
```

**6、通过dnsmasq来获取不同container的ip地址并更新到dns服务中**

详见： [https://github.com/iamfat/docker-dnsmasq](https://github.com/iamfat/docker-dnsmasq "https://github.com/iamfat/docker-dnsmasq")

**7、修改现有vps上nginx配置，以反向代理的形式访问到docker container中**

如下为nginx修改后配置：

```
server {
    listen 80;

    index index.php;

    charset utf8;
    server_name *.mmmmm.io mmmmm.io;

    location ~*^/ {
        proxy_pass http://may_lnmp_1.docker.local;
        proxy_set_header Host $host;
    }
}

```

这样，web 进行访问后会自定访问到 docker 容器里面，docker 容器内的 nginx 在独立监听80端口进行信息返回，vps 内的 nginx 再返回给用户。
