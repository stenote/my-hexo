title: nginx auth 密码生成
tags:
- nginx
date: "2014-10-25 12:33:02"
---

```php
#!/usr/bin/env php
<?php

$password = $argv[1];

echo crypt($password, base64_encode($password));
```

根据获取结果，以 name:password 的形式存储到 **auth_basic_user_file** 指定文件即可。
在nginx中进行配置即可：

```
server {
    listen 80;

    index index.php;

    charset utf8;
    server_name docker.mmmmm.io;

    location / {
        proxy_pass http://seagull.docker.local:10086/;
        auth_basic "input your name and password";
        auth_basic_user_file /etc/nginx/pwd/docker.mmmmm.io;
    }
}

```
如上即可。
