title: Reflection
tags:
- PHP
date: "2014-04-27 23:22:11"
---

php 在 class 中，通过 private protected public 三个关键字限定了部分属性、方法不可在外部、不可在子类、可在任何地方进行调用。由于 private 的限定，这样就会导致我们在进行测试过程中，无法对某些 private 的变量进行获取。

但是 php 提供了 Reflection 极致，可进行 private 变量获取、设定，如下，一个简单的php代码片段：

```
<?php

class Foo {
    private static $var = 'var';

    pubulic static function a() {
        echo self::$var;
    }
}

Foo::a();
```

<!--more-->

我们调用 Foo::a() 可正常获取到 $var， 但是如果没有对外的 public static 的方法，如何获取呢？
可考虑使用 Reflection， 如下：

```
<?php

class Foo {
    private static $var = 'var';
}

//创建ReflectionClass
$rf = new ReflectionClass('Foo');

//获取property
$var = $rf->getProperty('var');

//设定可访问
$var->setAccessible(true);

//获取value
var_dump($var->getValue());
```

也可设定var的值，如下：

```
<?php

class Foo {

    private static $var = 'var';

    public static function a() {
        echo self::$var;
    }

}

$rf = new ReflectionClass('Foo');

$var = $rf->getProperty('var');

$var->setAccessible(true);

$var->setValue('bar');

Foo::a();
```

扩展阅读：

> [Reflection](http://cn2.php.net/manual/zh/book.reflection.php)
