title: vim中去掉BOM
tags:
- PHP
date: "2014-04-27 22:50:41"
---

有关BOM（字节顺序标记）简介如下：
> 字节顺序标记（英语：byte-order mark，BOM）是位于码点U+FEFF的统一码字符的名称。当以UTF-16或UTF-32来将UCS/统一码字符所组成的字符串编码时，这个字符被用来标示其字节序。它常被用来当做标示文件是以UTF-8、UTF-16或UTF-32编码的记号。
由于BOM的存在，在php代码中有可能出现如下错误：
`Cannot modify header information - headers already sent by`

当我们可确定header之前无任何输出时候, 可考虑到由于BOM引起的问题

可考虑在vim编辑器中打开后进行如下操作删除BOM：
```
:set nobomb```

当需要增加BOM时候，使用如下操作增加BOM:

```
:set bomb
```

<!--more-->

扩展阅读：
> [字节顺序标记](http://zh.wikipedia.org/wiki/%E4%BD%8D%E5%85%83%E7%B5%84%E9%A0%86%E5%BA%8F%E8%A8%98%E8%99%9F)> 
> [BOM 字节顺序标记(ByteOrderMark)](http://baike.baidu.com/subview/126558/5073178.htm)
