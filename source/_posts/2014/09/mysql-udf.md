title: MySQL-udf
tags:
- MySQL
date: "2014-09-08 10:48:01"
---

## mysql-udf简介：

> The UDF repository for MySQL is a community project that aims to offer an organized collection of open source MySQL user defined functions.
> 
> MySQL UDFs offer a powerful way to extend the functionality of your MySQL database. The UDFs on this site are all free: free to use, free to distribute, free to modify and free of charge, even for commercial projects.
mysql-udf官方链接：

*   [http://www.mysqludf.org/](http://www.mysqludf.org/)


## 个人理解：

mysql-duf类似与php的扩展一样，通过编译、安装部分C/C++写的扩展，来完善mysql自由功能或提供mysql性能。

以下为安装lib_mysqludf_json简单过程记录，该扩展用于进行在mysql中支持json格式输出（不支持json格式输入）

## 安装示例

### 下载lib_mysqludf_json源码：

```
$ wget https://github.com/mysqludf/lib_mysqludf_json/archive/master.zip
```

### 解压、编译

```
$ unzip master.zip
Archive:  master.zip
37f851c808c4161beb4d5e535771dc0c59c82de6
   creating: lib_mysqludf_json-master/
  inflating: lib_mysqludf_json-master/README.md  
  inflating: lib_mysqludf_json-master/lib_mysqludf_json.c  
  inflating: lib_mysqludf_json-master/lib_mysqludf_json.html  
  inflating: lib_mysqludf_json-master/lib_mysqludf_json.so  
  inflating: lib_mysqludf_json-master/lib_mysqludf_json.sql  

$ cd lib_mysqludf_json
$ rm lib_mysqludf_json.so
$ gcc $(mysql_config --cflags) -shared -fPIC -o lib_mysqludf_json.so lib_mysqludf_json.c
```

可以看到编译生成的lib_mysqludf_json.so（此处可能需要先安装libmysqlclient-dev)

### 安装扩展，定义function
```
$ sudo mv lib_mysqludf_json.so /usr/lib/mysql/plugin/
```

/usr/lib/mysql/plugin/ 目录可能不同。

### 重启mysql

### 导入functions sql文件

```
$ mysql -p < lib_mysqludf_json.sql
```

需要注意，该SQL文件先进行了 `drop function`，但是由于 **funciton** 不存在，可能会报错。手动修改即可（删除也可)

### 进行简单的命令执行测试：

```

mysql> select lib_mysqludf_json_info();
+---------------------------------+
| lib_mysqludf_json_info()        |
+---------------------------------+
| lib_mysqludf_json version 0.0.2 |
+---------------------------------+
1 row in set (0.00 sec)

mysql> select json_array('a', 'b');
+----------------------+
| json_array('a', 'b') |
+----------------------+
|  ["a","b"]          |
+----------------------+
1 row in set (0.00 sec)

```

## 扩展阅读：

* [https://github.com/mysqludf/lib_mysqludf_json#readme
](https://github.com/mysqludf/lib_mysqludf_json#readme)
