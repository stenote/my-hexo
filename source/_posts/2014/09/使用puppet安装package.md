title: 使用puppet安装package
tags:
- puppet
date: "2014-09-14 11:13:36"
---

安装了 puppet 之后，package service 等可以使用 puppet 直接进行管理了，如下，使用 puppet resource package 可直接安装、卸载、更新 package。以 git 为例：

```
user@hostname: ~$ puppet resource package git ensure=present
```
