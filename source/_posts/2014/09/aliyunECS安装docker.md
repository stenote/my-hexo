title: aliyun ECS安装docker
tags:
- docker
date: "2014-09-23 22:07:25"
---

aliyun的ECS上有虚拟网卡，和docker0冲突，修改路由表即可：

```
$ sudo route del -net 172.16.0.0 netmask 255.240.0.0
```
