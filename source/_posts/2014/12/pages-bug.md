title: pages 的一个 bug
date: "2014-12-26 21:18:48"
---


发现个 #pages# 的 #bug# ： `大 (\u5927)` 用 `(\u7528)` 等转为 PDF , 会变为 ⼤ `(\u2f24)` 用 `(\u2f64)`. 已发现部分汉字有该问题!

谁乐意联系 [Apple.inc](http://www.apple.com) 谁联系~
