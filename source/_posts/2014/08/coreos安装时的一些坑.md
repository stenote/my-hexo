title: coreos安装时的一些坑
tags:
- coreos
- Linux
date: "2014-08-31 17:57:32"
---

coreos简介：

> CoreOS is a new Linux distribution that has been rearchitected to provide features needed to run modern infrastructure stacks. The strategies and architectures that influence CoreOS allow companies like Google, Facebook and Twitter to run their services at scale with high resilience.

coreos 安装到本地 disk 可以使用 coreos-installer 脚本进行安装，可在脚本中增加 `-c` 参数设定 coreos 的 yaml 配置，安装过程中遇到了很多坑，以此进行总结：

* cloud-config.yaml 文件中，需要开始增加 `#cloud-config`，这句属于不小心遗忘掉了
* cloud-config.yaml 文件，内容需要 yaml 格式正确，如果无法正常进行 yaml 解析，会出现安装不生效问题，可考虑对 cloud-config.yaml 文件内容进行在线验证，验证站点：[http://yaml-online-parser.appspot.com/](http://yaml-online-parser.appspot.com/ "http://yaml-online-parser.appspot.com/")
* cloud-config.yaml 文件内容中部分信息配置要与实际一致，例如设定 `static.network` 时候，如果网卡名称与配置内容不符合，会导致 cloud-config.yaml 文件不生效。
* coreos-installer 安装 coreos 的时候，需要联网安装，由于某些你懂的原因，会导致无法正常安装，需要科学上网，具体方法各位自行查找，珍重。

祝你安装 coreos 成功。
