title: hiera-MySQL安装记录
tags:
- hiera
- MySQL
- puppet
- Ruby
date: "2014-08-12 21:42:12"
---

今天尝试对 hiera-mysql 进行安装，过程中遇到一些文件，简单进行记录。
首先，进行 gem 的 sources 修改(增加淘宝的rubysources [https://ruby.taobao.org](https://ruby.taobao.org)，如下：

```
$ sudo -i
# gem sources --list
*** CURRENT SOURCES ***

http://rubygems.org/

# gem sources --remove http://rubygems.org/
# gem sources --add https://ruby.taobao.org/
https://ruby.taobao.org/ added to sources
# gem sources --list
*** CURRENT SOURCES ***

https://ruby.taobao.org/
```

首先，尝试直接 gem 进行 hiera-mysql 的安装。

如下：

```
# gem install hiera-mysql -V
```
提示错误：

```
# ERROR: Failed to build gem native extension.

/usr/lib/ruby/1.9.1/rubygems/custom_require.rb:36in `require`: cannot load such file --mkmf (LoadError)
```

貌似是加载不了什么文件。

考虑到没有安装ruby-dev。尝试先安装ruby-dev试试：

```
# aptitude install ruby-dev
```

尝试重新进行hiera-mysql安装，
错误信息变为了：

```
Could not create Makefile due to some reason, probably lack of necessary libraries and/or headers. Check the mkmf.log file for more details. You may need configuration options.
```

没有找到mysql的相关信息，安装mysql-client(dev)

```
# aptitude install libmysqlclient-dev mysql-client mysql-server
```

尝试重新进行hiera-mysql安装。
错误信息变为了：

```
make
sh: 1: make: not found
```

安装build-essential

```
# aptitude install build-essential
```

重新进行 hiera-mysql 安装
安装成功。查看 gem list

```
# gem list

*** LOCAL GEMS ***

hiera-mysql (0.2.0)
mysql (2.9.1)
```

创建 hiera.yaml 配置文件，增加 backends 为 mysql，同时设定 mysql 相关信息：

```
---
:bacnekds:
  - mysql
:mysql:
  :host: localhost
  :user: root
  :pass: root
  :database: hiera
  :query: SELECT `value` FROM `data` WHERE `key` = '%{key}';
...
```

mysql创建数据如下：

```
mysql> CREATE DATABASE hiera;
mysql> USE hiera;
mysql> CREATE TABLE data (
    key VARCHAR(20),
    value VARCHAR(20)
);
mysql > INSERT INTO data VALUES ('foo', 'bar');
```

创建了 hiera 对应的查询数据库 hiera，对应的数据表 data，表有 key 和 value 两列。
key 对应的为 hiera 查询的列，value 为数据存储列。

尝试进行 hiera-mysql 数据调用

```
# hiera -c /path/to/hiera_config.yaml foo --debug

root@client:~# hiera -c hiera.yaml  foo --debug
DEBUG: 2014-08-12 21:35:23 +0800: mysql_backend initialized
DEBUG: 2014-08-12 21:35:23 +0800: mysql_backend invoked lookup
DEBUG: 2014-08-12 21:35:23 +0800: resolution type is priority
DEBUG: 2014-08-12 21:35:23 +0800: Executing SQL Query: SELECT `value` FROM `data` WHERE `key` = 'foo';
DEBUG: 2014-08-12 21:35:23 +0800: Mysql Query returned 1 rows
DEBUG: 2014-08-12 21:35:23 +0800: Mysql value : bar
bar
```
如上，正常通过mysql获取数据为bar。

总结：
hiera-mysql 安装，需要注意以下几点：
1、gem 源修改为淘宝，为了某些原因（你懂的）
2、安装 ruby-dev，避免无法找到相关依赖
3、安装 libmysql-client-dev，避免无法找到相关依赖
4、安装 build-essential，避免无法编译

祝你好运！
