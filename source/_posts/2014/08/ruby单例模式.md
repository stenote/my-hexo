title: ruby单例模式
tags:
- Ruby
date: "2014-08-24 23:13:25"
---

```
#!/usr/bin/ruby
#encoding: utf-8

class Logger

    @@logger = nil

    private_class_method :new

    def new
    end

    def self.instance
        @@logger = new unless @@logger
        @@logger
    end

end

l1 = Logger.instance
l2 = Logger.instance

puts l1.object_id == l2.object_id

```
