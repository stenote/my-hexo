title: puppet 覆盖模型
tags:
- puppet
date: "2014-07-06 20:22:19"
---

puppet的类拥有一个简单的继承——覆盖模型。我们可以通过对子类进行父类的继承，来达到对父类中的一个或者多个值的覆盖。

如下：
```
class bind::service  {
    service { "bind" :
        hasstatus => true,
        hasrestart => true,
        enable => true,
    }
}
```

我们进行一个继承

```
class bind::service::enable inherits bind::service {
    Service["bind"] {
        ensure => running,
        enable => true,
    }
}
 
class bind::service::disable inherits bind::service {
    Service["bind"] {
        ensure => stopped,
        enable => false,
    }
}
```

如上，我们进行了属性的增加。在继承时候，我们补充新增属性即可增加属性。
继承后实际结果为：

```
class bind::service::enable {
    service { "bind" :
        hasstatus => true,
        hasrestart => true,
        enable => true,
        ensure => running,
        enable => true,
    }
}
 
class bind::service::disable {
    service { "bind" :
        hasstatus => true,
        hasrestart => true,
        enable => true,
        ensure => stopped,
        enable => false,
    }
}
```

我们还可进行属性进行扩展

```
class bind {
    service {"bind" :
        require => Package["bind"],
    }
}
 
class bind::server inherits bind {
    Service["bind"] {
        require +> Package["bind-libs"],
    }
}
```

如上，我们进行了已有属性值的增加。
最终结果为 

```
class bind::server {
    service{ "bind" :
        require => Package["bind-libs", "bind"],
    }
}
```

我们还可对部分属性进行撤销

```
class bind {
    service {"bind" :
        require => Package["bind"],
    }
}
 
class bind::client inherits bind {
    Service["bind"] {
        require => undef,
    }
}
```

我们可使用undef对已有属性进行删除。
最终结果为

```
class bind::client {
    service { "bind" :
    }
}
```
