title: puppet user password
tags:
- openssl
- PHP
- puppet
date: "2014-07-09 22:13:01"
---

puppet 在创建 user 资源的时候，通常需要设定 password。
可以使用如下方法设定 password 后进行填充到 user 资源的 password 属性中即可：

```
$ openssl passwd -1
$ Password:
$ Verifying - Password:
$ #输出结果  1$MEKQwQz8$1YodG/Qk7lqK6lATOo4B5/
```

两次输入相同密码后，openssl 加密后结果会输出到 STDOUT 中。设定该输入结果为 user 资源的 password 属性即可。

需要注意，多次进行 `openssl passwd -1`的结果不一定相同！

PS: -1表示： MD5-based password algorithm，通常增加-1后的结果与 /etc/shadow 中的显示类似，但是如果不增加-1，生成密码也不影响使用。

php也提供了类似函数进行加密，如下，可使用简单cli命令设定密码
```
$ php -r "echo password_hash('123456', PASSWORD_DEFAULT);"
```
扩展阅读：

* [password_hash — Creates a password hash](http://cn2.php.net/manual/en/function.password-hash.php "http://cn2.php.net/manual/en/function.password-hash.php")
* [Puppet Type Reference #User](http://docs.puppetlabs.com/references/latest/type.html#user "http://docs.puppetlabs.com/references/latest/type.html#user")
* [OpenSSL](http://www.openssl.org/ "http://www.openssl.org/")

### [罗大锤，加油！](http://www.openssl.org/support/acknowledgments.html "http://www.openssl.org/support/acknowledgments.html")
