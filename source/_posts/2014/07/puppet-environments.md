title: puppet environments
tags:
- puppet
date: "2014-07-10 23:46:24"
---

puppet可在master设定不同的environment环境。不同的agent根据不同的需求来获取不同的environment的pp文件，这样可以很好得进行一个puppet环境隔离。



如下，master需要增加environment简单配置如下：

```
[development]
modulepath = $confdir/environments/development/modules
manifest = $confdir/enviroments/development/manifests/sites.pp

[testing]
modulepath = $confdir/environments/testing/modules
manifest = $confdir/environments/testing/manifests/site.pp
```

修改后目录结构如下：

```

root@master:/etc/puppet# tree .
.
├── auth.conf
├── environments
│   ├── development
│   │   ├── manifests
│   │   │   └── site.pp
│   │   └── modules
│   └── testing
│       ├── manifests
│       │   └── site.pp
│       └── modules
├── etckeeper-commit-post
├── etckeeper-commit-pre
├── files
│   └── foo
├── fileserver.conf
├── manifests
│   └── site.pp
├── modules
├── puppet.conf
└── templates

11 directories, 9 files
```
修改完成后重启 puppetmaster。

/etc/puppet/environments/testing/manifests/site.pp 内容如下 :

```
package { 'htop' :
    ensure => absent,
}
```
如上，进行 htop 删除

/etc/puppet/manifests/sites.pp内容如下 :

```
package { 'git' :
    ensure => present,
}

```
如上，安装 git。

agent进行agent test的时候增加 `environments` 参数后会获取 /etc/puppet/environments/testing/manifests/sites.pp 文件。如下：


```
root@client:~# puppet  agent  -t --environment testing --verbose --noop
Info: Retrieving plugin
Info: Caching catalog for agent
Info: Applying configuration version '1405003274'
Notice: /Stage[main]/Main/Package[htop]/ensure: current_value 1.0.2-3, should be absent (noop)
Notice: Class[Main]: Would have triggered 'refresh' from 1 events
Notice: Stage[main]: Would have triggered 'refresh' from 1 events
Notice: Finished catalog run in 0.33 seconds
```

发现尝试对 htop 进行 absent，没有尝试对 git 进行 present。

PS：我们可以尝试创建一个 git 库，分别有 `development`、`testing`、`master` 三个分支，分别在如上不同的 environment 目录切换到不同分支，等待 development 稳定后 merge 到 `testing`， test 通过后 merge 到 `master` 进行部署。再同步将 `master`分支 merge 到 `develop`、`testing` 分支。貌似是一个不错的好办法。

^_^
（突然发现昨天发[罗大锤加油](http://www.mmmmm.io/2014/07/puppet-user-password/ "http://www.mmmmm.io/2014/07/puppet-user-password/")后，昨天是罗大锤的生日）
