title: rsync使用非22端口进行文件上传
tags:
- Linux
- rsync
date: "2014-07-29 22:47:57"
---

为了安全，修改了 **ssh** 端口为 **9527**（为方便写博客，使用了假的端口号），但是发现 **rsync** 无法正常上传文件。查找相关文档后发现可使用如下命令进行文件上传

```

$ rsync -avzP -e 'ssh -p 9527' file.pdf user@mmmmm.io:~/
user@mmmmm.io's password:
building file list ...
1 file to consider
file.pdf
    28495403 100%  243.62kB/s    0:01:54 (xfer#1, to-check=0/1)

sent 28027168 bytes  received 42 bytes  206842.88 bytes/sec
total size is 28495403  speedup is 1.02
```
