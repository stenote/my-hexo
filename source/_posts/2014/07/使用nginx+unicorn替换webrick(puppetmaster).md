title: 使用nginx + unicorn 替换 webrick（puppetmaster）
tags:
- nginx
- puppet
- Ruby
date: "2014-07-13 15:02:31"
---

puppetmaster 自带了 webrick 作为自身的 web server，但是 webrick 的执行效率相对比较低下，需要考虑使用更加专业的 web server 进行替换，解决并发问题。

网上简单搜索后的解决办法通常分如下几种，如下：
1、apache + passenger
2、nginx + mongrel
3、nginx + unicorn

由于不会apache，故考虑使用后两种方法，实际尝试替换过程中，mongrel没有正常安装上（安装时没有安装ruby-dev和build-essential导致无法gem install mongrel），unicorn安装上了，故考虑使用第三种方法进行webrick替换。以下为简单安装过程。


### 1、安装unicorn

安装完 puppetmaster，会自动安装 ruby、gem 等依赖包。故可考虑直接使用 gem 命令安装 unicorn。由于 gem 包安装需要进行编译，故考虑先安装 build-essential

```
$ apt-get install build-essential ruby-dev && gem install unicorn --verbose
```

安装完成后，查看是否正常安装unicorn：

```
$ gem list

*** LOCAL GEMS ***

kgio (2.9.2)
rack (1.5.2)
raindrops (0.13.0)
unicorn (4.8.3)
```

### 2、安装nginx

```
$ puppet resource package nginx ensure=present
#实际还是会调用apt-get install nginx 
```

安装完成关闭nginx

```
$ puppet resource service nginx ensure=stopped
# service nginx stop
```

### 3、配置unicorn

增加rake配置文件

```
$ cp /etc/puppet/ && wget https://raw.githubusercontent.com/puppetlabs/puppet/master/ext/rack/config.ru
```

增加unicorn.conf配置文件

/etc/puppet/unicorn.conf

```
worker_processes 8
working_directory "/etc/puppet"
listen '/var/run/puppet/puppetmaster_unicorn.sock', :backlog => 512
timeout 120
pid "/var/run/puppet/puppetmaster_unicorn.pid"
 
preload_app true
if GC.respond_to?(:copy_on_write_friendly=)
  GC.copy_on_write_friendly = true
end
 
before_fork do |server, worker|
  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end
```

增加puppetmaster-unicoron服务管理脚本

```
#/etc/init.d/puppetmaster-unicorn
内容如下:


#!/bin/bash
# unicorn-puppet
lockfile=/var/lock/puppetmaster-unicorn
pidfile=/var/run/puppet/puppetmaster_unicorn.pid

RETVAL=0
DAEMON=/usr/local/bin/unicorn
DAEMON_OPTS=&quot;-D -c /etc/puppet/unicorn.conf&quot;
USER=root

start() {
    sudo -u $USER $DAEMON $DAEMON_OPTS
    RETVAL=$?
    [ $RETVAL -eq 0 ] &amp;&amp; touch &quot;$lockfile&quot;
    echo
    return $RETVAL
}

stop() {
    sudo -u $USER kill `cat $pidfile`
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] &amp;&amp; rm -f &quot;$lockfile&quot;
    return $RETVAL
}

restart() {
    stop
    sleep 1
    start
    RETVAL=$?
    echo
    [ $RETVAL -ne 0 ] &amp;&amp; rm -f &quot;$lockfile&quot;
    return $RETVAL
}

condrestart() {
    status
    RETVAL=$?
    [ $RETVAL -eq 0 ] &amp;&amp; restart
}

status() {
    ps ax | egrep -q &quot;unicorn (worker|master)&quot;
    RETVAL=$?
    return $RETVAL
}

usage() {
    echo &quot;Usage: $0 {start|stop|restart|status|condrestart}&quot; &gt;&amp;2
    return 3
}

case &quot;$1&quot; in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    condrestart)
        condrestart
        ;;
    status)
        status
        ;;
    *)
        usage
        ;;
esac

exit $RETVAL
```

### 4、nginx增加配置

```
#/etc/nginx/sites-enable/puppetmaster
内容如下:


upstream puppetmaster_unicorn {
    server unix:///var/run/puppet/puppetmaster_unicorn.sock fail_timeout=0;
}

server {
    listen 8140;

    ssl on;
    ssl_session_timeout 5m;
    ssl_certificate /var/lib/puppet/ssl/certs/master.pem;
    ssl_certificate_key /var/lib/puppet/ssl/private_keys/master.pem;
    ssl_client_certificate /var/lib/puppet/ssl/ca/ca_crt.pem;
    ssl_ciphers SSLv2:-LOW:-EXPORT:RC4+RSA;
    ssl_verify_client optional;

    root /usr/share/empty;

    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Client-Verify $ssl_client_verify;
    proxy_set_header X-Client-DN $ssl_client_s_dn;
    proxy_set_header X-SSL-Issuer $ssl_client_i_dn;
    proxy_read_timeout 120;

    location / {
        proxy_pass http://puppetmaster_unicorn;
        proxy_redirect off;
    }
}
```

启动nginx

5、修改pupetmaster配置

```
#/etc/puppet/puppet.conf
内容如下:

[master]
# These are needed when the puppetmaster is run by passenger
# and can safely be removed if webrick is used.
#ssl_client_header = SSL_CLIENT_S_DN
#ssl_client_verify_header = SSL_CLIENT_VERIFY

ssl_client_header = HTTP_X_CLIENT_DN
ssl_client_verify_header = HTTP_X_CLIENT_VERIFY
```

### 6、启动服务

启动puppet-unicoron

```
$ /etc/init.d/puppets-unicorn start
# service puppets-unicorn start
```

启动nginx
```
$ puppet resource service nginx ensure=running
# service nginx start
```

### 7、收尾工作
取消 puppetmaster 的开机自启动
增加 puppetmaster-unicorn 的开机自启动
增加 nginx 的开机自启动（nginx安装完成就会开机自启动）

### 简单原理：

nginx 使用 ssl 监听 puppet 原有 8140 端口，将 web 请求分发到 puppetmaster_unicorn.sock文件，unicorn 监听该 sock 文件，有 sock 请求后，分发到 puppetmaster 进行请求分析、返回。由于nginx 自身负载能力较强，并且 rake 的多 work 并发，执行效率相对原有 webrick 更高。

### 扩展阅读：

* [webrick](http://rubydoc.info/gems/webrick/1.3.1/frames "http://rubydoc.info/gems/webrick/1.3.1/frames")
* [unicorn](http://unicorn.bogomips.org/ "http://unicorn.bogomips.org/")
* [nginx ssl
](http://nginx.org/en/docs/mail/ngx_mail_ssl_module.html "http://nginx.org/en/docs/mail/ngx_mail_ssl_module.html")
