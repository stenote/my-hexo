title: microtime时间错误解决办法
tags:
- PHP
date: "2014-05-05 21:51:56"
---

> microtime() returns the current Unix timestamp with microseconds. This function is only available on operating systems that support the gettimeofday() system call.
但是实际在使用中，有可能出现部分错误，如下：

```php
<?php

$now = microtime();

do_something();

echo microtime() - $now;

```

<!--more-->

如上，我们在执行 do_something() 函数之前先通过 microtime() 进行了时间记录, 执行完 do_something() 后再做减法获取到 do_something() 函数的执行耗时.
但是实际中有可能会出现负值，如下为在tavis-ci中的一次调用中遇到的情况：

[https://travis-ci.org/stenote/SUM/jobs/24371337](https://travis-ci.org/stenote/SUM/jobs/24371337)

解决办法如下：

```
function microtimefloat() {
    list($usec, $sec) = explode(' ', microtime());
    return ((float)$usec + (float)$sec);
}

```

封装 microtime() 后使用microtime_float()进行调用记录

PS： microtime()可传入true参数返回浮点数直接解决该问题。

[http://www.php.net/manual/zh/function.microtime.php](http://www.php.net/manual/zh/function.microtime.php)
