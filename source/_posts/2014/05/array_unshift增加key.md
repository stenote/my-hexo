title: array_unshift 增加 key
tags:
- PHP
date: "2014-05-22 16:02:05"
---

array_unshift函数说明如下：
> int array_unshift ( array &$array , mixed $var [, mixed $... ])> 
> array_unshift() 将传入的单元插入到 array 数组的开头。注意单元是作为整体被插入的，因此传入单元将保持同样的顺序。所有的数值键名将修改为从零开始重新计数，所有的文字键名保持不变。


如上，只可传入 var 值，不可 unshift 设定 key 值和 value.

可考虑使用如下代码,即可完成对array_unshift增加key: 

```php
<?php
$array = array($key => $value) + $array;
```
