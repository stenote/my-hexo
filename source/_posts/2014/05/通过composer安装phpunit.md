title: composer 安装 phpunit
tags:
- composer
- PHP
- phpunit
date: "2014-05-11 18:04:02"
---

今天重装了操作系统, 不知为何，14.04 LTS 在我的 pc 上运行很不稳定. 安装完成后需要安装phpunit，考虑到 phpunit 可使用 composer 进行安装，故尝试用 composer 安装 phpunit，替换原有对 pear 安装 phpunit 和 phar 包安装 phpunit. 安装步骤简单进行记录, 过程如下: 

1、下载composer

```
may@ubuntu:~$ curl -sS https://getcomposer.org/installer | php
```

2、安装composer

```
may@ubuntu:~$ sudo mv composer.phar /usr/bin/composer
```

3、composer global安装phpunit

```
may@ubuntu:~$ composer global require phpunit/phpunit
Changed current directory to /home/may/.composer
Please provide a version constraint for the phpunit/phpunit requirement: 4.1.*@dev
./composer.json has been updated
Loading composer repositories with package information
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing sebastian/exporter (1.0.1)
    Downloading: 100%         

  - Installing sebastian/diff (1.1.0)
    Downloading: 100%         

  - Installing sebastian/comparator (1.0.0)
    Downloading: 100%         

  - Installing phpunit/php-text-template (1.2.0)
    Downloading: 100%         

  - Installing phpunit/phpunit-mock-objects (2.1.0)
    Downloading: 100%         

  - Installing sebastian/version (1.0.3)
    Downloading: 100%         

  - Installing sebastian/environment (1.0.0)
    Downloading: 100%         

  - Installing phpunit/php-token-stream (1.2.2)
    Downloading: 100%         

  - Installing phpunit/php-file-iterator (1.3.4)
    Downloading: 100%         

  - Installing phpunit/php-code-coverage (2.0.6)
    Downloading: 100%         

  - Installing symfony/yaml (v2.4.4)
    Downloading: 100%         

  - Installing phpunit/php-timer (1.0.5)
    Downloading: 100%         

  - Installing phpunit/phpunit (4.1.x-dev ac80683)
    Cloning ac806838558a0e9b721bc50cac46f9baa4a4e1db

phpunit/php-code-coverage suggests installing ext-xdebug (<=2.2.1)
phpunit/phpunit suggests installing phpunit/php-invoker (~1.1)
Writing lock file
Generating autoload files
```

4、增加composer vendor/bin到PATH
修改.bashrc增加：

```
if [ -d ~/.composer/vendor/bin ]; then
    PATH=$PATH:~/.composer/vendor/bin
fi
```
