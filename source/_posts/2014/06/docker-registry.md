title: docker-registry
tags:
- docker
- Linux
date: "2014-06-29 12:40:52"
---

国内访问[http://index.docker.io](http://index.docker.io ) (现为[http://hub.docker.com](http://hub.docker.com)) 速度极慢，并且 `docker pull image`的时候，速度奇慢, 所以我打算自己搭建一个 registry

搭建过程如下

<!--more-->

1、pull 下 registry
```
$ docker pul registry:latest
```

如果 pull 太慢，可考虑自行在国外 vps 上 pull 下来，打成 tar 包传到国内来

2、运行registry image

```
$ docker run --name=registry -d -p 5000:5000 registry
```

创建一个基于 registry 的 container，并把 5000 端口抛出，新生成 container 名为 registry

3、访问当前站点5000端口，查看是否可用

```
$ curl localhost:5000
```

结果如下：
```
"docker-registry server (dev) (v0.7.3)"
```

4、对本地需要进行push的image，创建新tag
举例，如下为我的docker的images（部分多余列已经删除）

```
REPOSITORY                       TAG                 IMAGE ID           
ubuntu                           14.04               e54ca5efa2e9
```

我们对这个ubuntu:14.04创建一个新的tag

```
$ docker tag e54ca5efa2e9 localhost:5000/ubuntu/ubuntu:14.04
```

然后进行push

```
$ docker push localhost:5000/ubntu/ubuntu:14.04
```

结果如下：
```
The push refers to a repository [localhost:5000/ubuntu/ubuntu] (len: 1)
Sending image list
Pushing repository localhost:5000/ubuntu/ubuntu (1 tags)
Image 511136ea3c5a already pushed, skipping
Image d7ac5e4f1812 already pushed, skipping
Image 2f4b4d6a4a06 already pushed, skipping
Image 83ff768040a0 already pushed, skipping
Image 6c37f792ddac already pushed, skipping
Image e54ca5efa2e9 already pushed, skipping
Pushing tag for rev [e54ca5efa2e9] on {http://localhost:5000/v1/repositories/ubuntu/ubuntu/tags/14.04}
```

如上，我们已正常push了一个image到自己的registry上

5、获取registry上的image

```
docker pull localhost:5000/ubuntu/ubuntu:14.04
```

docker 在 pull 和 push 的时候，完全按照 image 的名称进行判定 pull、push 的路径，结构如下：

```
domain:port/vendor_name/product_name:tag_name
```

它没有像 git 一样，可创建多个 remote，只能通过 push pull 的路径, 来判断 registry 地址.
