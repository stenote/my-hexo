title: rsync
tags:
- Linux
- rsync
date: "2014-06-21 13:42:37"
---

rsync简介：

> rsync is a utility software and network protocol for Unix-like systems (with a port to Microsoft Windows) that synchronizes files and directories from one location to another while minimizing data transfer by using delta encoding when appropriate. It also has the option to provide encrypted transfer by use of SSH.


通常我们通常可使用rsync代替scp进行linux操作系统之间数据传输，如下：

```
$ scp -r composer user@mmmmm.io:~/websites/
```

我们可使用rsync代替如上命令：

```
$ rsync -avzP composer user@mmmmm.io:~/websites/
```

如上两种方法，使用的是 ssh 进行文件、目录上传，由于 rsync 有自己独特的算法，导致上传、下载效率要优于 ssh


如上的方法的可能带来的缺陷如下：

1. 需要配置ssh-server
2. 需要配置特定的rsync用户进行同步，并且在服端配置授权公钥

rsync还提供另外一种数据方式：rsyncd：

1. 服务端配置 rsyncd 服务
2. rsyncd 设定可 rsync 路径、执行用户、授权账号、密码
如下为blog所在rsync配置：

```
#/etc/rsyncd.conf

max connections = 100
use chroot = no
log file = /var/log/rsyncd.log
pid file = /var/run/rsyncd.pid
lock file = /var/run/rsyncd.lock

[may]
path = /home/may
uid = may
gid = dev
read only = true
auth users = rsync
secrets file=/etc/rsync.pwd #设定验证，格式 用户:密码
transfer logging = yes
```

如上配置情况下，我们只需要执行如下命令即可进行 /home/may 目录的增量传输：

```
rsync -avzP rsync@mmmmm.io::may .
```

扩展阅读

> [http://rsync.samba.org/](http://rsync.samba.org/)
> 
> [http://zh.wikipedia.org/zh-cn/Rsync](http://zh.wikipedia.org/zh-cn/Rsync)
> 
> [http://www.perihel.at/3/index.html#rsync](http://www.perihel.at/3/index.html#rsync)
