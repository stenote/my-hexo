title: wordpress小插件分享
date: "2014-06-16 22:46:30"
tags:
---

推荐两个wordpress的小插件：

> [多说-社会化评论系统](http://duoshuo.com/)
> [七牛镜像存储](http://wordpress.org/plugins/wpjam-qiniu/)

"多说-社会化评论系统" 是一个可以替换 wordpress 评论功能的插件，效果如现在你看到的下面的评论的地方那样，支持多种账号登陆进行 wordpress 博文评论功能。这个插件在绑定了微博账号后，还可同步博客更新到微博~。甚好

"七牛镜像存储"是一个可将 blog 上的资源文件进行缓存到 cdn 中的一个插件，可加快页面访问。效果如你现在看不到的那样，速度提升了 70%。

PS：[小张](http://hengtian.org)的vps很稳定啊
