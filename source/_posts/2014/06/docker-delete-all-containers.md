title: docker 删除所有 container
tags:
- docker
date: "2014-06-08 16:45:27"
---

快速删除 docker 的所有的 container

```
$ docker rm `docker ps -aq`
```
