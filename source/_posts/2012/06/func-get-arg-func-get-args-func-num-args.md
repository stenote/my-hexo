title: func_get_arg func_get_args func_num_args()
tags:
- PHP
date: "2012-06-22 07:25:24"
---

<div>在某个函数或者方法里面，为了方便确认传入到参数，php提供了三个方便到函数</div>
<div>array func_get_args()</div>
<div>用于返回所有传入参数到一个数组。</div>
<div>in func_num_args() 相当于count(func_get_args())</div>
<div>func_get_arg(int) 相当与func_get_args()[int] //php5.4中已支持。未亲测</div>
<div>通常。进行方法转发到函数中，我们使用这个func_get_args进行传入参数获取，然后通过call_user_func_array进行方法转发。</div>
<div>
</div>
<div>php5.4中执行如下代码</div>
<div>function test($a, $b) {</div>
<div>    var_dump(func_get_arg(1) == func_get_args()[1]);</div>
<div>}</div>
<div>
</div>
<div>test(1, 2, 3);</div>
<div>页面显示bool(true)</div>
<div>globa $args = func_get_args();</div>
<div>func_get_arg($key) {</div>
<div>    return $args[$key];</div>
<div>}</div>
