title: mongodb 简介和相关服务
tags:
- mongodb
date: "2012-06-06 06:23:17"
---

<div>mongodb 是一个主流的NoSQL数据库服务器，默认占用27017端口，还可进行REST，开启一个REST服务，REST服务占用端口为默认端口加1000 （28017端口）</div>
<div>linux下安装</div>
<div>$ sudo apt-get install mongodb</div>
<div>服务启动 </div>
<div>$ sudo service mongodb start</div>
<div>$ sudo service mongodb stop</div>
<div>开启REST服务</div>
<div>$ sudo service mognodb stop //先停止服务</div>
<div>$ sudo /usr/bin/mongod --rest //启动REST服务</div>
<div>mongodb 配置文件在/etc/mongodb.conf 下进行配置</div>
<div>开启REST服务后可在http://localhost:28017 http://loaclhost:27017下查看</div>
<div>
</div>
<div>mongodb 提供了一个命令，这个命令可打开mongodb 的shell</div>
<div>$ mongo</div>
<div>MongoDB shell version: 2.0.4</div>
<div>connecting to: test</div>
<div>&gt; </div>
<div>页面显示效果如上：</div>
<div>
</div>
<div>show dbs; //显示所以的database;</div>
<div>use test; //切换到test 数据库，如果不存在，则创建该数据库，并且将db变量设定为当前数据库</div>
<div>db; //显示当前db变量值</div>
<div>
</div>
<div>mongodb中，可以按照javascript的基本语法进行执行。并且包含了javascript的基本库类。Math.sqrt()等方法都可使用</div>
<div>mongodb中，如果需要显示某个变量的制，或者某个方法的真是内容，直接输入这个变量名称回车即可查看到</div>
<div>例如，use test;后，var db = test; // test为一个数据库对象</div>
<div>db.users.find //返回find方法的caller，我忘记怎么说了，就是返回这个方法对应的函数源码</div>
<div>
</div>
<div>一个database里一般有多个tables</div>
<div>mysql等关系型数据库中，show tables;可查看所有的table</div>
<div>在mongodb中,table有另外一种表示方法，我们称之为collection，</div>
<div>show collections; 可查看出所有的表。</div>
<div>
</div>
<div>我们对某个table进行数据填写时候，需要先创建这个table的基本的格式。create table user( xxxx等等限定条件。</div>
<div>然后执行SQL语句insert into table_name (col1, col2, col3, ....) value (value1, value2, value3,....);</div>
<div>
</div>
<div>mongodb中无需创建特定格式的collection即可进行数据增加</div>
<div>&gt; db.users.insert({&quot;name&quot;: &quot;may&quot;});</div>
<div>上述命令就会在当前db创建一个users的collection，然后给这个collection增加一个对象{&quot;name&quot;: &quot;may&quot;};</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
