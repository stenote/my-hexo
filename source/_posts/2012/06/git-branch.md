title: Git branch
tags:
- Git
date: "2012-06-08 20:16:21"
---

<div>
<div>git branch命令是对git中的branch进行相关操作使用的命令，通常和git checkout、git merge命令匹配使用</div>
<div>
</div>
<div>创建一个新的branch使用命令</div>
<div>$ git branch new_branch</div>
<div>
</div>
<div>切换到这个new_branch分支使用命令</div>
<div>$ git checkout new_branch</div>
<div>
</div>
<div>
<div>上述两个命令可以合并为一个命令</div>
<div>
</div>
<div>$ git checkout -b new_branch</div>
</div>
<div>
</div>
<div>通过git status可以查看当前分支</div>
<div>$ git status</div>
<div># On branch new_branch</div>
<div>或者</div>
<div>$ git branch</div>
<div>查看当前分支，可能显示情况如下：</div>
<div>   develop</div>
<div>   master</div>
<div>* new_branch    //当前所在分支为new_branch</div>
<div>
</div>
<div>删除某个分支</div>
<div>git branch -d</div>
<div>git branch -D</div>
<div>d、D的意思即为delete</div>
<div>d为merge后正常删除</div>
<div>D为强制删除</div>
<div>
</div>
<div>多个分支不同情况下需要进行内容合并，使用git merge进行合并操作。</div>
<div>假设现在没有任何分支，进行创建</div>
<div>$ git branch branch_a</div>
<div>$ git branch branch_b</div>
<div>$ git checkout branch_a</div>
<div>$ echo 'a'  &gt; index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'touch index.php on branch_a'</div>
<div>$ git branch b</div>
<div>$ echo 'b' &gt; index.php</div>
<div>$ git add index.php</div>
<div>$ git commit -m 'touch index.php on branch_b'</div>
<div>$ git merge branch_a</div>
<div>这个时候提示错误</div>
<div>

Auto-merging index.php
CONFLICT (add/add): Merge conflict in index.php
Automatic merge failed; fix conflicts and then commit the result.

<div>大致意思为自动合并index.php失败，请手动修复并提交结果</div>
<div>$ git status //查看一下当前状况，发现index.php前面标明 both added，意为2者都进行了add，需要选择增加的</div>
<div>

$ vi index.php

&lt;&lt;&lt;&lt;&lt;&lt;&lt; HEAD
b
=======
a
&gt;&gt;&gt;&gt;&gt;&gt;&gt; a

</div>
<div>=====作为分割线，&lt;&lt;&lt;&lt;&lt;&lt;和&gt;&gt;&gt;&gt;&gt;标明分支名称，HEAD表示当前分支，b内容即为branch_b上的内容，a即为branch_a上的内容</div>
<div>修改内容为</div>
<div>a</div>
<div>b</div>
<div>正常add和commit.</div>
<div>cat index.php发现内容为</div>
<div>a</div>
<div>b</div>
<div>没有问题</div>
<div>这个时候完成了两分支合并，删除branch_a即可</div>
<div>$ git branch -d branch_a  </div>
<div>提示Deleted branch a (was 6639ebf).</div>
<div>
</div>
<div>以上为出现错误合并的现象，请自行尝试不冲突的文件合并（比如两分支有两个名称不相同的文件的合并）</div>
</div>
</div>
