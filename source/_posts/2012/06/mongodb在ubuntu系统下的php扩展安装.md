title: mogodb php ubuntu下的相关安装
tags:
- mongodb
date: "2012-06-09 06:22:07"
---


#### 扩展安装

首先安装 `php`。具体忘记了。
然后执行如下命令，安装 `mongo` 扩展:
```
$ sudo pecl install mongo
```

如果提示出错，请执行下面命令:
```
$ sudo apt-get update //进行apt升级
$ sudo apt-get install php5-dev php5-cli php-pear //安装相关php组件
$ sudo pecl install mongo
```

安装完毕后，按照提示，增加 **extension**

`/etc/php5/conf.d/` 下创建 `mongo.ini`, 写入 `extension=mongo.so`

`mongo.so` 的位置应该在 `/usr/lib/php5/2XXXX`内，完成之后重启 `php`进程, 重启 **apache** 等相关 web 服务服务

#### 测试安装结果

创建 php 文件，写入如下内容:

```
<?php
$conn = new Mongo();
var_dump($conn->listDBS());

```

执行上述代码查看结果。如果没有提示 `Mongo` 相关错误。那么说明安装正确。提示有错误。可能是 exetnsion 没有增加</div>

#### 扩展阅读

[`MongoDB` 相关函数手册](http://www.php.net/manual/zh/class.mongo.php)
