title: upsert multi
tags:
- mongodb
date: "2012-06-09 07:10:45"
---

<div>update命令还有两个参数，分别位列第三第四位置，这个两个参数类型为bool。默认值都为false</div>
<div>update命令全部为</div>
<div>db.collectionName.update(query, obj, upsert, multi);</div>
<div>upsert为是否对搜索不到的对象进行创建。</div>
<div>multi为是否多全部搜索到的对象进行处理。</div>
<div>先说upsert</div>
<div>&gt; db.users.find();</div>
<div>

{ &quot;_id&quot; : ObjectId(&quot;4fd20d79931bd9b6138172f8&quot;), &quot;user&quot; : &quot;may&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fd20e22931bd9b6138172f9&quot;), &quot;user&quot; : &quot;may&quot;, &quot;age&quot; : 20 }

</div>
<div>

我们可以看到这个collection中共有两个文档

&gt; db.users.update({&quot;school&quot;: &quot;tjut&quot;}, {&quot;$set&quot;: {&quot;user&quot;: &quot;kai&quot;}}, true);

我们执行上述命令，这个命令的意思是，尝试获取某个school为tjut的记录，并设定user为kai，如果不存在，则增加这个记录。

我们执行这个命令后查看记录

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd20d79931bd9b6138172f8&quot;), &quot;user&quot; : &quot;may&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fd20e22931bd9b6138172f9&quot;), &quot;user&quot; : &quot;may&quot;, &quot;age&quot; : 20 }
{ &quot;_id&quot; : ObjectId(&quot;4fd20e89939e6548f004bfe8&quot;), &quot;school&quot; : &quot;tjut&quot;, &quot;user&quot; : &quot;kai&quot; }

我们发现增加了school为tjut，user为kai的文档。

multi

可多多个记录进行设定

&gt; db.users.update({&quot;user&quot;: &quot;may&quot;}, {&quot;$inc&quot;: {&quot;age&quot;: 1}});

//搜索user为may的用户，对age进行+1处理

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd20e22931bd9b6138172f9&quot;), &quot;user&quot; : &quot;may&quot;, &quot;age&quot; : 20 }
{ &quot;_id&quot; : ObjectId(&quot;4fd20e89939e6548f004bfe8&quot;), &quot;school&quot; : &quot;tjut&quot;, &quot;user&quot; : &quot;kai&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fd20d79931bd9b6138172f8&quot;), &quot;age&quot; : 1, &quot;user&quot; : &quot;may&quot; }

我们发现只对一个记录进行了age + 1处理。没有对所有的user为may的记录进行+1处理。

&gt; db.users.update({&quot;user&quot;: &quot;may&quot;}, {&quot;$inc&quot;: {&quot;age&quot;： 1}}, false, true);

如果我们执行上述，则表明。搜索所有user为may的文档，这定这个文档的中age +1 ，如果不存在user为may的记录则不进行任何操作。

&gt; db.user.find();  //查找一下

{ &quot;_id&quot; : ObjectId(&quot;4fd20e22931bd9b6138172f9&quot;), &quot;user&quot; : &quot;may&quot;, &quot;age&quot; : 21 }
{ &quot;_id&quot; : ObjectId(&quot;4fd20e89939e6548f004bfe8&quot;), &quot;school&quot; : &quot;tjut&quot;, &quot;user&quot; : &quot;kai&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fd20d79931bd9b6138172f8&quot;), &quot;age&quot; : 2, &quot;user&quot; : &quot;may&quot; }

我们发现对所有的文档中user:may的文档进行了age +1。操作。

</div>
