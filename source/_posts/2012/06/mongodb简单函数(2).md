title: mongodb 相关简单函数和方法（插入、删除）等 （2）
tags:
- mongodb
date: "2012-06-08 06:26:34"
---

<div>如果每次我们都讲某个对象获取出来，再进行修改再进行update操作，这样效率很低，而且需要步骤太多了。mongodb提供了一种高效的修改方法：</div>
<div>//set</div>
<div>&gt; db.users.insert({&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 22}); //插入一个用户</div>
<div>&gt; db.users.find();</div>
<div>

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 20 }

//显示某个用户

如果我们给这个文档重新设定一个新的属性呢。可使用$set

&gt; db.users.update({&quot;_id&quot;: ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$set&quot;: {&quot;sex&quot;: &quot;male&quot;}});

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 20, &quot;name&quot; : &quot;may&quot;, &quot;sex&quot; : &quot;male&quot; }

我们发现给它新增加了一个sex属性。

同样，我们可以使用$set再进行修改

&gt; db.users.update({&quot;_id&quot;: ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$set&quot;: {&quot;sex&quot;: &quot;female&quot;}});

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 20, &quot;name&quot; : &quot;may&quot;, &quot;sex&quot; : &quot;female&quot; }

//$set 通常对某个文档增加一个新的属性或者修改这个文档中已有的属性

同样，可以增加就可以删除。这个属性为unset

&gt; db.users.update({&quot;_id&quot;: ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$unset&quot;: {&quot;sex&quot;: 1}});

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 20, &quot;name&quot; : &quot;may&quot; }

我们尝试多_id进行删除

&gt; db.users.update({&quot;_id&quot;: ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$unset&quot;: {&quot;_id&quot;: 1}});

Mod on _id not allowed

页面提示如上，说明这个_id是不可被删除的。重新find一下。查看结果。

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 20, &quot;name&quot; : &quot;may&quot; }

没有被删除

如果某个属性的值类型为数字，我们还可进行数值增加、删除等操作。这个使用使用$inc

&gt; db.users.update({ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$inc&quot;: {&quot;age&quot;: 10}});

给_id为 ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)的文档的age属性增加10

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 30, &quot;name&quot; : &quot;may&quot; }

我们尝试对某个不存在的属性进行数值增加

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$inc&quot;: {&quot;no&quot;: 1000}});

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;age&quot; : 30, &quot;name&quot; : &quot;may&quot;, &quot;no&quot; : 1000 }

系统会增加这个不存在的属性，并进行数值设定

我们删除这个对象的多余的属性

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$unset&quot;: {&quot;no&quot;: 1, &quot;age&quot;: 1}});

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;name&quot; : &quot;may&quot; }

push用于对某个数组属性增加新的值

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;comments&quot;: {&quot;author&quot;: &quot;may&quot;}}});

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;comments&quot; : [ { &quot;author&quot; : &quot;may&quot; } ], &quot;name&quot; : &quot;may&quot; }

//我们发comments为一个数组，数组内包含元素

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;comments&quot;: {&quot;title&quot;: &quot;just a joke!&quot;}}});

需要注意的是。push不能对系统中已有的不为array的数组进行数据增加：

&gt; db.users.update({&quot;_id&quot;: ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;name&quot;: {&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 20}}})

按照字面意思，理解为对某个文档的name进行push两个对象进去，但是由于该属性name不为数组，所以系统提示出错

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$unset&quot;: {&quot;comments&quot;: 1}});

对于push，我们可以在push的时候进行已有数值判定，如果push进入的数组内已有某个数值，那么我们就不再进行push，通常做法是获取出所有数值，在进行比对，但是mongodb提供了一个push时候的方法。可自行进行判断。

$addToSet //添加到设置

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;friend&quot;: {&quot;name&quot;: &quot;kai&quot;}}});

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$addToSet&quot;: {&quot;friend&quot;: {&quot;name&quot;: &quot;kai&quot;}}}); //如果我们用push，会有两个friend，但是这个不符合实际要求，使用addToSet，查看结果

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;friend&quot; : [ { &quot;name&quot; : &quot;kai&quot; } ], &quot;name&quot; : &quot;may&quot; }

我们尝试使用push再进行测试

&gt; db.users.update({&quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;friend&quot;: {&quot;name&quot;: &quot;kai&quot;}}});

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;), &quot;friend&quot; : [ { &quot;name&quot; : &quot;kai&quot; }, { &quot;name&quot; : &quot;kai&quot; } ], &quot;name&quot; : &quot;may&quot; }

他有了两个相同的friend。addToSet就是有push的时候增减是否重复值判断的作用。

现在的做法是，我们插入friend的是对象，有些时候，我们只需要插入某个人的名称（string）即可。使用如下方法：

&gt; db.users.update({ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$push&quot;: {&quot;friend&quot;: &quot;haiyan&quot;}});

批量插入，我们可以使用each //each是通常使用在数组上的一个方法。这个地方用法类型

&gt; db.users.update({ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$addToSet&quot;: {&quot;friend&quot;: {&quot;$each&quot;: [&quot;wang1&quot;, &quot;wang2&quot;, &quot;wang3&quot;]}}});

需要注意的是，如果使用push，不会进行each，相反，会插入一个key为$each，value为数组的对象进去

同样，我们还可以删除数组中的某个键值

使用和push相反的push

&gt; db.users.update({ &quot;_id&quot; : ObjectId(&quot;4fd0ab65bd1049ad60f809bc&quot;)}, {&quot;$pop&quot;: {&quot;friend&quot;: &quot;wang1&quot;}});

删除某个文档中friend的某个key为wang1的值，或者没key，只有value的wang1的值；

常用插入、修改、删除方法已如上

</div>