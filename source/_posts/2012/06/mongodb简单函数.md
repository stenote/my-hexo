title: mongodb 相关简单函数和方法（插入、删除）等 （1）
tags:
- mongodb
date: "2012-06-08 06:31:27"
---

<div>$ mongo //连接进数据库后
<div>
</div>
</div>
<div>&gt; show dbs;  //显示所有database</div>
<div>
</div>
<div>&gt; show foobar; //选择某个数据库</div>
<div>
</div>
<div>&gt; show collections; //显示所有collections;</div>
<div>
</div>
<div>&gt; var user = {&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 23, &quot;school&quot;: &quot;TJUT&quot;}; //创建一个user对象，设定相关属性</div>
<div>
</div>
<div>&gt; db.users.insert(user); //如果不存在则创建一个users collection，并且插入了这个user</div>
<div>
</div>
<div>&gt; db.users.insert({&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 23, &quot;school&quot;: &quot;TJUT&quot;}); //也可这么插入 </div>
<div>
</div>
<div>&gt; db.user.save(user);</div>
<div>
</div>
<div>&gt; db.users.save({&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 23, &quot;school&quot;: &quot;TJUT&quot;});</div>
<div>
</div>
<div>&gt; db.users.find(); //查找users collection 中的任意数据</div>
<div>
</div>
<div>&gt; db.users.findOne(); //查找一个数据，</div>
<div>//上述find没有增加限定条件</div>
<div>
</div>
<div>&gt; db.users.find({&quot;name&quot;: &quot;may&quot;}); //在users collection中搜索name为may的所有的文档</div>
<div>
</div>
<div>&gt; db.users.findOne({&quot;name&quot;: &quot;may&quot;}); //在users collection 中搜索一个name为may的文档</div>
<div>
</div>
<div>//某个实例 </div>
<div>&gt; db.users.find(); </div>
<div>

{ &quot;_id&quot; : ObjectId(&quot;4fcf60accb7539fbf4ecb9b1&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 1, &quot;school&quot; : &quot;TJUT&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fcf60d1cb7539fbf4ecb9b2&quot;), &quot;name&quot; : &quot;kai&quot;, &quot;age&quot; : 24, &quot;school&quot; : &quot;ysu&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fcf60e4cb7539fbf4ecb9b3&quot;), &quot;name&quot; : &quot;may&quot; }

&gt; db.users.find({&quot;name&quot;: &quot;may&quot;});

{ &quot;_id&quot; : ObjectId(&quot;4fcf60accb7539fbf4ecb9b1&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 1, &quot;school&quot; : &quot;TJUT&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fcf60e4cb7539fbf4ecb9b3&quot;), &quot;name&quot; : &quot;may&quot; }

&gt; db.users.findOne({&quot;name&quot;: &quot;may&quot;});

{
 &quot;_id&quot; : ObjectId(&quot;4fcf60accb7539fbf4ecb9b1&quot;),
 &quot;name&quot; : &quot;may&quot;,
 &quot;age&quot; : 1,
 &quot;school&quot; : &quot;TJUT&quot;
}

&gt; db.users.remove({&quot;age&quot;: 1}); //删除users中age为1的文档

&gt; db.users.remove(); //清空这个users collection 

&gt; db.users.clean(); //清空这个users collection

</div>
<div>//修改</div>
<div>
</div>
<div>首先，获取某个记录</div>
<div>&gt; var may = db.users.findOne({&quot;name&quot;: &quot;may&quot;, &quot;age&quot;: 1});</div>
<div>&gt; may;</div>
<div>

{ &quot;_id&quot; : ObjectId(&quot;4fcf6483cb7539fbf4ecb9b5&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 1 }

&gt; db.users.find();

{ &quot;_id&quot; : ObjectId(&quot;4fcf6483cb7539fbf4ecb9b5&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 1 }
{ &quot;_id&quot; : ObjectId(&quot;4fcf65e5cb7539fbf4ecb9b6&quot;), &quot;name&quot; : &quot;may&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fcf6634cb7539fbf4ecb9b7&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : &quot;二十&quot; }

</div>
<div>&gt; delete may._id;</div>
<div>&gt;  may.age = 20;</div>
<div>&gt; db.users.update({&quot;name&quot;: &quot;may&quot;}, may);</div>
<div>&gt; db.users.find();</div>
<div>

{ &quot;_id&quot; : ObjectId(&quot;4fcf6483cb7539fbf4ecb9b5&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : 20 }
{ &quot;_id&quot; : ObjectId(&quot;4fcf65e5cb7539fbf4ecb9b6&quot;), &quot;name&quot; : &quot;may&quot; }
{ &quot;_id&quot; : ObjectId(&quot;4fcf6634cb7539fbf4ecb9b7&quot;), &quot;name&quot; : &quot;may&quot;, &quot;age&quot; : &quot;二十&quot; }

这样我们会把一个name为may的文档修改age为20

</div>
<div>
</div>
<div>&gt; db.users.update({&quot;name&quot;: &quot;may&quot;}, {&quot;name&quot;: &quot;kai&quot;}); //修改</div>
