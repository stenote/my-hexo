title: SPL
tags:
- PHP
date: "2012-06-26 19:34:37"
---

<div><span>　查看SPL所有类与方法,我们可以用以下语句:　print_r(spl_classes());</span>
<div>
</div>
<span>　　我会详细说明此这10大接口的设计理念.PHP所有编程万变不离其宗.</span>
<div>
</div>
<span>　　SPL有以下接口(10大接口)</span>
<div>
</div>
<span>　　1.ArrayAccess 其功能是使类可以像PHP中的数组一样操作。有点类似于.net平台的index操作。</span>
<div>
</div>
<span>　　2.Traversable 是</span>Zend引擎<span>的内置接口，它是能让类用于foreach语句的接口，但是在PHP中并不能直接实现Traversable。只能间接地通过Iterator或IteratorAggregate接口实现。</span>
<div>
</div>
<span>　　3.IteratorAggregate(继承Traversable,是他的儿子)是除Iterator之外另一个从Traversable接口中继承而来的。其接口也很简单，只有一个函数。就是返回一个迭代器实例</span>
<div>
</div>
<span>　　4.Iterator(也是继承Traversable,是他的儿子)，SPL中大部分接口和类都是从这个接口继承而来的。</span>
<div>
</div>
<span>　　5.RecursiveIterator (继承Iterator,为Traversable的孙子)，递归迭代器，通过hasChildren()和getChildren()两个函数实现递归遍历子元素。</span>
<div>
</div>
<span>　　6.Countable 这接口就一个count()函数，返回结果集的数量。实现这个接口的类可以用count()函数查询其结果集。</span>
<div>
</div>
<span>　　7.Serializable 该接口实现序列化和反序列化的接口。在没有SPL之前，可以通过__sleep() 和__wakeup()实现相同的功能，若同时实现了Serializable 接口和_sleep() 和__wakeup()，则只有</span>Serializable接口<span>的函数启作用。</span>
<div>
</div>
<span>　　8.SplObserver Observer观察者模式的中的观察者。</span>
<div>
</div>
<span>　　9.SplSubject Observer观察者模式的中的发布者。</span>
<div>
</div>
<span>　　10.OuterIterator 它的实现者可以包含一个或多个迭代器成员，即可以通过getInnerIterator()接口函数获取内部的迭代器，也可以直接通过类本身实现的Iterator接口遍历内部的迭代器数据。这在SPL是一个非常重要的接口，SPL中很多内置的迭代器实现了这个接口。</span>
<div>
</div>
<span>　　(接口分析未完成,更新中...)</span>
<div>
</div>
<span>　　SPL中已经声明的类。</span>
<div>
</div>
<span>　　1.DirectoryIterator 这个类用来查看一个目录中的所有文件和子目录</span>
<div>
</div>
<span>　　2.FilterIterator 这是一个抽象类，它实现了OuterIterator接口。它包装一个已有的迭代器类，通过抽象方法accept()过滤掉不需要的内容，形成一个新的迭代器。</span>
<div>
</div>
<span>　　3.LimitIterator 这也是一个实现OuterIterator的类。它有点类似于SQL中的LIMIT语句。它通过包装一个已有迭代器，然后截取其中某一段数据形成一个新的迭代器。</span>
<div>
</div>
<span>　　4.RecursiveDirectoryIterator 递归查看一个目录中的所有文件的子目录。</span>
<div>
</div>
<span>　　5.SimpleXMLIterator 一个遍历XML内容的类</span>
<div>
</div>
<span>　　6.IteratorIterator 实现对迭代器的包装，这也是SPL中对OuterIterator默认实现。</span>
<div>
</div>
<span>　　7.InfiniteIterator 从字面意思就知道，这是个无限循环的迭代器，当next()到达最后时，会自动调用rewind()函数，又从头开始。</span>
<div>
</div>
<span>　　8.AppendIterator 它实现了对一系统迭代器的包装，并且可以在运行过程中添加新的迭代器。</span>
<div>
</div>
<span>　　9.SplFileObject 文件操作类，可以按行的方式遍历文件内容。同时还能获取文件的大小及其它详细信息。</span>
<div>
</div>
<span>　　10.SplFileInfo 获取文件信息类。SplFileObject 从该类继承。</span>
<div>
</div>
<span>　　错误处理是一大块:</span>
<div>
</div>
<span>　　下面来我讲讲PHP-&gt;SPL 关于错误处理方面的架构:</span>
<div>
</div>
<span>　　Exception 这是错误处理的接口,这是个值得一提,在所有程序中都会用到的父类.</span>
<div>
</div>
<span>　　笔者也想不通，为什么作者不作Exception做为一个接口呢???我得想想.</span>
<div>
</div>
<span>　　下面分为两大基类</span>
<div>
</div>
<span>　　一个是LogicException (逻辑错误处理类)</span>
<div>
</div>
<span>　　一个是RuntimeException(实时错误处理类)</span></div>
