title: MySQL字符串比较
tags:
- MySQL
date: "2012-12-31 06:08:34"
---

<div>mysql可进行字符串的比较，通常字符串的比较包括如下几种情况</div>
<div>&gt; 大于</div>
<div>=  等于，需要注意的是，在mysql中，等于使用=，于其他编程语言不同，不需要进行==判断</div>
<div>&lt;  小于</div>
<div>between A and B ，在A和B的范围中，例如 C between A and D</div>
<div>
</div>
<div>需要注意的是，mysql在字符串比较中，会进行字符集和字符串类型的比对，如果两个字符串都是二进制，则会按照二进制规则比对，如果两个字符串一个为二进制，一个不为二进制，则会都换算为二进制进行比对。</div>
<div>比对过程中还会按照字符集的collation进行比对，通常使用到collation比对是针对于字符串的字母大小写比对。</div>
<div>1、两个二进制比对</div>
<div>mysql &gt; set @a = binary 'a';</div>
<div>mysql &gt; set @b = binary 'A';</div>
<div>mysql &gt; select @a = @b;</div>
<div>结果为0</div>
<div>两个二进制进行比对，会直接使用二进制进行比对，a的ASCII码是93 A的ASCII码是65 （记得不清楚了），所以肯定@a = @b 为0</div>
<div>
</div>
<div>2、两个非二进制比对</div>
<div>mysql &gt; set @a = _latin1 'a';</div>
<div>mysql &gt; set @b = _latin1 'A';</div>
<div>mysql &gt; select @a = @b;</div>
<div>结果不一定</div>
<div>此时需要查看latin1这个character set的默认collation是否为ci结尾，如果为ci结尾，则返回1，表示相等，如果为cs或者bin结尾，则返回0，表示不相等。</div>
<div>
</div>
<div>3、一个非二进制，一个二进制比对</div>
<div>mysql &gt; set @a = binary 'a';</div>
<div>mysql &gt; set @b = _latin1 'A';</div>
<div>mysql &gt; select @a = @b;</div>
<div>结果为0</div>
<div>系统会默认将@b执行@b = convert(@b using binary)然后再进行比对，二进制比对见上面的1</div>
<div>
</div>
<div>我们在进行比对的时候，可以使用convert进行类型转换，或者使用collate进行charset的修改，需要注意的是，进行collate时，collation如果不相同，系统会报错。如下：</div>
<div>

select @a collate latin1_general_ci = @b collate latin1_general_cs;

由于比对时，使用了不同的collation，系统会报错

select @a collate latin1_general_ci = @b collate latin1_general_ci;

使用了相同的collation，系统会正常进行判断。

</div>
