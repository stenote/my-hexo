title: MySQL小技巧
tags:
- MySQL
date: "2012-12-31 06:12:32"
---

<div>$ mysql -ss foobar</div>
<div>进入skip-columns-name silent 模式，并选择foobar数据库</div>
<div>
</div>
<div>$ mysql --silent foobar</div>
<div>进入silent模式，并且选择foobar数据库</div>
<div>
</div>
<div>$ echo &quot;select now()&quot; | mysql -v</div>
<div>获取select now()的结果，并且显示比较详细的信息</div>
<div>$ echo &quot;select now()&quot; | mysql --vv</div>
<div>获取select now()的结果，并且显示比上面还详细的信息</div>
<div>
</div>
<div>mysql &gt; show full columns from limbs;</div>
<div>查看limbs的详细的列信息</div>
<div>mysql &gt; desc limbs;</div>
<div>相同效果，但是更简洁</div>
<div>mysql &gt; show full columns from limbsG;</div>
<div>查看limbs的详细列信息，同时以垂直效果显示</div>
<div>
</div>
<div>mysql &gt; T foobar.sql</div>
<div>mysql &gt; show full columns from foobar.sql;</div>
<div>T 用于开启一个tee,tee是linux中用于记录操作的一个命令，详细可看man tee</div>
<div>执行T foobar.sql后，就会记录所有mysql的输入、输出结果到指定的foobar.sql文件中</div>
<div>如果想要关闭该功能，可继续使用t来进行关闭</div>
<div>mysql &gt; t</div>
<div>也可使用</div>
<div>$ mysql --tee foobar.sql进行记录</div>
<div>
</div>
<div>mysql的变量都是@开头的变量，变量名称规则和其他变量名称规则相似</div>
<div>mysql &gt; select @user_id := id from user;</div>
<div>将user表中搜索出来的id保存到@user_id中，如果有多个结果，则保存最后一个，如果为空，则@user_id值不遍，如果本身就没值，则值为null;</div>
<div>调用变量可直接调用，如下：</div>
<div>mysql &gt; delete from user where id = @user_id;</div>
<div>变量也可通过set关键字进行赋值，如下：</div>
<div>mysql &gt; set @user = 'foobar';</div>
<div>需要注意，set赋值时字符需要增加单引号</div>
<div>mysql的变量大小写不敏感</div>
<div>
</div>
<div>一段sql语句，如下：</div>
<div>mysql &gt; set @n = 0;</div>
<div>mysql &gt; select @n := @n + 1 as rownum, thing, arms , legs from limbs;</div>
<div>结果如下</div>
<div></div>
<div>可见，@n一直在增长，也就是说，每次select 后，都会n值进行增加</div>
<div>
</div>
<div>mysql &gt; status;</div>
<div>查看当前mysql状态</div>
<div>mysql &gt; show status;</div>
<div>查看mysql的所有信息</div>
<div>mysql &gt; show status like 'uptime';</div>
<div>查看mysql的uptime的信息</div>
<div>
</div>
<div>mysql中单引号，双引号通常用于字符串的包裹，` 通常用于包裹系统table名称、columns名称、database名称</div>
<div>
</div>
<div>mysql &gt; show engines;</div>
<div>查看所有当前mysql系统支持的引擎</div>
<div>
</div>
<div>upper lower两个函数是用于返回大写和小写的字符串的</div>
<div>针对binaray不支持大小写转换。</div>
<div>
</div>
<div>快速创建一个特定字符集的字符串可使用</div>
<div>set @a = _latin1 'a';这种方式,latin1为字符集，'a'为对应类型</div>
