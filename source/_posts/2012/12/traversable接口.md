title: Traversable接口
tags:
- PHP
date: "2012-12-08 06:33:04"
---

<div>Traversable是一个特定的PHP接口，它不可被单独的实现。</div>
<div>Traversable提供了foreach的入口，相当于Countable提供了count()方法的入口。</div>
<div>
</div>
<div>interface Traversable {</div>
<div>    public function foreach() {</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>它通常是由系统中的Iterator或者IteratorAggregate来代替之。</div>
<div>
</div>
<div>var_dump(new ArrayObject(array()) instaceof Traversable);</div>
<div>//bool(true)</div>
<div>
</div>
<div>不可直接进行一个基类实现这个接口</div>
<div>
</div>
<div>class Foobar implements Traversable {</div>
<div>    public function foreach() {</div>
<div>    }</div>
<div>}</div>
<div>提示错误</div>
<div>通常我们直接去实现Iterator即可。</div>
<div>以下为Iterator的简单摘要</div>
<div>
<div><span>**Iterator** </span><span><span>extends</span> **Traversable** </span>{</div>
<div>/* 方法 */</div>
<div><span>    abstract</span> <span>public</span> <span>mixed</span> <span>[current](http://www.php.net/manual/zh/iterator.current.php)</span> ( <span>void</span> )</div>
<div><span>    abstract</span> <span>public</span> <span>scalar</span> <span>[key](http://www.php.net/manual/zh/iterator.key.php)</span> ( <span>void</span> )</div>
<div><span>    abstract</span> <span>public</span> <span>void</span> <span>[next](http://www.php.net/manual/zh/iterator.next.php)</span> ( <span>void</span> )</div>
<div><span>    abstract</span> <span>public</span> <span>void</span> <span>[rewind](http://www.php.net/manual/zh/iterator.rewind.php)</span> ( <span>void</span> )</div>
<div><span>    abstract</span> <span>public</span> <span>boolean</span> <span>[valid](http://www.php.net/manual/zh/iterator.valid.php)</span> ( <span>void</span> )</div>
<span>}</span></div>
