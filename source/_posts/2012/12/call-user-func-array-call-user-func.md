title: call_user_func_array call_user_func
tags:
- PHP
date: "2012-12-24 04:59:13"
---

<div>有些时候，调用某个函数并传入长度不确定到函数到时候，就需要使用到</div>
<div>call_user_func_array和call_user_func这两个函数。</div>
<div>call_user_func_array(string | array, array)</div>
<div>call_user_func(string | array, mix[, mix..., mix])</div>
<div>
</div>
<div>例如</div>
<div>function say($name, $age) {</div>
<div>    echo $name. '=&gt;' . $age;</div>
<div>}</div>
<div>
</div>
<div>call_user_func_array('say', array('name', 'age'));</div>
<div>call_user_func('say', 'name', 'age');</div>
<div>
</div>
<div>执行后页面显示name=&gt;age</div>
<div>
</div>
<div>class User {</div>
<div>    function say($name, $age) {</div>
<div>        echo $name. '=&gt;'. $age;</div>
<div>    }</div>
<div>    </div>
<div>     static function haha($name) {</div>
<div>        echo 'haha '. $name;</div>
<div>    } </div>
<div>}</div>
<div>
</div>
<div>$user = new User();</div>
<div>call_user_func_array(array($user, 'say'), array('name', 'age'));</div>
<div>//name=&gt;age</div>
<div>call_user_func_array(array('User', 'haha'), array('name'));</div>
<div>//haha name</div>
<div>如下两个函数也可。</div>
<div>call_user_func(array($user, 'say'), 'name', 'age');</div>
<div>call_user_func(array('User', 'say'), 'name');</div>
<div>
</div>
<div>call_user_func和call_user_func_array是为了方便传值。如果调用某个对象到方法时，不存在这个方法，那么，可会自动去调用__call方法。</div>
<div>
</div>
<div>call_user_func call_user_func_array 的第一个参数如果为数组，还可使用如下方法：</div>
<div>call_user_func(array(Auth::instance(), 'hash'), 'password);</div>
<div>call_user_func_array(array(Auth::instance(), 'hash'), array('password'));</div>
<div>
</div>
<div>表示先执行Auth的静态方法instance,再调用对象或者字符串对应类的hash方法，并传入'password'为参数；</div>
<div>
</div>
<div>
</div>
