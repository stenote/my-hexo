title: postfix
tags:
- Linux
date: "2012-12-03 06:27:56"
---

<div>postfix是一个mta（mail transfer agent) 服务</div>
<div>
</div>
<div><span style="font-size: medium;color: #888888">简单安装：</span></div>
<div>ubuntu下安装使用</div>
<div>sudo apt-get install postfix</div>
<div>mail安装使用</div>
<div>sudo apt-get install mailutils</div>
<div>
</div>
<div><span style="color: #888888;font-size: medium">简单配置：</span></div>
<div>postfix安装后，可通过postconf命令进行postfix的配置修改</div>
<div>postfix的配置文件目录为/etc/postfix/mail.cf</div>
<div>
</div>
<div>postconf -d  //查看默认配置 default</div>
<div>postconf -n  //查看当前配置 now</div>
<div>postconf -e  //设定某个配置</div>
<div>
</div>
<div>
</div>
<div><span style="color: #888888;font-size: medium">邮件发送：</span></div>
<div>mail -t rui.ma@geneegroup.com -s hello world</div>
<div>Cc:</div>
<div>hello</div>
<div>
</div>
<div>会发送一个标题为hello world 内容为hello的邮件到rui.ma@geneegroup.com</div>
<div>
</div>
<div>echo 'Foobar' | mail -t rui.ma@geneegroup.com -s hello world</div>
<div>发送一个内容为Foobar，标题为hello world的邮件到rui.ma@geneegroup.com</div>
<div>
</div>
<div>文件mail.txt内容为foobar</div>
<div>mail -t rui.ma@geneegroup.com -s hello world &lt; mail.txt</div>
<div>发送内容为foobar，标题为hello world的邮件到rui.ma@geneegroup.com</div>
<div>
</div>
<div>管道命令不再熬述</div>
<div>
</div>
<div>
</div>
<div>
</div>
