title: php之memcache
tags:
- PHP
date: "2012-12-13 08:05:52"
---

<div>Memecache，php常用的缓存服务</div>
<div>
</div>
<div>服务端口为11211</div>
<div>
</div>
<div>安装方法：</div>
<div>$ sudo apt-get install php5-memcache;</div>
<div>$ sudo apt-get install memcached;</div>
<div>会自动安装php5所需要的memcache服务</div>
<div>
</div>
<div>Memcache常用函数如下：</div>
<div>
</div>
<div>
<div><span>**Memcache** </span>{</div>
<div><span>bool</span> <span>[add](http://php.net/manual/zh/memcache.add.php)</span> ( <span><span>string</span> `$key`</span> , <span><span>[mixed](http://php.net/manual/zh/language.pseudo-types.php#language.types.mixed)</span> `$var`</span> [, <span><span>int</span> `$flag`</span> [, <span><span>int</span> `$expire`</span> ]] )</div>
<div><span>bool</span> <span>[addServer](http://php.net/manual/zh/memcache.addserver.php)</span> ( <span><span>string</span> `$host`</span> [, <span><span>int</span> `$port`<span> = 11211</span></span> [, <span><span>bool</span> `$persistent`</span> [, <span><span>int</span> `$weight`</span> [, <span><span>int</span> `$timeout`</span> [, <span><span>int</span> `$retry_interval`</span> [, <span><span>bool</span> `$status`</span> [, <span><span>[callback](http://php.net/manual/zh/language.pseudo-types.php#language.types.callback)</span> `$failure_callback`</span> [,<span><span>int</span> `$timeoutms`</span> ]]]]]]]] )</div>
<div><span>bool</span> <span>[close](http://php.net/manual/zh/memcache.close.php)</span> ( <span>void</span> )</div>
<div><span>bool</span> <span>[connect](http://php.net/manual/zh/memcache.connect.php)</span> ( <span><span>string</span> `$host`</span> [, <span><span>int</span> `$port`</span> [, <span><span>int</span> `$timeout`</span> ]] )</div>
<div><span>int</span> <span>[decrement](http://php.net/manual/zh/memcache.decrement.php)</span> ( <span><span>string</span> `$key`</span> [, <span><span>int</span> `$value`<span> = 1</span></span> ] )</div>
<div><span>bool</span> <span>[delete](http://php.net/manual/zh/memcache.delete.php)</span> ( <span><span>string</span> `$key`</span> [, <span><span>int</span> `$timeout`<span> = 0</span></span> ] )</div>
<div><span>bool</span> <span>[flush](http://php.net/manual/zh/memcache.flush.php)</span> ( <span>void</span> )</div>
<div><span>string</span> <span>[get](http://php.net/manual/zh/memcache.get.php)</span> ( <span><span>string</span> `$key`</span> [, <span><span>int</span> `&amp;$flags`</span> ] )</div>
<div><span>array</span> <span>[getExtendedStats](http://php.net/manual/zh/memcache.getextendedstats.php)</span> ([ <span><span>string</span> `$type`</span> [, <span><span>int</span> `$slabid`</span> [, <span><span>int</span> `$limit`<span> = 100</span></span> ]]] )</div>
<div><span>int</span> <span>[getServerStatus](http://php.net/manual/zh/memcache.getserverstatus.php)</span> ( <span><span>string</span> `$host`</span> [, <span><span>int</span> `$port`<span> = 11211</span></span> ] )</div>
<div><span>array</span> <span>[getStats](http://php.net/manual/zh/memcache.getstats.php)</span> ([ <span><span>string</span> `$type`</span> [, <span><span>int</span> `$slabid`</span> [, <span><span>int</span> `$limit`<span> = 100</span></span> ]]] )</div>
<div><span>string</span> <span>[getVersion](http://php.net/manual/zh/memcache.getversion.php)</span> ( <span>void</span> )</div>
<div><span>int</span> <span>[increment](http://php.net/manual/zh/memcache.increment.php)</span> ( <span><span>string</span> `$key`</span> [, <span><span>int</span> `$value`<span> = 1</span></span> ] )</div>
<div><span>mixed</span> <span>[pconnect](http://php.net/manual/zh/memcache.pconnect.php)</span> ( <span><span>string</span> `$host`</span> [, <span><span>int</span> `$port`</span> [, <span><span>int</span> `$timeout`</span> ]] )</div>
<div><span>bool</span> <span>[replace](http://php.net/manual/zh/memcache.replace.php)</span> ( <span><span>string</span> `$key`</span> , <span><span>[mixed](http://php.net/manual/zh/language.pseudo-types.php#language.types.mixed)</span> `$var`</span> [, <span><span>int</span> `$flag`</span> [, <span><span>int</span> `$expire`</span> ]] )</div>
<div><span>bool</span> <span>[set](http://php.net/manual/zh/memcache.set.php)</span> ( <span><span>string</span> `$key`</span> , <span><span>[mixed](http://php.net/manual/zh/language.pseudo-types.php#language.types.mixed)</span> `$var`</span> [, <span><span>int</span> `$flag`</span> [, <span><span>int</span> `$expire`</span> ]] )</div>
<div><span>bool</span> <span>[setCompressThreshold](http://php.net/manual/zh/memcache.setcompressthreshold.php)</span> ( <span><span>int</span> `$threshold`</span> [, <span><span>float</span> `$min_savings`</span> ] )</div>
<div><span>bool</span> <span>[setServerParams](http://php.net/manual/zh/memcache.setserverparams.php)</span> ( <span><span>string</span> `$host`</span> [, <span><span>int</span> `$port`<span> = 11211</span></span> [, <span><span>int</span> `$timeout`</span> [, <span><span>int</span> `$retry_interval`<span> = false</span></span> [, <span><span>bool</span> `$status`</span> [, <span><span>[callback](http://php.net/manual/zh/language.pseudo-types.php#language.types.callback)</span> `$failure_callback`</span> ]]]]] )</div>
<span>}</span></div>
<div>
</div>
<div>其中add和set方法完全一样</div>
<div>如下</div>
<div><span> </span><span><span>string</span> `$key`</span><span> , </span><span><span>[mixed](http://php.net/manual/zh/language.pseudo-types.php#language.types.mixed)</span> `$var`</span><span> [, </span><span><span>int</span> `$flag`</span><span> [, </span><span><span>int</span> `$expire`</span><span> ]] )</span></div>
<div>key值，value值， 是否压缩，过期时间设定</div>
<div>
</div>
<div>get方法如下：</div>
<div>
<div><span>string</span> <span>**Memcache::get**</span> ( <span><span>string</span> `$key`</span> [, <span><span>int</span> `&amp;$flags`</span> ] )</div>
<div><span>array</span> <span>**Memcache::get**</span> ( <span><span>array</span> `$keys`</span> [, <span><span>array</span> `&amp;$flags`</span> ] )</div>
<div>通常使用第一个即可</div>
<div>key值，是否压缩</div>
<div>
</div>
<div>connect 连接函数</div>
<div><span>bool</span><span> </span><span>**Memcache::connect**</span><span> ( </span><span><span>string</span> `$host`</span><span> [, </span><span><span>int</span> `$port`</span><span> [, </span><span><span>int</span> `$timeout`</span><span> ]] )</span></div>
<div>host地址，port端口，连接时间</div>
<div>如果连接时间要相对比较长，可以使用</div>
<div>pconnect方法</div>
<div>
</div>
<div><span>[mixed](http://php.net/manual/zh/language.pseudo-types.php#language.types.mixed)</span><span> </span><span>**Memcache::pconnect**</span><span> ( </span><span><span>string</span> `$host`</span><span> [, </span><span><span>int</span> `$port`</span><span> [, </span><span><span>int</span> `$timeout`</span><span> ]] )</span></div>
<div>
</div>
<div>
</div>
<div>真心不想写笔记了，累了。脖子疼啊。唉。</div>
<div>memcache使用方法如下：</div>
<div>
</div>
<div>$mem = new Memcache(); //创建memcache对象</div>
<div>$mem-&gt;connect('localhost', 11211); //连接memcache</div>
<div>$mem-&gt;addServer('192.168.1.55', 11211); //增加另外一个memcache服务</div>
<div>
</div>
<div>$mem-&gt;set('key1', 'value1'); //设定key1的值为value1</div>
<div>$mem-&gt;set('key2', 'value2'); //设定key2的值为value2</div>
<div>
</div>
<div>echo $mem-&gt;get('key1'); //获取key1的值</div>
<div>print_r($mem-&gt;get(array('key1', 'key2')); //获取key1 key2的值</div>
<div>
</div>
<div>$mem-&gt;delete('key1'); //删除key1的值</div>
<div>
</div>
<div>$mem-&gt;flush(); //清空所有</div>
<div>$mem-&gt;set('int', 1); //设定int的值为1</div>
<div>$mem-&gt;increment('int', 2); //为int的值加1</div>
<div>$mem-&gt;decrement('int', 1); //为int的值减1</div>
<div>
</div>
<div>
</div>
<div>$mem-&gt;close();</div>
<div>
</div>
</div>
<div>脖子好疼，睡觉睡觉</div>
