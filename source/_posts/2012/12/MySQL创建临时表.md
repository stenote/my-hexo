title: Mysql创建临时表
tags:
- MySQL
date: "2012-12-29 23:55:51"
---

<div>create <span style="color: #ff9900">**temporary**</span> table temp_table </div>
<div>(</div>
<div>id int(2) not null auto_increment,</div>
<div>primary key (id)</div>
<div>);</div>
<div>通过在创建表的同时设定表的类型为temporary即可创建临时表。</div>
<div>临时表的特点：</div>
<div>1、临时性，创建的临时表在当前数据连接断开后会自动drop</div>
<div>2、单一性，该表只在当前的数据连接中可用，如果重新创建一个连接，是无法找到该临时表。</div>
<div>3、隐藏性，创建后的临时表不能使用show tables查找到。</div>
<div>4、覆盖性，可以创建一个名称和现有表名称相同的临时表，但是现有的表则不可使用。但是show tables的时候可以显示现有表。</div>
<div>5、简单性、不可执行特殊的SQL语句，包括一下几个方面：</div>
<div>    I. 只可对表名调用一次SELECT * FROM temp_table, temp_table AS t2;是错误的</div>
<div>    II.使用引擎有限，只可使用若干引擎 <span style="color: #ff9900">//TODO</span></div>
<div>    III.不可进行rename的操作。</div>
<div>
</div>
<div>通常创建临时表使用如下方法：</div>
<div>mysql &gt; create temporary table temp_user select * from user;</div>
<div>删除临时表使用</div>
<div>mysql &gt; drop temporary table if exists temp_user;</div>
<div>需要注意的是，通常temporary会在断开数据库连接后自动删除，但是为了防止某次操作后数据库连接是长连接而导致对临时表的操作没有对应到实际表中，需要主动在临时表操作结束后将临时表删除。</div>
