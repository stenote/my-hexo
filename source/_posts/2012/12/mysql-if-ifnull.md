title: MySQL if ifnull
tags:
- MySQL
date: "2012-12-27 06:54:23"
---

<div>正常执行select 操作如下：</div>
<div></div>
<div>使用if进行null判断后的操作如下：</div>
<div></div>
<div>使用ifnull判断后的操作如下：</div>
<div></div>
<div>正常进行select，会显示真实结果</div>
<div>使用if会进行判断，返回特定的结果，第一个参数是判断条件，第二个参数为条件为true的返回结果，第三个参数为条件为false的返回结果</div>
<div>可以理解为</div>
<div>参数一 ? 参数二 : 参数三</div>
<div>
</div>
<div>使用ifnull进行判断则直接进行is null判断，如果第一个参数为null，则返回第二个参数，如果第一个参数不为null，则返回第一个参数</div>
<div>可以理解为</div>
<div>参数一 is null ? 参数二 ： 参数一</div>
