title: MySQL数据库导入、导出等相关简易命令
tags:
- MySQL
date: "2012-12-26 04:27:55"
---

<div>mysql 数据库可随时进行数据库内的数据的导出导入，这样便于对mysql数据库进行转移和备份操作。</div>
<div>导入：</div>
<div style="margin-left: 30px">导入通常有如下5种方法：</div>
<div style="margin-left: 60px">1、mysql命令登录数据库后，选择需要导入的数据库，使用source命令对sql语句进行导入</div>
<div style="margin-left: 90px">mysql &gt; use foobar;</div>
<div style="margin-left: 90px">mysql &gt; source foobar.sql;</div>
<div style="margin-left: 90px"></div>
<div style="margin-left: 60px">2、在命令行中，使用mysql链接命令，后缀数据库和sql语句文件</div>
<div style="margin-left: 90px">$ mysql -hlocalhost -umay -p foobar &lt; foobar.sql;</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">3、先获取sql语句文件内容，然后管道符连接至mysql数据库</div>
<div style="margin-left: 90px">$ cat foobar.sql | mysql -hlocalhost -umay -p foobar;</div>
<div style="margin-left: 90px"></div>
<div style="margin-left: 60px">4、类似3，我们可以推测，使用如下命令导入或执行相关sql命令</div>
<div style="margin-left: 90px">$ echo 'drop database foobar' | mysql -hlocalhost -umay -p;</div>
<div style="margin-left: 90px"></div>
<div style="margin-left: 60px">5、mysql本身有-e参数，可使用-e 参数执行相关sql语句</div>
<div style="margin-left: 90px">$ mysql -hlocalhost -umay -p -e 'drop database foobar';</div>
<div style="margin-left: 90px"></div>
<div style="margin-left: 60px">通常1、2、3 命令适合大SQL文件导入，如果是单一SQL语句，建议使用4 、 5种方法</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px"></div>
<div>导出：</div>
<div style="margin-left: 30px">导出通常用一种方法，如下： （我所知道的方法只有如下一种，如果有新的方法，需要进行补充）</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">$ mysqldump -hlocalhost -umay -p foobar &gt; foobar.sql;</div>
<div style="margin-left: 60px">这样就可以将foobar数据库导出到foobar.sql文件中了，这个导出是将数据库内的所有的信息都进行了导出，包含了数据库内的数据。如果只需要导出数据库的结构，可以使用如下命令：</div>
<div style="margin-left: 60px">$ mysqldump -hlocalhost -umay -p -d foobar &gt; foobar2.sql;</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">-d 选项用于导出结构，不会导出数据。</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 30px"></div>
<div>
</div>
<div>mysql和mysqldump 命令中最后一个是dbname 一个是tablename。如果不存在tablename，则对dbname进行操作。</div>
<div>如果不存在dbname，mysqldump不会执行。</div>
<div>如果不存在dbname，mysql 可进行连接，但是不可进行导入SQL。</div>
<div>
</div>
<div>导出所有的数据库</div>
<div>$ mysqldump -hlocalhost -umay -p --all-databses &gt; all.sql;</div>
