title: MySQL字符串匹配
tags:
- MySQL
date: "2012-12-31 07:05:45"
---

<div>mysql字符串匹配使用like和rlike进行匹配</div>
<div>like是正常的SQL语句匹配，通常如下：</div>
<div>mysql &gt; select name from user where name like 'foo%'</div>
<div>mysql &gt; select name from user where name like 'foo_';</div>
<div>mysql &gt; select name from user where name not like 'foo%';</div>
<div>mysql &gt; select name from user where name not like 'foo_';</div>
<div>%表示匹配多个字符</div>
<div>_表示匹配一个字符</div>
<div>
</div>
<div>
</div>
<div>mysql &gt; select name from user where name rlike 'foobar.*';</div>
<div>mysql &gt; select name from user where name not rlike 'foobar.*';</div>
<div>
</div>
<div>
</div>
<div>rlike后即跟正则表达式进行匹配</div>
<div>或者如下：</div>
<div>mysql &gt; select name from user where name regexp 'foobar.*';</div>
<div>mysql &gt; select name from user where name not regexp 'foobar.*';</div>
<div>
</div>
<div>
</div>
