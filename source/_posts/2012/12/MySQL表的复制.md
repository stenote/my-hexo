title: MySQL表的复制
tags:
- MySQL
date: "2012-12-28 04:36:20"
---

<div>我们以mysql数据库本身的user表为例进行操作：</div>
<div>
</div>
<div>在foobar数据库中创建一个和mysql数据库中user表结构相同的名user_from_mysql的表</div>
<div>mysql&gt; create table foobar.user_from_mysql like mysql.user;</div>
<div>可创建一个结构完全相同但是无数据的表，不过需要提前创建foobar数据库</div>
<div>比较标准的写法是</div>
<div>mysql&gt; CREATE TABLE `foobar`.`user_from_mysql` LIKE `mysql`.`user`;</div>
<div>
</div>
<div><span style="font-size: small;color: #ff0000">_注意：使用like进行表结构复制的时候，会保存表结构的KEY属性_</span></div>
<div>
</div>
<div>创建表结构后，我们就可以对表内的数据进行复制了，如下：</div>
<div>mysql&gt; insert into foobar.user_from_mysql select * from mysql.user;</div>
<div>这样可以将mysql.user表中的所有数据都复制到foobar.user_from_mysql表中</div>
<div>
</div>
<div>同样，如果有另外一个表，只要满足表结构一定，我们也可进行如下操作：</div>
<div>mysql&gt; insert into foobar.other_user select User, Host from mysql.user where Host != 'localhost';</div>
<div>需要满足foobar.other_user表中只具有和User, Host 列</div>
<div>
</div>
<div>加入other_user表中User对应的列为user，Host对应的列为from，我们可以使用</div>
<div>mysql&gt; insert into foobar.other_user select User as user, Host as `from` from mysql.user;</div>
<div>需要注意的是，as后出现系统保留字，需要进行``转义</div>
<div>
</div>
<div>在插入时我们可以选择插入的结果，同样，在创建时我们也可选择需要创建的列</div>
<div>mysql &gt; create table user select User as user from mysql.user where 0;</div>
<div>我们创建了一个和mysql.user中User列结构完全相同的user列在user表中</div>
<div>where 0 的意思是不检索出数据</div>
<div>
</div>
<div>我们还可以在创建表时自主增加相关列，例如：</div>
<div>mysql &gt; create table user (id int not null auto_increment, primary key （id） ) select User as user from mysql.user where 0;</div>
<div>我们创建了一个主键为id，并自动增长，包含和mysql.user相同的User列的表user</div>
<div>
</div>
<div>如果我们想创建一个table，同时具有两个表中列结构，我们可以在在select后进行left join等join操作，如下：</div>
<div>mysql &gt; create table foobar (id int not null auto_increment, primary key (id)) select db.Db as `db` , user.Host as `host` from mysql.db left join mysql.user on mysql.db.User = mysql.user.User;</div>
<div>
</div>
