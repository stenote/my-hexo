title: MySQL存储引擎
tags:
- MySQL
date: "2012-12-30 00:53:03"
---

<div>mysql支持很多的存储引擎，针对不同的表我们需要设定不同的存储引擎，并且我们可以设定mysql默认的存储引擎。</div>
<div>
</div>
<div>查看系统支持的存储引擎可以使用如下命令：</div>
<div>mysql &gt; show engines;</div>
<div>
</div>
<div>查看某个表的引擎通常有如下三种方法：</div>
<div>1、通过查看表的创建SQL语句来判断表所使用的引擎类型</div>
<div>mysql &gt; show create table user;</div>
<div>
</div>
<div>

CREATE TABLE `user` (
 `User` char(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
 `Host` char(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1

2、查看表状态

mysql &gt; show table status G;

该命令会列出当前数据库内的所有的表状态，建议进行G，垂直显示

*************************** 1\. row ***************************
 Name: user
 Engine: MyISAM
 Version: 10
 Row_format: Fixed
 Rows: 6
 Avg_row_length: 229
 Data_length: 1374
Max_data_length: 64457769666740223
 Index_length: 1024
 Data_free: 0
 Auto_increment: NULL
 Create_time: 2012-12-29 15:54:58
 Update_time: 2012-12-29 15:54:58
 Check_time: NULL
 Collation: latin1_swedish_ci
 Checksum: NULL
 Create_options: 
 Comment:

3、查看information_schema.tables中的信息

select engine from information.tables where table_schema = 'foobar' and table_name = 'user';

查看foobar.user的engine;

修改某个表的引擎

mysql &gt; alter table user engine = myisam;

</div>
