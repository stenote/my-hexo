title: MySQL配置设定
tags:
- MySQL
date: "2012-12-26 04:37:54"
---

<div>mysql的配置通常对于不同操作系统有不同的位置进行存储</div>
<div>linux下如下：</div>
<div style="margin-left: 30px">/etc/mysql/my.cnf</div>
<div style="margin-left: 30px">~/.my.cnf</div>
<div style="margin-left: 30px"></div>
<div>mysql的配置和git使用类似配置，如下</div>
<div></div>
<div>
</div>
<div>[client]</div>
<div>#client为大的类型</div>
<div>user = root  #user值设定为root</div>
<div>password = RCAHU4FN #password设定为RCAHU4FN</div>
<div>[mysql]</div>
<div>auto-rehash #用于开启mysql的自动补全</div>
<div>
</div>
<div>一下为一些常用配置</div>
<div>skip-columns-name 对搜索结果过滤头部</div>
<div>auto-rehash 自动补全</div>
<div>no-auto-rehash 关闭自动补全</div>
<div>silent 不会显示边线</div>
<div>
</div>
<div>可以使用mysql -ss 去掉头部和边线，让页面显示清爽</div>
<div>
</div>
