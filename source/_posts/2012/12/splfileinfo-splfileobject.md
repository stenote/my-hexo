title: SplFileInfo SplFileObject
tags:
- PHP
date: "2012-12-08 06:37:30"
---

<div>SplFileInfo主要用于获取文件外部信息，例如ctime basename等信息</div>
<div>SplFIleObject通常是对文件进行读取，写入等操作</div>
<div>先看看SplFileInfo</div>
<div>[http://cn2.php.net/manual/en/class.splfileinfo.php](http://cn2.php.net/manual/en/class.splfileinfo.php)</div>
<div>
</div>
<div>
<div><span>**SplFileInfo** </span>{</div>
<div>/* Methods */</div>
<div><span>public</span> <span>[__construct](http://cn2.php.net/manual/en/splfileinfo.construct.php)</span> ( <span><span>string</span> `$file_name`</span> )  //初始化，需传入文件名称或者完整路径文件名称</div>
<div><span>public</span> <span>int</span> <span>[getATime](http://cn2.php.net/manual/en/splfileinfo.getatime.php)</span> ( <span>void</span> ) //获取文件访问时间</div>
<div><span>public</span> <span>string</span> <span>[getBasename](http://cn2.php.net/manual/en/splfileinfo.getbasename.php)</span> ([ <span><span>string</span> `$suffix`</span> ] ) //获取文件名称</div>
<div><span>public</span> <span>int</span> <span>[getCTime](http://cn2.php.net/manual/en/splfileinfo.getctime.php)</span> ( <span>void</span> ) //获取文件创建时间</div>
<div><span>public</span> <span>string</span> <span>[getExtension](http://cn2.php.net/manual/en/splfileinfo.getextension.php)</span> ( <span>void</span> )  //获取文件后缀</div>
<div><span>public</span> <span>SplFileInfo</span> <span>[getFileInfo](http://cn2.php.net/manual/en/splfileinfo.getfileinfo.php)</span> ([ <span><span>string</span> `$class_name`</span> ] )  //获取文件对应的SplFileObject对象</div>
<div><span>public</span> <span>string</span> <span>[getFilename](http://cn2.php.net/manual/en/splfileinfo.getfilename.php)</span> ( <span>void</span> ) //获取文件名称</div>
<div><span>public</span> <span>int</span> <span>[getGroup](http://cn2.php.net/manual/en/splfileinfo.getgroup.php)</span> ( <span>void</span> ) //获取文件所属的Group id</div>
<div><span>public</span> <span>int</span> <span>[getInode](http://cn2.php.net/manual/en/splfileinfo.getinode.php)</span> ( <span>void</span> ) //获取文件<span>inode</span></div>
<div><span>public</span> <span>string</span> <span>[getLinkTarget](http://cn2.php.net/manual/en/splfileinfo.getlinktarget.php)</span> ( <span>void</span> ) //获取link文件目标路径</div>
<div><span>public</span> <span>int</span> <span>[getMTime](http://cn2.php.net/manual/en/splfileinfo.getmtime.php)</span> ( <span>void</span> ) //获取文件修改iashijian</div>
<div><span>public</span> <span>int</span> <span>[getOwner](http://cn2.php.net/manual/en/splfileinfo.getowner.php)</span> ( <span>void</span> ) //获取文件拥有者id</div>
<div><span>public</span> <span>string</span> <span>[getPath](http://cn2.php.net/manual/en/splfileinfo.getpath.php)</span> ( <span>void</span> ) //获取文件路径</div>
<div><span>public</span> <span>SplFileInfo</span> <span>[getPathInfo](http://cn2.php.net/manual/en/splfileinfo.getpathinfo.php)</span> ([ <span><span>string</span> `$class_name`</span> ] ) //返回文件路径对应的SplFileObject</div>
<div><span>public</span> <span>string</span> <span>[getPathname](http://cn2.php.net/manual/en/splfileinfo.getpathname.php)</span> ( <span>void</span> ) //获取文件相对路径</div>
<div><span>public</span> <span>int</span> <span>[getPerms](http://cn2.php.net/manual/en/splfileinfo.getperms.php)</span> ( <span>void</span> ) //获取文件权限</div>
<div><span>public</span> <span>string</span> <span>[getRealPath](http://cn2.php.net/manual/en/splfileinfo.getrealpath.php)</span> ( <span>void</span> ) //获取文件真实路径</div>
<div><span>public</span> <span>int</span> <span>[getSize](http://cn2.php.net/manual/en/splfileinfo.getsize.php)</span> ( <span>void</span> ) //获取文件大小</div>
<div><span>public</span> <span>string</span> <span>[getType](http://cn2.php.net/manual/en/splfileinfo.gettype.php)</span> ( <span>void</span> ) //获取文件类型</div>
<div><span>public</span> <span>bool</span> <span>[isDir](http://cn2.php.net/manual/en/splfileinfo.isdir.php)</span> ( <span>void</span> ) //判断是否为目录</div>
<div><span>public</span> <span>bool</span> <span>[isExecutable](http://cn2.php.net/manual/en/splfileinfo.isexecutable.php)</span> ( <span>void</span> ) //判断是否可执行</div>
<div><span>public</span> <span>bool</span> <span>[isFile](http://cn2.php.net/manual/en/splfileinfo.isfile.php)</span> ( <span>void</span> ) //判断是否为文件</div>
<div><span>public</span> <span>bool</span> <span>[isLink](http://cn2.php.net/manual/en/splfileinfo.islink.php)</span> ( <span>void</span> ) //判断是否为link</div>
<div><span>public</span> <span>bool</span> <span>[isReadable](http://cn2.php.net/manual/en/splfileinfo.isreadable.php)</span> ( <span>void</span> ) //判断是否可读</div>
<div><span>public</span> <span>bool</span> <span>[isWritable](http://cn2.php.net/manual/en/splfileinfo.iswritable.php)</span> ( <span>void</span> ) //判断是否可写</div>
<div><span>public</span> <span>SplFileObject</span> <span>[openFile](http://cn2.php.net/manual/en/splfileinfo.openfile.php)</span> ([ <span><span>string</span> `$open_mode`<span> = r</span></span> [, <span><span>bool</span> `$use_include_path`<span> = false</span></span> [, <span><span>resource</span> `$context`<span> = **`NULL`**</span></span> ]]] ) //返回文件对应的SplFileOjbect</div>
<div><span>public</span> <span>void</span> <span>[setFileClass](http://cn2.php.net/manual/en/splfileinfo.setfileclass.php)</span> ([ <span><span>string</span> `$class_name`</span> ] ) //不知道</div>
<div><span>public</span> <span>void</span> <span>[setInfoClass](http://cn2.php.net/manual/en/splfileinfo.setinfoclass.php)</span> ([ <span><span>string</span> `$class_name`</span> ] ) //不知道</div>
<div><span>public</span> <span>void</span> <span>[__toString](http://cn2.php.net/manual/en/splfileinfo.tostring.php)</span> ( <span>void</span> ) //显示文件名称</div>
<span>}</span></div>
<div>
</div>
<div>
</div>
<div>**SplFileObject**</div>
<div>
<div>/* Constants */</div>
<div><span>const</span> <span>integer</span> <var>[<var>DROP_NEW_LINE</var>](http://cn2.php.net/manual/en/class.splfileobject.php#splfileobject.constants.drop-new-line)</var> <span>= 1</span> ;</div>
<div><span>const</span> <span>integer</span> <var>[<var>READ_AHEAD</var>](http://cn2.php.net/manual/en/class.splfileobject.php#splfileobject.constants.read-ahead)</var> <span>= 2</span> ;</div>
<div><span>const</span> <span>integer</span> <var>[<var>SKIP_EMPTY</var>](http://cn2.php.net/manual/en/class.splfileobject.php#splfileobject.constants.skip-empty)</var> <span>= 4</span> ;</div>
<div><span>const</span> <span>integer</span> <var>[<var>READ_CSV</var>](http://cn2.php.net/manual/en/class.splfileobject.php#splfileobject.constants.read-csv)</var> <span>= 8</span> ;</div>
<div>/* Methods */</div>
<div><span>public</span> <span>[__construct](http://cn2.php.net/manual/en/splfileobject.construct.php)</span> ( <span><span>string</span> `$filename`</span> [, <span><span>string</span> `$open_mode`<span> = &quot;r&quot;</span></span> [, <span><span>bool</span> `$use_include_path`<span> = false</span></span> [, <span><span>resource</span> `$context`</span> ]]] )</div>
<div><span>public</span> <span>string|array</span> <span>[current](http://cn2.php.net/manual/en/splfileobject.current.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>bool</span> <span>[eof](http://cn2.php.net/manual/en/splfileobject.eof.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>bool</span> <span>[fflush](http://cn2.php.net/manual/en/splfileobject.fflush.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>string</span> <span>[fgetc](http://cn2.php.net/manual/en/splfileobject.fgetc.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>array</span> <span>[fgetcsv](http://cn2.php.net/manual/en/splfileobject.fgetcsv.php)</span> ([ <span><span>string</span> `$delimiter`<span> = &quot;,&quot;</span></span> [, <span><span>string</span> `$enclosure`<span> = &quot;&quot;&quot;</span></span> [, <span><span>string</span> `$escape`<span> = &quot;\&quot;</span></span> ]]] )</div>
<div><span>public</span> <span>string</span> <span>[fgets](http://cn2.php.net/manual/en/splfileobject.fgets.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>string</span> <span>[fgetss](http://cn2.php.net/manual/en/splfileobject.fgetss.php)</span> ([ <span><span>string</span> `$allowable_tags`</span> ] )</div>
<div><span>public</span> <span>bool</span> <span>[flock](http://cn2.php.net/manual/en/splfileobject.flock.php)</span> ( <span><span>int</span> `$operation`</span> [, <span><span>int</span> `&amp;$wouldblock`</span> ] )</div>
<div><span>public</span> <span>int</span> <span>[fpassthru](http://cn2.php.net/manual/en/splfileobject.fpassthru.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>int</span> <span>[fputcsv](http://cn2.php.net/manual/en/splfileobject.fputcsv.php)</span> ( <span><span>array</span> `$fields`</span> [, <span><span>string</span> `$delimiter`<span> = ','</span></span> [, <span><span>string</span> `$enclosure`<span> = '&quot;'</span></span> ]] )</div>
<div><span>public</span> <span>mixed</span> <span>[fscanf](http://cn2.php.net/manual/en/splfileobject.fscanf.php)</span> ( <span><span>string</span> `$format`</span> [, <span><span>[mixed](http://cn2.php.net/manual/en/language.pseudo-types.php#language.types.mixed)</span> `&amp;$...`</span> ] )</div>
<div><span>public</span> <span>int</span> <span>[fseek](http://cn2.php.net/manual/en/splfileobject.fseek.php)</span> ( <span><span>int</span> `$offset`</span> [, <span><span>int</span> `$whence`<span> = SEEK_SET</span></span> ] )</div>
<div><span>public</span> <span>array</span> <span>[fstat](http://cn2.php.net/manual/en/splfileobject.fstat.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>int</span> <span>[ftell](http://cn2.php.net/manual/en/splfileobject.ftell.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>bool</span> <span>[ftruncate](http://cn2.php.net/manual/en/splfileobject.ftruncate.php)</span> ( <span><span>int</span> `$size`</span> )</div>
<div><span>public</span> <span>int</span> <span>[fwrite](http://cn2.php.net/manual/en/splfileobject.fwrite.php)</span> ( <span><span>string</span> `$str`</span> [, <span><span>int</span> `$length`</span> ] )</div>
<div><span>public</span> <span>void</span> <span>[getChildren](http://cn2.php.net/manual/en/splfileobject.getchildren.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>array</span> <span>[getCsvControl](http://cn2.php.net/manual/en/splfileobject.getcsvcontrol.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>int</span> <span>[getFlags](http://cn2.php.net/manual/en/splfileobject.getflags.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>int</span> <span>[getMaxLineLen](http://cn2.php.net/manual/en/splfileobject.getmaxlinelen.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>bool</span> <span>[hasChildren](http://cn2.php.net/manual/en/splfileobject.haschildren.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>int</span> <span>[key](http://cn2.php.net/manual/en/splfileobject.key.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>void</span> <span>[next](http://cn2.php.net/manual/en/splfileobject.next.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>void</span> <span>[rewind](http://cn2.php.net/manual/en/splfileobject.rewind.php)</span> ( <span>void</span> )</div>
<div><span>public</span> <span>void</span> <span>[seek](http://cn2.php.net/manual/en/splfileobject.seek.php)</span> ( <span><span>int</span> `$line_pos`</span> )</div>
<div><span>public</span> <span>void</span> <span>[setCsvControl](http://cn2.php.net/manual/en/splfileobject.setcsvcontrol.php)</span> ([ <span><span>string</span> `$delimiter`<span> = &quot;,&quot;</span></span> [, <span><span>string</span> `$enclosure`<span> = &quot;&quot;&quot;</span></span> [, <span><span>string</span> `$escape`<span> = &quot;\&quot;</span></span> ]]] )</div>
<div><span>public</span> <span>void</span> <span>[setFlags](http://cn2.php.net/manual/en/splfileobject.setflags.php)</span> ( <span><span>int</span> `$flags`</span> )</div>
<div><span>public</span> <span>void</span> <span>[setMaxLineLen](http://cn2.php.net/manual/en/splfileobject.setmaxlinelen.php)</span> ( <span><span>int</span> `$max_len`</span> )</div>
<div><span>public</span> <span>bool</span> <span>[valid](http://cn2.php.net/manual/en/splfileobject.valid.php)</span> ( <span>void</span> )</div>
</div>
