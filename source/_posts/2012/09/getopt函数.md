title: getopt函数
tags:
- PHP
date: "2012-09-06 06:17:58"
---

<div>getopt是php在cli环境中使用的一个函数，用户获取执行php脚本时附加的参数</div>
<div>
</div>
<div>首先学习php cli脚本书写方法：</div>
<div>1、增加#!</div>
<div>2、php文件增加x属性，或者使用php file_name.php执行</div>
<div>
</div>
<div>
</div>
<div> #!用于写在脚本文件第一行，用于设定脚本执行的命令</div>
<div>
</div>
<div>#!/usr/bin/env php </div>
<div>表示调用env中的php进行调用</div>
<div>
</div>
<div>之后写正常的php脚本，记得进行php标签包裹</div>
<div>
</div>
<div><span style="color: #000000">&lt;?php</span></div>
<div>
</div>
<div><span style="color: #000000">var_dump($argv);</span></div>
<div>
</div>
<div>
</div>
<div>$argv是一个获取php zai cli状态下执行脚本后的附加参数，例如：</div>
<div>
</div>
<div>cli.php是一个已经增加了x属性的文件，内容如下：</div>
<div>#!/usr/bin/env php</div>
<div>&lt;?php</div>
<div>print_r($argv);</div>
<div>
</div>
<div>
</div>
<div>执行这个文件</div>
<div>./cli.php -h localhost -u may -p</div>
<div>
</div>
<div>页面显示</div>
<div>Array </div>
<div>(</div>
<div>    [0] =&gt; ./index.php</div>
<div>    [1] =&gt; -h</div>
<div>    [2] =&gt; localhost</div>
<div>    [3] =&gt; -u</div>
<div>    [4] =&gt; may</div>
<div>    [5] =&gt; -p</div>
<div>)</div>
<div>
</div>
<div>我们可以进行相关判定并执行相关命令，但是有些情况下，我们并不确定用户输入的是否按照要求进行输入，而且，通常用户都会按照自己个性化的设定去输入，比如某些用户习惯先输入-u may ，这个时候我们可以使用getopt函数进行操作</div>
<div>
</div>
<div>该函数具有两个参数</div>
<div>array getopt($shotopts[, $longopts]);</div>
<div>
</div>
<div>shortopts是获取cli执行中 - 的信息</div>
<div>longopts 是获取cli 执行中的 -- 信息</div>
<div>
</div>
<div>shortopts 是 string</div>
<div>longopts 是array</div>
<div>
</div>
<div>我们重新修改文件如下：</div>
<div>#!/usr/bin/env php</div>
<div>&lt;?php</div>
<div>
</div>
<div>var_dump(getopt('h:u:'));</div>
<div>
</div>
<div>执行</div>
<div>./cli.php  -h localhost -u may -p</div>
<div>
</div>
<div>页面显示</div>
<div>array{</div>
<div>    ['h'] =&gt; 'localhost'</div>
<div>    ['u'] =&gt; 'may'</div>
<div>    ['p'] =&gt; bool(false)</div>
<div>}</div>
<div>
</div>
<div>如果执行该脚本中还有其他附加属性，例如：</div>
<div>./cli.php  -h localhost -u may -p --message='hello world' --ignore_error='true'</div>
<div>我们使用longopts 进行获取</div>
<div>#!/usr/bin/env php</div>
<div>&lt;?php</div>
<div>
</div>
<div>var_dump(getopt('h:u:p：', array('message:', 'ignore_error:')));</div>
<div>
</div>
<div>执行脚本页面显示：</div>
<div>
<div>array{</div>
<div>    ['h'] =&gt; 'localhost'</div>
<div>    ['u'] =&gt; 'may'</div>
<div>    ['p'] =&gt; bool(false)</div>
<div>    ['message'] =&gt; 'hello world'</div>
<div>    ['ignore_error'] =&gt; 'true'</div>
<div>}</div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div><span style="color: #ff0000">shortopts和longopts的value值中，如果后面跟一个：，表示可获取，如果为两个：：，表示可有可无，如果不跟：，则废弃。</span></div>
