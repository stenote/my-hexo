title: Git config
tags:
- Git
date: "2012-09-04 18:49:44"
---

<div>git config可对git库进行相关config设置</div>
<div>常用config命令为</div>
<div>git config --type(--global --system 以及没有) key1.key2 value 用于设置key1.key2 的value</div>
<div>git config --type(--global --system 以及没有) key1.key2 用户获取key1.key2的value</div>
<div>
</div>
<div>e.g </div>
<div>$ git config user.name </div>
<div>may</div>
<div>用户获取当前repository的user的name，git返回may，说明当前repository的user.name为may</div>
<div>$ git config user.name test</div>
<div>用于设置当前repository的user的name为test</div>
<div>
</div>
<div>其实，某些时候，git并不能正确获取repository的user.name等相关设置信息。</div>
<div>git config通常情况下存在于三个地方，第一个是最常接触的repository的.git/config，就是git 开发库，第二个是$HOME，家目录 中的.gitconfig，第三个是/etc/gitconfig文件。</div>
<div>git config 可增加参数获取不同位置的config信息，</div>
<div>--system 为/etc/gitconfig</div>
<div>--global 为~/.gitconfig</div>
<div>什么都不写为 repository/.git/config</div>
<div>
</div>
<div>首先分别在3个位置设置一下user.name</div>
<div>$ cd ~/repository/</div>
<div>$ git config user.name repository</div>
<div>$ git config --global user.name global</div>
<div>$ sudo git config --system user.name system</div>
<div>
</div>
<div>分别获取查看user.name</div>
<div>$ cd ~/repository/</div>
<div>$ git config user.name </div>
<div>repository</div>
<div>$ cd /</div>
<div>$ git config user.name </div>
<div>global</div>
<div>$ git config --global user.name</div>
<div>global</div>
<div>$ sudo git config --system user.name</div>
<div>system</div>
<div>
</div>
<div>在一个非repository的地方获取repository的config信息，由于当前并非git 的repository，所以会尝试获取--global中的信息，如果global中没有信息，会再尝试获取--system中的信息，如果system中也没有config信息，则返回空。</div>
<div>
</div>
<div>执行下列命令，再尝试一下</div>
<div>$ rm -rf ~/.gitconfig</div>
<div>$ cd /</div>
<div>$ git config user.name</div>
<div>system</div>
<div>由于/下并非repository，尝试获取--global内信息，由于global已删除，再尝试获取system中信息，返回user.name为system</div>
<div>
</div>
<div>$ sudo rm -rf /etc/gitconfig</div>
<div>$ cd /</div>
<div>$ git config user.name</div>
<div>
</div>
<div>返回空</div>
<div>$ cd ~/repository</div>
<div>$ git config user.name</div>
<div>repository</div>
<div>返回repository中的user.name为repository</div>
<div>
</div>
<div>同样，进行config信息设定的时候也可增加--global --system等相关参数进行设定</div>
<div>$ cd ~/repository</div>
<div>$ git config user.name new_repostiory</div>
<div>$ git config --global user.name new_global</div>
<div>$ sudo git config --system user.name new_system</div>
<div>
</div>
<div>再次不再重述</div>
<div>
</div>
<div>常用设置：</div>
<div>git config --global user.name</div>
<div>git config --global user.email</div>
<div>git config --global color.ui</div>
<div>git config --global core.editor</div>
<div>git config --global receive.denyCurrentBranch ignore 设置可再当前branch push</div>
<div>git config --global receive.denyNonFastForwards true 设定不可强行步入fastforwards</div>
<div>git config --global alias.a add 设定git a 功能与git add 相同。当然也可通过设定.bashrc等设定alias来达到这个类似效果: alias add='git add' </div>
<div>
</div>
<div>其他git config 配置后续补全</div>
<div>
</div>
<div>
</div>
<div>还可通过git config -e --global直接进入编辑器编辑模式手动进行编辑，编辑器会默认获取git config --global core.editor。如果不存在会用nano（ubuntu 11.10下亲测），可预先设定git config --global core.editor vim ,git config --global core.editor emacs等使用顺手编辑器编辑gitconfig文件。</div>
<div>例如git config --global user.name在gitconfig文件中设置如下</div>
<div>[user]</div>
<div>    name = global</div>
<div>修改global为labolg后保存，重新获取user.name</div>
<div>$ git config --global user.name</div>
<div>labolg</div>
<div>
</div>
<div>
</div>
<div>删除config信息可以使用如下命令</div>
<div>$ git config --unset --global user.name</div>
<div>$ git config --unset user.name</div>
<div>$ sudo git config --unset --system user.name</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
<div>
</div>
