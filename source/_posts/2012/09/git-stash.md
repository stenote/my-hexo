title: git stash
tags:
- Git
date: "2012-09-04 20:00:28"
---

<div>stash的意思是隐藏，在git中，我们使用git stash命令对已经修改、修改后add的代码进行隐藏（实际上是保存在另外一个地方），在需要的时候再将这部分代码拿出来，通常使用在，一个任务在进行过程中，我们需要停止该任务，但是当前代码开发了一半了，那么我们可以使用git stash命令进行相关隐藏后再去开发其他任务，待其他任务开发完成后，可以使用git stash pop或者git stash apply ，讲某个隐藏了的代码拿出来继续修改</div>
<div>
</div>
<div>$ git stash list //查看当前stash状态</div>
<div>stash@{0}: WIP on develop: dc68b8e git init</div>
<div>
</div>
<div>可见当前在develop分支上有一个stash时所在commit-id为dc68b8e ，message为git init的stash代码块，该stash的编号为stashs@{0}</div>
<div>

* * *
</div>
<div>$ git stash show stash@{0}</div>
<div>

index.php | 1 +
 1 file changed, 1 insertion(+)

页面显示为index.php文件增加了一行

* * *

$ git stash apply stash@{0} //从stash区域中拿出来stash@{0}的代码，同时<span style="color: #ff0000">不删除</span>stash区域

$ git status

# On branch develop
# Changes not staged for commit:
# (use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
# (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)
#
# modified: index.php
#
no changes added to commit (use &quot;git add&quot; and/or &quot;git commit -a&quot;)

* * *

$ git stash list

stash@{0}: WIP on develop: dc68b8e git init

刚才的stash依然存在

* * *

$ git checkout ./

清空所有的修改代码

* * *

$ git stash pop stash{0} //从stash区域中拿出来stash@{0}的代码，同时<span style="color: #ff0000">删除</span>stash区域中stash@{0}的代码

$ git status

# On branch develop
# Changes not staged for commit:
# (use &quot;git add &lt;file&gt;...&quot; to update what will be committed)
# (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)
#
# modified: index.php
#
no changes added to commit (use &quot;git add&quot; and/or &quot;git commit -a&quot;)

Dropped stash@{0} (c3a3674ae870327d4d22105979211ee4fb76a033) //提示<span style="color: #ff0000">删除</span>了stash{0}

文件被拿出来了看看stash list

$ git stash list

无显示，为空

对当前状态进行stash

* * *

$ git stash save '当前隐藏' //可直接stash，但是不会创建message，不便于记录

$ git stash list

stash@{0}: On develop: 当前隐藏

$ git stash show stash@{0} //查看这个stash

index.php | 1 +
 1 file changed, 1 insertion(+)

$ git stash drop stash@{0}

Dropped stash@{0} (6a24cd3203760e016ed34a4a5b557c3f0b670ee3) 

提示删除了这个stash

$ git statsh list  

//页面显示为空

创建一堆stash

$ git stash list

stash@{0}: WIP on develop: dc68b8e git init
stash@{1}: WIP on develop: dc68b8e git init
stash@{2}: WIP on develop: dc68b8e git init

* * *

清空stash列表

$ git stash clear

$ git stash list

//页面显示为空

<span style="color: #ff0000">不可对单一文件或者目录进行stash</span>

</div>
