title: Tmux  GNU Screen 的替代品
tags:
- Linux
date: "2012-07-07 20:15:45"
---

-- 基本使用

tmux   //运行

C-b d   //返回主 shell ， tmux 依旧在后台运行，里面的命令也保持运行状态

tmux attach  //恢复tmux

tmux 相关内容
功能：替换screen命令
内部概念： session window 
session为一个会话，可以理解为又开启了一个新的进程。
window 为一个视窗，这个视窗不是里面的窗口。相当于开启了一个新的tab
命令格式 ：
ctrl + b后再执行相关命令，需要注意的是ctrl + b可能和emacs冲突
运行tmux：
直接输入tmux进行运行，会开启一个新的window。可以在多个window中切换
恢复已有的tmux，使用tmux attach（前面的几个字母即可，例如tmux att）
退出tmux，使用exit完全退出，或者使用ctrl + b d 退出。d 的意思是detache
还有一个退出方法ctrl + b &amp; 这个会依次kill到打开的tab（window）
以下为一些基本的命令
1、多session 多window（tab）切换
ctrl + b s (s即session)
ctrl + b w （w即window）
执行上述命令后显示系统中对应的session或者windows，上下移动后回车即可选定对应的session或者window

创建一个window(tab)， 使用ctrl + b c 进行创建，创建后回切换到新的window（tab）中，并且下端tmux绿色的列表中显示新增加的window（tab）
关闭一个window(tab)，使用ctrl + b x 然后y进行关闭。

切换window，通常使用如下命令：
ctrl + b n 切换到下一个window next
ctrl + b p 切换到上一个window previous
ctrl +b 数字 切换到数字对应的window，每个window前都会显示数字的。数字从0开始。
ctrl + b l 切换到最后一个window last
ctrl + b f 查找对应名称的window，回车后直接切换。

默认情况window的名称为当前shell名称，比如bash，tmux还提供了一个修改window的命令
ctrl + b , 
然后输入新的window名称即可。不过对中文的支持不是太好

一个window中，我们可以创建多个小的window，这样便于在页面中进行管理。
ctrl + b &quot; 
ctrl + b %
这两个都是可以进行页面拆分的。不建议记录，拆分后可以使用
ctrl + b space 进行页面重新布局的。tmux提供了多种不同的布局样式，一般情况都可满足使用

在一个window中的不同的window进行切换，使用ctrl + b o进行切换
或者使用ctrl + b 上下左右进行切换

如果想要保留所有window，并且让当前光标所在window为主window，可使用
ctrl + b ! ，其他的的该window内的window就会自动增加到window列表中的一个新的window中，而当前window成为一个独立的window

session的创建，一般开启另外一个进程后才可产生一个新的session
快捷session命令如下：，
ctrl + b ) 切换到下一个session
ctrl + b ( 切换到上一个session
修改session名称使用ctrl + b $ 可进行修改
貌似tmux没提供一个快速的切换session的命令

-- 快捷键

tmux 的使用主要就是依靠快捷键，通过 C-b 来调用。

C-b ?  // 显示快捷键帮助

C-b C-o  //调换窗口位置

C-b 空格键  //采用下一个内置布局

C-b ! // 把当前窗口变为新窗口

C-b  &quot;  // 模向分隔窗口

C-b % // 纵向分隔窗口

C-b q // 显示分隔窗口的编号

C-b o // 跳到下一个分隔窗口

C-b 上下键 // 上一个及下一个分隔窗口

C-b C-方向键 //调整分隔窗口大小

C-b &amp; // 确认后退出 tmux

C-b c // 创建新窗口

C-b 0~9 //选择几号窗口

C-b c // 创建新窗口

C-b n // 选择下一个窗口

C-b l // 最后使用的窗口

C-b p // 选择前一个窗口

C-b w // 以菜单方式显示及选择窗口

C-b s // 以菜单方式显示和选择会话

C-b t //显示时钟

<div>
</div>
