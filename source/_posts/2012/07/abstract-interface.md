title: abstract interface
tags:
- PHP
date: "2012-07-11 06:35:43"
---

<div>abstract class是一个通常用来定义部分抽象函数的基类，抽象的函数不具有函数体，而且抽象函数必须被重载。</div>
<div>abstract class Base_Class {</div>
<div>    abstract function save();</div>
<div>    abstract function delete();</div>
<div>    abstract function update();</div>
<div>    abstract function query();</div>
<div>    abstract function total_count();</div>
<div>    public function hello () {</div>
<div>       echo 'hello world';</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>class Main extends Base_Class {</div>
<div>    public function save() {}</div>
<div>    public function delete() {}</div>
<div>    public function update() {}</div>
<div>    public function query() {}</div>
<div>    public function total_count() {}</div>
<div>    public function hello () {</div>
<div>        echo date('Y/m/d H:i:s', time());</div>
<div>        parent::hello();</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>上述，创建了一个抽象的类，并创建了部分抽象方法，抽象方法没有函数体， 然后用一个正常的类继承了这个抽象类，然后重载了这个类的抽象方法，并重载了hello这个非抽象方法，然后调用Base_Class的hello方法</div>
<div>
</div>
<div>抽象类的特点是，对需要调用的函数进行了抽象处理，不具有函数体，需要继承的子类，必须去重载抽象函数，否则报错。</div>
<div>缺点是，抽象类和其他的类一样，不能进行多重继承。</div>
<div>
</div>
<div>为了做到多重集成，php提供了接口</div>
<div>interface IBase_Interface {</div>
<div>    function save();</div>
<div>    function update();</div>
<div>    function delete();</div>
<div>    function query();</div>
<div>    function total_count();</div>
<div>    function hello();</div>
<div>}</div>
<div>
</div>
<div>class Main implements IBase_Interface {</div>
<div>
<div>    public function save() {}</div>
<div>    public function delete() {}</div>
<div>    public function update() {}</div>
<div>    public function query() {}</div>
<div>    public function total_count() {}</div>
<div>    public function hello () {}</div>
</div>
<div>}</div>
<div>
</div>
<div>使用implements去实现一个接口，并且需要完全重载接口提供的方法。</div>
<div>接口的好处是，可以进行多个接口的实现，缺点是，接口不提供特定的一些常用的有函数体的方法。</div>
<div>为此，可以使用一个基类实现一个接口，其他子类再去继承这个接口的形式。</div>
<div>class Main extends Base_Class implements IBase_Interface {}</div>
<div>
</div>
