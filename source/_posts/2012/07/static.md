title: static
tags:
- PHP
date: "2012-07-11 06:48:11"
---

<div>static通常意为静态的。通常使用在一个函数的static变量中，或者使用在类中的静态成员和静态方法，以及调用自己的静态方法和父类的非静态方法</div>
<div>函数内的静态方法不常用，不做解释</div>
<div>
</div>
<div>class People {</div>
<div>    static $school = 'foobar';</div>
<div>
</div>
<div>    function say_static_school() {</div>
<div>       echo self::$school;</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>
</div>
<div>class User extends People {</div>
<div>    static $school = 'tjut';</div>
<div>    static function say_user_static_school () {</div>
<div>         echo self::$school;</div>
<div>         </div>
<div>    }</div>
<div>
</div>
<div>    function say_school() {</div>
<div>        echo self::$school;</div>
<div>        parent::say_static_school();</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>echo People::$school;   //foobar</div>
<div>echo User::$school;    //tjut</div>
<div>
</div>
<div>User::say_user_static_school();  //tjut</div>
<div>$may = new User();</div>
<div>$may-&gt;say_school();  //tjutfoobar</div>
