title: Ruby基本命令
tags:
- Ruby
date: "2012-07-02 02:10:57"
---

### print puts p

Ruby 常用的输出关键字为 `print`，`puts`， `p`，区别如下

* `print` 和 `puts` 都是输出相关信息，不过 puts 会自动增加 `\n`换行
* `p` 用于直接输出 ruby 内部存储的字符或字符串

### Ruby中字符连接使用 `+`，类似于 javascript 
### 对于库，需要使用 `include Math` 等类似命令先载入一下
### Ruby 的注释

Ruby 的注释比较奇特:

* `#` 单行注释
* `#sdfadf#` 单行内的注释
* > `=begin`
  > 多行注释，好诡异的注释方法
  > `=end`

---

```
# 使用 each 进行遍历, 传入 block
a = ['may', 'qi', 'chao']
a.each{|n|
  puts n    
}

may = {'name'=>'may', 'age'=>20}
may.each{|key, value|
    print key, ' => ', value, "n"
}
```

---

ARGV 这个数组内的值为通过 cli 中后的数值

```
#!/usr/bin/env ruby
fileHandler = File.open('hello.rb')
while line = fileHandler.gets do
    puts line
end
```

---

### 读取文件内容


#### 逐行读取
```
#!/usr/bin/env ruby
fileHandler = File.open('hello.rb')
while line = fileHandler.gets do
    puts line
end
```

#### 批量读取

```
#!/usr/bin/env ruby
fileHandler = File.open('hello.rb')
puts fileHandler.read
```