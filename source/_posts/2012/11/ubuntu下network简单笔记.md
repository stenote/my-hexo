title: ubuntu下简单的network相关笔记
tags:
- Linux
date: "2012-11-27 05:07:38"
---

<div>ubuntu下简单的network笔记如下：</div>
<div>查看当前ip状态 ifconfig</div>
<div>结果可能如下：</div>
<div>

eth0 Link encap:Ethernet HWaddr 00:30:18:a1:0c:73 
 inet addr:192.168.1.15 Bcast:192.168.1.255 Mask:255.255.255.0
 inet6 addr: fe80::230:18ff:fea1:c73/64 Scope:Link
 UP BROADCAST RUNNING MULTICAST MTU:1500 Metric:1
 RX packets:15442 errors:3 dropped:0 overruns:0 frame:2
 TX packets:12232 errors:0 dropped:0 overruns:0 carrier:8
 collisions:0 txqueuelen:1000 
 RX bytes:8981516 (8.9 MB) TX bytes:2176124 (2.1 MB)

lo Link encap:Local Loopback 
 inet addr:127.0.0.1 Mask:255.0.0.0
 inet6 addr: ::1/128 Scope:Host
 UP LOOPBACK RUNNING MTU:16436 Metric:1
 RX packets:4 errors:0 dropped:0 overruns:0 frame:0
 TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
 collisions:0 txqueuelen:0 
 RX bytes:240 (240.0 B) TX bytes:240 (240.0 B)

可以看到lo和eth0两个设备（其实lo）不是设备，具体是啥不知道，如上可查看到网卡的mac地址，ip地址

重启网络：

sudo service networking restart

</div>
<div>单一网卡重启</div>
<div>sudo ifdown eth0</div>
<div>sudo ifup eth0</div>
<div>
</div>
<div>查看电脑网卡设备</div>
<div>cat /proc/network/dev</div>
<div>
</div>
<div>修改电脑网卡名称</div>
<div>/etc/udev/rules.d/70-persistent-net.rules</div>
<div>修改eth名称即可</div>
<div>
</div>
