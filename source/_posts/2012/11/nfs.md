title: nfs
tags:
- Linux
date: "2012-11-27 06:29:23"
---

<div>http://linux.chinaunix.net/techdoc/system/2009/02/06/1060861.shtml</div>
<div>
</div>
<div>protmap已经过时，被替换为了rpcbind，但是服务停止时候还是需要使用</div>
<div>sudo service portmap stop/start/restart 等进行修正</div>
<div>
</div>
<div>http://blog.sina.com.cn/s/blog_86ca10130100vhrb.html</div>
<div>
</div>
<div>nfs需要server和client都安装nfs相关文件：</div>
<div>server： nfs-kernel-server</div>
<div>client： nfs-common</div>
<div> 需要注意的是，安装时候，系统会自动安装rpcbind等相关附加文件</div>
<div>
</div>
<div>安装成功后，在server端进行服务状态查看</div>
<div>sudo service nfs-kernel-server status</div>
<div>服务显示</div>
<div>nfsd not running</div>
<div>尝试直接启动nfs-kerner-server</div>
<div>sudo service nfs-kernel-server start</div>
<div>系统显示</div>
<div>

Not starting NFS kernel daemon: no exports.

页面提示无法启动NFS核心服务，没有exports

exports介绍：

<div>这个文件的内容非常简单，每一行由抛出路径，客户名列表以及每个客户名后紧跟的访问选项构成：</div>
> <pre>&lt;span style=&quot;font-size: small;&quot;&gt;[共享的目录] [主机名或IP(参数,参数)]&lt;/span&gt;</pre>
<div>其中参数是可选的，当不指定参数时，nfs将使用默认选项。默认的共享选项是 sync,ro,root_squash,no_delay。</div>
<div>
</div>
<div>当主机名或IP地址为空时，则代表共享给任意客户机提供服务。</div>
<div>当将同一目录共享给多个客户机，但对每个客户机提供的权限不同时，可以这样：</div>
> <pre>&lt;span style=&quot;font-size: small;&quot;&gt;[共享的目录] [主机名1或IP1(参数1,参数2)] [主机名2或IP2(参数3,参数4)]&lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt; 下面是一些NFS共享的常用参数：&lt;/span&gt;</pre>
> <div>
> </div>
> <div>
> </div>
> > <pre>&lt;span style=&quot;font-size: small;&quot;&gt;ro                      只读访问 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        rw                      读写访问 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        sync                    所有数据在请求时写入共享 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        async                   NFS在写入数据前可以相应请求 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        secure                  NFS通过1024以下的安全TCP/IP端口发送 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        insecure                NFS通过1024以上的端口发送 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        wdelay                  如果多个用户要写入NFS目录，则归组写入（默认） &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        no_wdelay               如果多个用户要写入NFS目录，则立即写入，当使用async时，无需此设置。 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        hide                    在NFS共享目录中不共享其子目录 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        no_hide                 共享NFS目录的子目录 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        subtree_check           如果共享/usr/bin之类的子目录时，强制NFS检查父目录的权限（默认） &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        no_subtree_check        和上面相对，不检查父目录权限 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        all_squash              共享文件的UID和GID映射匿名用户anonymous，适合公用目录。 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        no_all_squash           保留共享文件的UID和GID（默认） &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        root_squash             root用户的所有请求映射成如anonymous用户一样的权限（默认） &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        no_root_squas           root用户具有根目录的完全管理访问权限 &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        anonuid=xxx             指定NFS服务器/etc/passwd文件中匿名用户的UID &lt;/span&gt;&lt;br clear=&quot;none&quot;/&gt;&lt;span style=&quot;font-size: small;&quot;&gt;        anongid=xxx             指定NFS服务器/etc/passwd文件中匿名用户的GID&lt;/span&gt;</pre>
> > <div>
> > </div>
</div>
<div>例如：我们创建一个/home/share的目录，这个目录的owner group为nobody nogroup，设定具有读写权限，可使用</div>
<div>/home/share *(rw,sync,no_subtree_check,anonuid=65534,anongid=65534)</div>
<div>
</div>
<div>设定成功后</div>
<div>sudo service nfs-kernel-server start</div>
<div>系统显示：</div>
<div>

* Exporting directories for NFS kernel daemon... [ OK ] 
 * Starting NFS kernel daemon

客户端查看服务器中开放的状态

sudo showmount -e 192.168.1.55 (这个ip为server的ip)

Export list for 192.168.1.55:
/home/share *

页面提示/home/share/给所有ip的服务器都进行了开放

尝试挂载到/mnt/nfs目录中

mount 192.168.1.55:/home/share /mnt/nfs

挂载成功

取消挂在

umount /mnt/nfs，不需要使用192.168.1.55等相关信息即可

</div>
<div>
</div>
