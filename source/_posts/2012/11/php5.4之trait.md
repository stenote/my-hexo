title: php5.4 之trait
tags:
- PHP
date: "2012-11-12 01:14:30"
---

<div>trait是php5.4中的一个新特定。</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">trait的语法如下</div>
<div style="margin-left: 30px">trait trait_name {</div>
<div style="margin-left: 60px">function func1() {</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">}</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">function func2() {</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px">}</div>
<div style="margin-left: 30px">}</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">class A {</div>
<div style="margin-left: 60px">use trait_name;</div>
<div style="margin-left: 30px">}</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">如上我们创建了个trait，它具有两个方法，func1 func2 </div>
<div style="margin-left: 30px">class A中use了这个trait，所以class A也具有这两个方法，我们可以如下调用</div>
<div style="margin-left: 30px">$a = new A;</div>
<div style="margin-left: 30px">$a-&gt;func1();</div>
<div style="margin-left: 30px">$a-&gt;func2()</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">如果class A 中已经存在trait中的function，那么会调用class A的函数，而不是trait中的函数</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">trait Trait_Class {</div>
<div style="margin-left: 60px">function a() {</div>
<div style="margin-left: 90px">echo 'a';</div>
<div style="margin-left: 60px">}</div>
<div style="margin-left: 30px">}</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">class A {</div>
<div style="margin-left: 60px">use Trait_Class;</div>
<div style="margin-left: 60px">function a() {</div>
<div style="margin-left: 90px">echo 'hello world';</div>
<div style="margin-left: 60px">}</div>
<div style="margin-left: 30px">}</div>
<div style="margin-left: 30px">$a = new A;</div>
<div style="margin-left: 30px">$a-&gt;a();</div>
<div style="margin-left: 30px">页面会显示hello world， 不会显示a，因为class A中已经存在了a函数，所有不会去调用Trait_Class中的a函数。</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">trait就相当与一个片段的class，包含了class的变量、方法，但是不能被直接通过new方法使用，需要在一个class中使用use关键字加载trait，然后再通过new关键字调用trait中的方法。需要注意的是，trait中的变量、方法等不会重载class中已存在的变量、方法。</div>
<div style="margin-left: 30px"></div>
