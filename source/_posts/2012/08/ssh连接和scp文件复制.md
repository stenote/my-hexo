title: ssh 连接和scp 文件复制 （转载）
tags:
- Linux
date: "2012-08-15 06:22:46"
---

<div>scp是 secure copy的缩写, scp是linux系统下基于ssh登陆进行安全的远程文件拷贝命令。linux的scp命令可以在linux服务器之间复制文件和目录.</div>
<div>scp命令的用处：</div>
<div>scp在网络上不同的主机之间复制文件，它使用ssh安全协议传输数据，具有和ssh一样的验证机制，从而安全的远程拷贝文件。</div>
<div>scp命令基本格式：</div>
<div>scp [-1246BCpqrv] [-c cipher] [-F ssh_config] [-i identity_file]</div>
<div>[-l limit] [-o ssh_option] [-P port] [-S program]</div>
<div>[[user@]host1:]file1 [...] [[user@]host2:]file2</div>
<div>scp命令的参数说明:</div>
<div>-1</div>
<div>强制scp命令使用协议ssh1</div>
<div>-2</div>
<div>强制scp命令使用协议ssh2</div>
<div>-4</div>
<div>强制scp命令只使用IPv4寻址</div>
<div>-6</div>
<div>强制scp命令只使用IPv6寻址</div>
<div>-B</div>
<div>使用批处理模式（传输过程中不询问传输口令或短语）</div>
<div>-C</div>
<div>允许压缩。（将-C标志传递给ssh，从而打开压缩功能）</div>
<div>-p 保留原文件的修改时间，访问时间和访问权限。</div>
<div>-q</div>
<div>不显示传输进度条。</div>
<div>-r</div>
<div>递归复制整个目录。</div>
<div>-v 详细方式显示输出。scp和ssh(1)会显示出整个过程的调试信息。这些信息用于调试连接，验证和配置问题。</div>
<div>-c cipher</div>
<div>以cipher将数据传输进行加密，这个选项将直接传递给ssh。</div>
<div>-F ssh_config</div>
<div>指定一个替代的ssh配置文件，此参数直接传递给ssh。</div>
<div>-i identity_file</div>
<div>从指定文件中读取传输时使用的密钥文件，此参数直接传递给ssh。</div>
<div>-l limit</div>
<div>限定用户所能使用的带宽，以Kbit/s为单位。</div>
<div>-o ssh_option</div>
<div>如果习惯于使用ssh_config(5)中的参数传递方式，</div>
<div>-P port  注意是大写的P, port是指定数据传输用到的端口号</div>
<div>-S program</div>
<div>指定加密传输时所使用的程序。此程序必须能够理解ssh(1)的选项。</div>
<div>
</div>
<div>公司进行了端口转发，使用ssh连接命令为：</div>
<div>
</div>
<div>ssh -p 9628 rui.ma@vodka.genee.cc</div>
<div>
</div>
<div>scp命令的实际应用</div>
<div>1&gt;从本地服务器复制到远程服务器</div>
<div>(1) 复制文件：</div>
<div>命令格式：</div>
<div>scp local_file remote_username@remote_ip:remote_folder</div>
<div>或者</div>
<div>scp local_file remote_username@remote_ip:remote_file</div>
<div>或者</div>
<div>scp local_file remote_ip:remote_folder</div>
<div>或者</div>
<div>scp local_file remote_ip:remote_file</div>
<div>第1,2个指定了用户名，命令执行后需要输入用户密码，第1个仅指定了远程的目录，文件名字不变，第2个指定了文件名</div>
<div>第3,4个没有指定用户名，命令执行后需要输入用户名和密码，第3个仅指定了远程的目录，文件名字不变，第4个指定了文件名</div>
<div>实例：</div>
<div>scp /home/linux/soft/scp.zip root@www.mydomain.com:/home/linux/others/soft</div>
<div>scp /home/linux/soft/scp.zip root@www.mydomain.com:/home/linux/others/soft/scp2.zip</div>
<div>scp /home/linux/soft/scp.zip www.mydomain.com:/home/linux/others/soft</div>
<div>scp /home/linux/soft/scp.zip www.mydomain.com:/home/linux/others/soft/scp2.zip</div>
<div>(2) 复制目录：</div>
<div>命令格式：</div>
<div>scp -r local_folder remote_username@remote_ip:remote_folder</div>
<div>或者</div>
<div>scp -r local_folder remote_ip:remote_folder</div>
<div>第1个指定了用户名，命令执行后需要输入用户密码；</div>
<div>第2个没有指定用户名，命令执行后需要输入用户名和密码；</div>
<div>例子：</div>
<div>scp -r /home/linux/soft/ root@www.mydomain.com:/home/linux/others/</div>
<div>scp -r /home/linux/soft/ www.mydomain.com:/home/linux/others/</div>
<div>上面 命令 将 本地 soft 目录 复制 到 远程 others 目录下，即复制后远程服务器上会有/home/linux/others/soft/ 目录</div>
<div>2&gt;从远程服务器复制到本地服务器</div>
<div>从远程复制到本地的scp命令与上面的命令雷同，只要将从本地复制到远程的命令后面2个参数互换顺序就行了。</div>
<div>例如：</div>
<div>scp root@www.mydomain.com:/home/linux/soft/scp.zip /home/linux/others/scp.zip</div>
<div>scp www.mydomain.com:/home/linux/soft/ -r /home/linux/others/</div>
<div>linux系统下scp命令中很多参数都和 ssh1 有关 , 还需要看到更原汁原味的参数信息,可以运行man scp 看到更细致的英文说明. </div>
<div>
</div>
<div>公司进行了端口转发。使用scp命令如下：</div>
<div>scp -P 9628 rui.ma@vodka.genee.cc:~/lims2_may.sql ./</div>
<div>scp -P 9628 -r rui.ma@vodka.genee.cc:~/lims2/ ./</div>
