title: vim 宏 （宏的录制、调用）
tags:
- vim
date: "2012-08-25 18:28:53"
---

<div>vim提供了宏录制，可以将重复操作的步骤录制为宏，然后自动设定宏脚本运行次数，即可完成多次宏中所录制的步骤，可以极大地提高工作效率。</div>
<div>
</div>
<div>1、宏的录制开始</div>
<div style="margin-left: 30px">进入正常模式（非插入模式、非命令行模式），点击q后，vim会自动进入宏录制模式</div>
<div style="margin-left: 30px"></div>
<div>2、宏的名称</div>
<div style="margin-left: 30px">q之后输入的下一个字符会是宏的名称，如果已经存在之前的宏的名称，系统会进行替换。通常宏的名称是字母。</div>
<div style="margin-left: 30px">例如qa，宏的名称就是a。</div>
<div style="margin-left: 30px">需要注意的是，宏的名称不能为多个字符。比如qdelete_quote ，系统会记录宏的名称为d，同时vim中运行elete_quote</div>
<div style="margin-left: 30px"></div>
<div>3、宏的录制</div>
<div style="margin-left: 30px">这个时候，我们执行的所有的操作，系统都会进行记录。</div>
<div style="margin-left: 30px">例如：我们需要删除每行开头的第一个单词，需要进行的操作是，移动到这一样的开头，删除第一个单词，然后移动到下一行即可。执行如下：</div>
<div style="margin-left: 30px">^dawj</div>
<div style="margin-left: 30px">^为移动到行首</div>
<div style="margin-left: 30px">daw为delete a world</div>
<div style="margin-left: 30px">j为移动到下一行</div>
<div style="margin-left: 30px"></div>
<div>4、退出录制</div>
<div style="margin-left: 30px">正常模式情况下，按q直接推出录制。</div>
<div style="margin-left: 30px"></div>
<div>5、调用宏</div>
<div style="margin-left: 30px">正常模式情况下，输入重复次数，@符号，以及宏名称进行重复调用</div>
<div style="margin-left: 30px">例如上述的名称为a的宏，如果调用100次，只需要执行：</div>
<div style="margin-left: 30px">100@a</div>
<div style="margin-left: 30px">即可，如果不设定数字，则表示执行一次</div>
<div style="margin-left: 30px">@a</div>
<div>
</div>
<div>6、宏的保存</div>
<div style="margin-left: 30px">如果我们需要对一个特定的宏进行记录和保存，不想每次都在开发过程中重复进行操作，那么可以对宏进行保存。</div>
<div style="margin-left: 30px">比如刚才的宏。我们执行后，可以进行保存。</div>
<div style="margin-left: 30px">需要在正常模式下，执行 &quot;ap 即可，通过这个命令我们也可以发现，宏其实是存储在vim的寄存器中的。</div>
<div style="margin-left: 30px">我们执行&quot;ap后发现页面显示</div>
<div style="margin-left: 30px">^dawj（如果存在esc或者回车，宏会自动转义为自己理解的其他字符）</div>
<div style="margin-left: 30px">或者我们正常模式执行 :reg ，查看宏名称对应的寄存器也可</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">我们对获取到的宏的内容写入.vimrc 中，格式如下：</div>
<div style="margin-left: 30px"> </div>
<div style="margin-left: 30px">let @a=&quot;^dawj&quot;</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">意思为，设定 宏 a 为 ^dawj </div>
<div style="margin-left: 30px">然后我们就可以在系统任何打开vim的地方直接进行执行，而不需要进行其他录制。</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">如果在vim中重新创建新的名称相同的宏，那么系统默认的宏会被覆盖。</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">我们公司的cli脚本在创建的时候，需要填写相关文件信息、注视，我做了一个类似的宏脚本，如下：</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">let @c=&quot;1ggi#!/usr/bin/env php^M&lt;?php^M /*^M * file ^[^[o* author Rui Ma &lt;&gt;&lt;80&gt;klrui.ma@geneegroup.com^[o* data&lt;80&gt;kbe ^M*^M* useage^M* breif&lt;80&gt;kb&lt;80&gt;kb&lt;80&gt;kbief^M*/^[:w^M^M&quot;</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">执行后效果如下：</div>
<div style="margin-left: 60px">
</div>
<div style="margin-left: 60px"></div>
<div style="margin-left: 60px"></div>