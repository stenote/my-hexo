title: Countable
tags:
- PHP
date: "2012-08-17 18:39:43"
---

<div>Countable为一个interface。</div>
<div>用于某个对象的原型实现后。使用count方法进行获取这个对象的相关信息。</div>
<div>例如：</div>
<div>class User implements Countable {</div>
<div>    public function count() {</div>
<div>        return 100;</div>
<div>    }</div>
<div>}</div>
<div>
</div>
<div>$user = new User();</div>
<div>echo count($user);</div>
<div>页面显示100</div>
<div>或者echo $user-&gt;count();</div>
