title: vim复制粘贴
tags:
- vim
date: "2012-08-14 05:08:17"
---

如果只是想使用系统粘贴板的话直接在输入模式按Shift+Insert（粘贴）就可以了，部分键盘下没有Insert键。那么我们只能使用&quot;+p进行粘贴了。

下面讲一下**vim**的粘贴板的基础知识，有兴趣的可以看看，应该会有所收获的。

**vim**帮助文档里与粘贴板有关的内容如下：

1.  **vim**有12个粘贴板，分别是0、1、2、...、9、a、“、＋；用:reg命令可以查看各个粘贴板里的内容。在**vim**中简单用y只是**复制**到“（双引号)粘贴板里，同样用p粘贴的也是这个粘贴板里的内容；

不同版本的vim下的粘贴板的个数和名称不同！在使用之前建议使用:reg命令进行查看。

2.  要将**vim**的内容**复制**到某个粘贴板，需要退出编辑模式，进入正常模式后，选择要**复制**的内容，然后按&quot;Ny（注意带引号）完成**复制**，其中N为粘贴板号(注意是按一下双引号然后按粘贴板号最后按y)，例如要把内容**复制**到粘贴板a，选中内容后按&quot;ay就可以了，有两点需要说明一下：

    *   “号粘贴板（临时粘贴板）比较特殊，直接按y就**复制**到这个粘贴板中了，直接按p就粘贴这个粘贴板中的内容；
    *   +号粘贴板是系统粘贴板，用&quot;+y将内容**复制**到该粘贴板后可以使用Ctrl＋V将其粘贴到其他文档（如firefox、gedit）中，同理，要把在其他地方用Ctrl＋C或右键**复制**的内容**复制**到**vim**中，需要在正常模式下按&quot;+p；

3.  要将**vim**某个粘贴板里的内容粘贴进来，需要退出编辑模式，在正常模式按&quot;Np，其中N为粘贴板号，如上所述，可以按&quot;5p将5号粘贴板里的内容粘贴进来，也可以按&quot;+p将系统全局粘贴板里的内容粘贴进来。
<div><span>注意：在我这里，只有vim.gtk或vim.gnome才能使用系统全局粘贴板，默认的vim.basic看不到+号寄存器。安装vim.gnome使用apt-get install vim-gnome，然后vim自动会链接到vim.gnome。

下面是vim复制粘贴的基本命令：
</span><span>yy复制游标所在行整行。或大写一个Y。 </span>
<span>2yy或y2y复制两行。请举一反三好不好！ :-) </span>
<span>y^复制至行首，或y0。不含游标所在处字元。 </span>
<span>y$复制至行尾。含游标所在处字元。 </span>
<span>yw复制一个word。 </span>
<span>y2w复制两个字（单词）。 </span>
<span>yG复制至档尾。 </span>
<span>y1G复制至档首。 </span>
<span>p小写p代表贴至游标后（下）。 </span>
<span>P大写P代表贴至游标前（上）。</span>

<span>复制单个字符</span>
<span>首选进入正常模式（按ESC就行）</span>
<span>然后按ｖ（指定</span><span>粘贴板为&quot;1v 引号不能少</span><span>），进入visual方式，然后就可以移动方向键选中文本，然后按y，就拷贝完成，</span>
<span>如果你要从光标处开始复制 4 个字符，可以按 4yl (复制光标后的)（&quot;14yl）(&quot;110yl 后面10个字符),4yh(复制光标前的) ，就复制了4个字符到缓冲区中了，</span></div>

按下来就可以用 p （指定<span>粘贴板为&quot;1v 引号不能少</span>）命令随便粘贴了（1为指下粘贴板名）

常用命令：

1、&quot;+y 复制到全局粘贴板

2、&quot;+p 从全局粘贴板获取出来，需要注意的是，即使vim被关闭，粘贴版的内容也会被保留下来。

3、&quot;1y 复制到第一粘贴板

4、&quot;1p 从第一粘贴板中获取出来， 同样，我们可以使用&quot;&quot;y 直接放入到vim中&quot;&quot;粘贴板中，一会可以直接使用p去将结果粘贴到页面中。
