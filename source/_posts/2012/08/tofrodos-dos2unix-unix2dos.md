title: tofrodos (dos2unix unix2dos)
tags:
- Linux
date: "2012-08-17 18:35:29"
---

<div>linux中文件结尾是使用n，而dos中文件行末使用rn，为了进行行末代码统一，linux下提供了不同文件类型（dos文件-》unix文件，unix-》dos）之间的转换命令。</div>
<div>在ubuntu 10.04之前（包括10.04）可使用unix2dos、 dos2unix进行转换。之后不知由于何种原因，ubuntu的源中删除了它们，取而代之的是一个叫做tofrodos的dpkg包（tofrodos没有拼写错误，不是tofromdos）。</div>
<div>
</div>
<div>首先安装tofrodos</div>
<div>$ sudo apt-get install tofrodos</div>
<div>查看tofrodos提供的可用命令</div>
<div>$ dpkg -L tofrodos</div>
<div>

* * *

/.

/usr
/usr/share
/usr/share/doc
/usr/share/doc/tofrodos
/usr/share/doc/tofrodos/readme.txt.gz
/usr/share/doc/tofrodos/copyright
/usr/share/doc/tofrodos/NEWS.Debian.gz
/usr/share/doc/tofrodos/changelog.Debian.gz
/usr/share/doc/tofrodos/tofrodos.html
/usr/share/man
/usr/share/man/man1
/usr/share/man/man1/fromdos.1.gz
/usr/bin
/usr/bin/fromdos
/usr/share/man/man1/todos.1.gz
/usr/bin/todos

* * *

</div>
<div>可以看出提供了todos和fromdos两个命令，todos相当于unix2dos，fromdos相当于dos2unix。</div>
<div>执行方法为todos filename， fromdos filename</div>
<div>
</div>
<div>如果依然怀念dos2unix和unix2dos命令，可以去创建两个link</div>
<div>

* * *
</div>
<div>$ cd /usr/bin/</div>
<div>$ sudo ln -s todos unix2dos</div>
<div>$ sudo ln -s fromdos dos2unix</div>
<div>
</div>
<div>

* * *
</div>
<div>拓展阅读：</div>
<div>
</div>
<div>[http://blog.163.com/xiaowei_090513/blog/static/117718359201091393735295/](http://blog.163.com/xiaowei_090513/blog/static/117718359201091393735295/)</div>
