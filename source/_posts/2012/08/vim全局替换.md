title: vim 全局替换
tags:
- vim
date: "2012-08-14 06:00:24"
---

<div>

语法为 :[addr]s/源字符串/目的字符串/[option]
全局替换命令为：:%s/源字符串/目的字符串/g

<div>
**[addr] 表示检索范围，省略时表示当前行。**
如：“1，20” ：表示从第1行到20行；
“%” ：表示整个文件，同“1,$”；
“. ,$” ：从当前行到文件尾；
s : 表示替换操作</div>
<div>
**[option] : 表示操作类型**
如：g 表示全局替换; 
c 表示进行确认
p 表示替代结果逐行显示（Ctrl + L恢复屏幕）；
省略option时仅对每行第一个匹配串进行替换；
如果在源字符串和目的字符串中出现特殊字符，需要用””转义</div>
<div>
**下面是一些例子：**
#将That or this 换成 This or that
:%s/(That) or (this)/u2 or l1/
—- 
#将句尾的child换成children
:%s/child([ ,.;!:?])/children1/g</div>
<div>—-</div>
</div>
<div>
</div>
<div>#删除行末的空格</div>
<div>:%s/ *$//g</div>
<div>
</div>
<div>
#将mgi/r/abox换成mgi/r/asquare
:g/mg([ira])box/s//mg//my1square/g    &lt;=&gt;  :g/mg[ira]box/s/box/square/g
—-
#将多个空格换成一个空格
:%s/  */ /g
—-
#使用空格替换句号或者冒号后面的一个或者多个空格
:%s/([:.])  */1 /g
—-
#删除所有空行
:g/^$/d
—-
#删除所有的空白行和空行
:g/^[  ][  ]*$/d
—-
#在每行的开始插入两个空白
:%s/^/&gt;  /
—-
#在接下来的6行末尾加入.
:.,5/$/./
—-
#颠倒文件的行序
:g/.*/m0O  &lt;=&gt; :g/^/m0O
—-
#寻找不是数字的开始行,并将其移到文件尾部
:g!/^[0-9]/m$ &lt;=&gt; g/^[^0-9]/m$
—-
#将文件的第12到17行内容复制10词放到当前文件的尾部
:1,10g/^/12,17t$
~~~~重复次数的作用
—-
#将chapter开始行下面的第二行的内容写道begin文件中
:g/^chapter/.+2w&gt;&gt;begin
—-
:/^part2/,/^part3/g/^chapter/.+2w&gt;&gt;begin
—-
:/^part2/,/^part3/g/^chapter/.+2w&gt;&gt;begin|+t$</div>
<div>
</div>
<div>
</div>
<div>
<div>
<div>

### <span>VI全局替换(转)</span>  

<span><span>2008-12-15 09:33:21</span><span>|  分类：</span> [Emacs/VIM](http://blog.163.com/vic_kk/blog/#m=0&amp;t=1&amp;c=fks_084067092087089071085095094095080095085068084083084071 "Emacs/VIM")</span><span><span>|</span><span>字号</span></span><span><span> </span><a>订阅</a></span>

</div>
</div>
<div>
</div>
<div>
</div>
<div>

<span style="font-size: small"><span lang="EN-US">[</span><span>翻译</span><span lang="EN-US">]</span></span>

<span style="font-size: small">_<span lang="EN-US">&quot; substitution</span>_</span>

<span style="font-size: small">_<span lang="EN-US">&quot;</span>__<span>替换</span>_</span>

<span style="font-size: small"><span lang="EN-US">:%s/fred/joe/igc            _: general substitute command_</span></span>

<span style="font-size: small"><span lang="EN-US">                            </span>_<span>普通替换命令</span>_</span>

<span style="font-size: small"><span lang="EN-US">[</span><span>注解</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">                                                                                                    :help :s</span></span>

<span style="font-size: small"><span lang="EN-US">                                                                                                    :help :s_flags</span></span>

<span style="font-size: small"><span lang="EN-US">                                                                                                    :help 10.3</span></span>

<span style="font-size: small"><span>将文件中所有的</span><span lang="EN-US">fred</span><span>替换为</span><span lang="EN-US">joe</span><span>，替换时忽略</span><span lang="EN-US">fred</span><span>的大小写，一行中有多个匹配时，对每一个都进行替换，替换前提示。</span></span>

<span style="font-size: small"><span>这是最简单的替换命令，需要了解</span><span lang="EN-US">:s</span><span>命令的标志位，例如“</span><span lang="EN-US">i</span><span>”代表忽略大小写，“</span><span lang="EN-US">g</span><span>”代表对行内所有匹配都做替换，“</span><span lang="EN-US">c</span><span>”代表替换前提示。</span></span>

<span style="font-size: small"><span>“</span><span lang="EN-US">%</span><span>”字符表示在文件范围内进行替换，它相当于“</span><span lang="EN-US">1,$</span><span>”，即从第一行替换到最后一行。</span></span>

<span style="font-size: small"><span lang="EN-US"> </span></span>

<span style="font-size: small"><span lang="EN-US">[</span><span>翻译</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">:%s//joe/igc                _: Substitute what you last searched for *N*_</span></span>

<span style="font-size: small"><span lang="EN-US">                            </span>_<span>替换你最后查找的内容</span>_</span>

<span style="font-size: small"><span lang="EN-US"> [</span><span>注解</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span>当“</span><span lang="EN-US">:s</span><span>”命令中的查找模式字串为空时，会使用你最后一次使用查找命令所查找的字符串作为查找模式字串，也就是说，会替换你最后一次查找的内容。查找的内容可以是你使用“</span><span lang="EN-US">/</span><span>”或“</span><span lang="EN-US">?</span><span>”命令所查找的，也可以是上一次“</span><span lang="EN-US">:s</span><span>”命令的查找模式，或者是“</span><span lang="EN-US">:global</span><span>”命令的查找模式。</span></span>

<span style="font-size: small"><span>上面的命令是把你最后一次查找的内容，替换为</span><span lang="EN-US">joe</span><span>。</span></span>

<span style="font-size: small"><span lang="EN-US"> </span></span>

<span style="font-size: small"><span lang="EN-US">[</span><span>翻译</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">:%s/~/sue/igc               _: Substitute your last replacement string for *N*_</span></span>

<span style="font-size: small"><span lang="EN-US">                            </span>_<span>替换你最后替换的字符串</span>_</span>

<span style="font-size: small"><span lang="EN-US">[</span><span>注解</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">                                                                                                    :help /~</span></span>

<span style="font-size: small"><span>当“</span><span lang="EN-US">:s</span><span>”命令中的查找模式字串为“</span><span lang="EN-US">~</span><span>”时，会替换你最后替换的字符串。例如，上次你执行了“</span><span lang="EN-US">:%s/abc/</span><span lang="EN-US">joe</span><span lang="EN-US">/igc</span><span>”命令，那么上面的命令相当于“</span><span lang="EN-US">:%s/</span><span lang="EN-US">joe</span><span lang="EN-US">/sue/igc</span><span>”。</span></span>

<span style="font-size: small"><span lang="EN-US"> </span></span>

<span style="font-size: small"><span lang="EN-US">[</span><span>翻译</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">:%s/r//g                   _: Delete DOS returns ^M_</span></span>

<span style="font-size: small"><span lang="EN-US">                            </span>_<span>删除</span><span lang="EN-US"> DOS </span>__<span>回车符</span><span lang="EN-US"> ^M</span>_</span>

<span style="font-size: small"><span lang="EN-US">[</span><span>注解</span><span lang="EN-US">]</span></span>

<span style="font-size: small"><span lang="EN-US">                                                                                                    :help /r</span></span>

<span style="font-size: small"><span>有时打开</span><span lang="EN-US">DOS</span><span>文件时，会发现第行结尾都有一个</span><span lang="EN-US">^M</span><span>，这么命令就是用来删除</span><span lang="EN-US">^M</span><span>的。</span></span>

<span style="font-size: x-small">
</span>

</div>
</div>
