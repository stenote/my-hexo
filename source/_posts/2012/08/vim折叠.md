title: vim 折叠
tags:
- vim
date: "2012-08-19 21:13:37"
---

<div>为了便于代码查看，vim提供了折叠功能。这样可以让用户在使用vim查看、编辑代码的时候提高效率。</div>
<div>
</div>
<div>vim中通过不同的“标识符”进行设定。通常不用进行修改，使用默认设置即可。</div>
<div>设定折叠标识符使用如下命令：</div>
<div>
</div>
<div><span>可用选项 'foldmethod' 来设定折叠方式：set fdm=*****，也可使用set foldmethod=******进行设定。</span></div>
<div>
<span>有 6 种方法来选定折叠：</span>
<span>manual           手工定义折叠</span>
<span>indent             更多的缩进表示更高级别的折叠</span>
<span>expr                用表达式来定义折叠</span>
<span>syntax             用语法高亮来定义折叠</span>
<span>diff                  对没有更改的文本进行折叠</span>
<span>marker            对文中的标志折叠</span>
<span>注意，每一种折叠方式不兼容，如不能即用expr又用marker方式，我主要轮流使用indent和marker方式进行折叠。</span></div>
<div>系统默认使用的折叠符为marker 。其他5中方法自行进行尝试</div>
<div>
</div>
<div>vim中折叠使用的命令通常用z进行开头。</div>
<div>
</div>
<div>常用命令如下：</div>
<div>
</div>
<div style="margin-left: 30px">zf 创建一个折叠，f的意思可能是fold，同时折叠</div>
<div style="margin-left: 30px">zo 打开一个折叠，但是不删除折叠标识符    //o为open的意思</div>
<div style="margin-left: 60px">zO打开多个折叠，通常用于选定范围后进行打开，如果折叠上还有折叠，那么都会被一次性打开</div>
<div style="margin-left: 60px">zr zR 和zO的执行一样。r表示read，读取。相当与打开</div>
<div style="margin-left: 30px">zc 关闭增加了折叠标识符的代码                  //c为close的意思</div>
<div style="margin-left: 60px">zC关闭多个折叠，通常也用于选定范围后进行关闭</div>
<div style="margin-left: 30px">zd 删除已存在的折叠标识符                          //d为delete的意思</div>
<div style="margin-left: 60px">注意： 如果光标所在行有若干个折叠，则删除最外层的折叠，如果是选定范围后zd，那么范围内的折叠都会被删除</div>
<div style="margin-left: 30px">zD 和zd实际操作效果一样。没有区别</div>
<div style="margin-left: 30px">zE 不用选定，删除所有折叠标识符</div>
<div style="margin-left: 60px">相当于1gg;shift + v ; G; zd;</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">zj 移动到下一个折叠点</div>
<div style="margin-left: 30px">zk 移动到上一个折叠点</div>
<div style="margin-left: 30px">[z 移动到当前打开的折叠的开头</div>
<div style="margin-left: 30px">]z 移动到当前打开的折叠的末尾</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">通常使用如上命令：</div>
<div style="margin-left: 30px"></div>
<div style="margin-left: 30px">其他命令：</div>
<div style="margin-left: 30px"><span>zf56G，创建从当前行起到56行的代码折叠；</span>
<span>10zf或10zf+或zf10↓，创建从当前行起到后10行的代码折叠。</span>
<span>10zf-或zf10↑，创建从当前行起到之前10行的代码折叠。</span>
<span>在括号处zf%，创建从当前行起到对应的匹配的括号上去（（），{}，[]，&lt;&gt;等）。</span></div>
<div>
</div>
<div>注意：执行开启、关闭折叠代码的时候，对折叠标识符不会进行删除，那么这个时候如果使用u或者ctrl+r进行撤销、前进，可能会导致折叠代码标识符被删除、增加。可以理解为，开启、关闭折叠代码和撤销、前进没有关联。</div>
<div>
</div>
