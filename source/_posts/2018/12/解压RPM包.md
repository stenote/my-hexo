title: 解压RPM包
date: 2018-12-31 23:51:40
tags:
  - Linux
---

不安装 RPM 包的情况下解压出包中的内容

```
rpm2cpio ./packagecloud-test-1.1-1.x86_64.rpm | cpio -idmv
```