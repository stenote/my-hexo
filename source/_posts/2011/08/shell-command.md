title: Shell基本函数（1）
tags:
- Linux
date: "2011-08-16 20:26:15"
---

<pre class="brush: bash">
#echo 基本的输出函数，和php中的echo相同。
#shell中变量赋值为name=123等类似方法。但是调用的时候都需要使用$name。
#重新赋值时候用name即可
~$: name=hello
~$: echo $name
hello
~$: name=world
~$: echo $name
world

#直接在变量后增加数值进行字符串连接
~$: name=test
~$: echo $name
test
~$: name=${name}hello
~$: echo $name
testhello
~$: name=&quot;$name&quot;world
~$: echo $name
testhelloworld

#export 用来将一个变量设置成“全局变量”
~$: name=helloworld
~$: echo $name
helloworld
~$: bash
~$: echo $name

~$: exit
~$: echo $name
helloworld
~$: export $name
~$: bash
~$: echo $name
helloworld

#env为英文enviorment的简写，用来显示当前系统中的环境变量
~$: env
#会显示很多系统环境变量，包括
#HOME USERNAME PWD LANG GDM_LANG等信息。
#这些信息可直接使用echo 输出。例如：
~$: echo $HOME
/home/may

#read函数主要用于对变量赋值（通过用户输入）read有两个选项，-p -t
#-p 可能是point out的意思，具体不清楚。主要用于提示信息显示
#-t 顾名思义为time，时间设定，超过时间没有回车，则为空
#read 选项后直接跟参数，再跟选项、参数，最后跟需要赋值的变量名
~$: read -p &#039;please keyin something&#039; -t 30 name
#这个时候输入hello 回车，再输出name，则显示hello
~$: echo $name
hello

</pre>
