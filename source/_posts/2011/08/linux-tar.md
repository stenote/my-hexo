title: 文件压缩之Tar
tags:
- Linux
date: "2011-08-15 20:19:24"
---

## 简单说明

`tar` 主要是将多个文件打包成一个文件。打包完成后再使用 `bzip2`、`gzip` 等其他软件进行压缩

tar 常用的参数包括 :

* `-c` 打包
* `-v` 查看信息
* `-x` 解打包，可理解为解压缩
* `-j`，同时使用bzip2 压缩/解压缩
* `-z` 使用gzip压缩/解压缩
* `-f` 要处理的文件filename 建议单独放到后面使用
* `-C` 解压缩时指向特定目录,注意，只是解压缩时使用

## 使用举例

### 将 `hello.php` 打包为 `hello.php.tar`

```
tar -cf hello.php.tar hello.php
```

### 将刚压缩的 `hello.php.tar`

```
tar -x -f hello.php.tar
```

### 将 `test` 目录打包为 `test.tar`

```
tar -c -f test.tar test/
```

### 将压缩的 `test.tar` 目录解压

```
tar -x -f test.tar
```

### 压缩目录或文件为 `bzip2` 的格式

```
tar -cj -f hello.php.tar.bz2 hello.php
tar -cj -f hello.tar.bz2 hello/
```

### 对刚压缩的后缀为 `tar.bz2` 的文件进行解压缩

```
tar -xj -f hello.php.tar.bz2
```

### 压缩目录或者文件为 `gzip` 的格式

```
tar -cz -f index.php.tar.gz index.php
tar -cz -f index.tar.gz index/
```

### 对刚压缩的 `tar.gz` 的文件进行解压缩

```
tar -xz -f index.php.tar.gz
tar -xz -f index.tar.gz
```

### 压缩时或者解压时增加 `-v` 参数显示压缩/解压缩关系到的所有文件

```
tar -zxv -f index.php.tar.gz
tar -zxv -f index.tar.gz
tar -czv -f index.php.tar.gz index.php
tar -czv -f index.php.tar.gz index/
tar -cjv -f hello.php.tar.bz2 hello.php
tar -cjv -f hello.tar.bz2 hello/
tar -jxv -f hello.tar.bz2
tar -jxv -f hello.php.tar.bz2
```

### 解压一个文件到 `/home/may/`

```
tar -zxv -f index.php.tar.gz -C /home/may
tar -jxv -f index.php.tar.bz2 -C /home/may
```
