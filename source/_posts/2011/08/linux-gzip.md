title: gzip
tags:
- Linux
date: "2011-08-11 19:53:15"
---

## 安装

```
sudo apt-get install gzip
# 默认系统已经安装, 如果未安装可使用上述命令安装
```

## 使用说明

`gzip` 常用选项包括`-c`、`-d`、`-#`

其他 `-v` 等参数可以不记


## 使用举例

### 压缩一文件并接重命名

```
gzip -c index.php > index.php.gz
```

### 自动压缩为原文件名后带.gz的压缩文件

```
gzip index.php
```

### 解压缩文件

```
gzip -d index.php.gz
```

### 解压缩并重命名

```
gzip -cd index.php.gz > hello.php
```

### #参数进行等级压缩，1 最快，9 最慢，数值越高压缩比例越好，默认为 6

```
gzip -9 index.php.gz
```

### 压缩时候使用 -v 参数查看压缩比例

```
gzip -9v index.php
```

页面显示

```
index.php:	-33.3% -- replaced with index.php.gz
```

### zcat 查看

```
# 先压缩 hello.php
gzip -9 hello.php
# 使用 zcat 对压缩后的 gz 文件进行查看
zcat hello.php.gz
# 页面显示文件内容
```
