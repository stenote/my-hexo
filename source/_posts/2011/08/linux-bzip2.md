title: 文件压缩之bzip2
tags:
- Linux
date: "2011-08-15 00:08:16"
---

## 安装bzip2

```
sudo apt-get install bzip2
```

bzip2常用选项包括 `-c`、 `-d`、 `-#`、 `-v`、 `-k`、 `-z` 

* `-c` 文件流量定向
* `-d` 解压缩
* `-#` 压缩比例设置
* `-v` 查看压缩比率
* `-k` 保持源文件存在
* `-z` 压缩

## 简单实用

### 直接对一个文件进行压缩

```
bzip2 hello.php
bzip2 -z hello.php
```

上述两命令等同。
原有的 `hello.php` 被压缩为 `hello.php.bz2`，并且删除原文件。

### 压缩一文件并接重命名

```
bzip2 -c index.php > index.php.bz2
```

原文件存在，并且新产生压缩的文件 `index.php.bz2`

### 解压缩文件

```
bzip2 -d index.php.bz2
```

压缩文件被解压为压缩前的状态，并且原压缩文件不存在。

### 解压缩并进行压缩文件重命名

```
bzip2 -cd index.php.bz2 > index.hello
```

解压缩 `index.php.bz2` 文件为 `index.hello`
并且 `index.php.bz2` 文件不被删除

### 进行等级压缩，`1` 最快，`9` 最慢，数值越高压缩比例越好，默认为 `6`

```
bzip2 -9 index.php
```

压缩时候还可以使用-v参数查看压缩比例

```
bzip -9v index.php
```

页面显示

```
index.php:  0.231:1, 34.667 bits/byte, 
-333.33% saved, 12 in, 52 out.
```

### zcat 可以直接查看 bzip2 压缩文件内容


1. 进行压缩
```
bzip2 -9 hello.php
```

2. 查看

```
zcat hello.php.bz2
```