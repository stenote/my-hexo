title: 文件压缩之Compress
tags:
- Linux
date: "2011-08-10 23:44:40"
---

## 安装

```
apt-get install ncompress
```

## 简单说明

comprss 有常用的几个选项: `-d`、`-c`、 `-f`

* `-d` 用来进行对一个 compress 压缩的文件进行解压缩
* `-c` 用来对一个文件进行 compress 的压缩
* `-f` 用来强制对文件进行压缩，压缩后的文件强制替换现有的已经压缩成的文件

## 使用实例

### 压缩以为 `hellp.php` 的文件为 `hello.php.Z`

```
compress -c hello.php > hello.php.Z
或
compress -f hello.php
```

### 解压缩 `hello.php.Z` 的文件

```
compress -d hello.php.Z
# 或者
uncompress hello.php.Z
```
