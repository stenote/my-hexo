title: 列表
date: 2017-06-27 22:12:38
---

## 未分类词条

* [浮式仪器平台](https://zh.wikipedia.org/wiki/%E6%B5%AE%E5%BC%8F%E5%84%80%E5%99%A8%E5%B9%B3%E5%8F%B0)
* [熊本熊](https://zh.wikipedia.org/wiki/%E7%86%8A%E6%9C%AC%E7%86%8A)
* [鸸鹋](https://zh.wikipedia.org/wiki/%E9%B8%B8%E9%B9%8B)
* [象鼩](https://zh.wikipedia.org/wiki/%E8%B1%A1%E9%BC%A9)
* [长耳跳鼠](http://baike.baidu.com/view/1019756.htm)
* [细尾獴/狐獴 (丁满)](https://zh.wikipedia.org/wiki/%E7%8B%90%E7%8D%B4)
* [浣熊 (干脆面)](https://zh.wikipedia.org/wiki/%E6%B5%A3%E7%86%8A)
* [水獭 (读 shuǐ tǎ)](https://zh.wikipedia.org/wiki/%E6%B0%B4%E7%8D%BA)
* [卡祖笛](https://zh.wikipedia.org/wiki/%E5%8D%A1%E7%A5%96%E7%AC%9B)
* [斯皮克斯金刚鹦鹉](https://zh.wikipedia.org/wiki/%E6%96%AF%E7%9A%AE%E5%85%8B%E6%96%AF%E9%87%91%E5%89%9B%E9%B8%9A%E9%B5%A1)
* [科拉超深钻孔](http://baike.baidu.com/view/3923108.htm)
* [科米蛙](http://baike.baidu.com/view/1151627.htm)

## 一些小众的 vps

* [conoha.jp](https://www.conoha.jp/zh) 日本 vps, 支持支付宝付款, SSD 硬盘, 1G RAM, 支持支付宝
* [alpharacks](https://www.alpharacks.com) 美国 vps, 适用于翻墙, 年付 3.99$, 只能用信用卡
* [bandwagonghost](https://bandwagonhost.com) 美国 vps, 专业翻墙 vps(自己提供 shadowsocks 和 openvpn 搭建服务), 支持支付宝
* [NyAvm](https://my.nyavm.com/index.php) 新加坡的 VPS 提供商, 联通/移动线路非常优秀!
* [XhostFire](https://www.xhostfire.com) 俄罗斯的 VPS 提供商，联通线路非常优秀！不绕道，ping 值可到 100ms 左右
* [mightweb.net](http://www.zhujiceping.com/18835.html) 达拉斯 VPS 提供商
* [ultravps.eu](http://www.zhujiceping.com/17560.html) 欧洲 VPS 提供商

----

* [vultr.com](https://www.vultr.com) 日本线路丢包严重，某些情况下会到绕到美国

## 计算机 Wiki

* [PTR记录](http://baike.baidu.com/view/2637324.htm)

## 好玩的建筑

* [罗德岛太阳神巨像](https://zh.wikipedia.org/wiki/%E7%BE%85%E5%BE%97%E5%B3%B6%E5%A4%AA%E9%99%BD%E7%A5%9E%E9%8A%85%E5%83%8F)

## 好玩的电影

* [独裁者](http://movie.douban.com/subject/4845728/)
* [波拉特](http://movie.douban.com/subject/1870044/)

你也可以看我和 merlin 的 airtable 分享列表：[airtable](https://airtable.com/shrOZke1DriAMo8fi) (请自带梯子)

## 人物、历史

* [朝鲜战争](https://zh.wikipedia.org/wiki/%E6%9C%9D%E9%B2%9C%E6%88%98%E4%BA%89)
* [盛田昭夫](https://zh.wikipedia.org/wiki/%E7%9B%9B%E7%94%B0%E6%98%AD%E5%A4%AB)
* [宫本武藏](https://zh.wikipedia.org/wiki/%E5%AE%AE%E6%9C%AC%E6%AD%A6%E8%97%8F)
* [真田广之](https://zh.wikipedia.org/wiki/%E7%9C%9F%E7%94%B0%E5%BB%A3%E4%B9%8B)
* [松下幸之助](https://zh.wikipedia.org/wiki/%E6%9D%BE%E4%B8%8B%E5%B9%B8%E4%B9%8B%E5%8A%A9)
* [廣末涼子](https://zh.wikipedia.org/wiki/%E5%BB%A3%E6%9C%AB%E6%B6%BC%E5%AD%90)
* [菊地凛子](https://zh.wikipedia.org/wiki/%E8%8F%8A%E5%9C%B0%E5%87%9C%E5%AD%90)
* [希波克拉底](https://zh.wikipedia.org/wiki/%E5%B8%8C%E6%B3%A2%E5%85%8B%E6%8B%89%E5%BA%95)
* [伊娃格林](https://zh.wikipedia.org/wiki/%E4%BC%8A%E5%A8%83%C2%B7%E6%A0%BC%E8%93%AE)
* [绫濑遥](https://zh.wikipedia.org/wiki/%E7%B6%BE%E7%80%A8%E9%81%99)
* [麦斯·米科尔森 Mads Mikkelsen](https://movie.douban.com/celebrity/1040529/)
* [切瓦特·埃加福特 Chiwetel Ejiofor](https://movie.douban.com/celebrity/1010581/)
* [本尼迪克特·王 Benedict Wong](https://movie.douban.com/celebrity/1301179/)
