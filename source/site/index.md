title: About This Site
date: 2015-01-25 22:37:00
---

文章归档中由于脚本问题出现 "1999年十一月", 但不影响阅读
网站使用了[digitalocean](http://www.digitalocean.com) 的 VPS , 建议购买和使用.

---

以上为 **2014** 年 **12** 月 前的 **About ME**
以下为 **2015** 年 **1** 月 更新

页面使用了 **[hexo](http://hexo.io)** 进行部署、发布, 主题使用了 **[Yilia](https://github.com/litten/hexo-theme-yilia)** 
暂时挂靠在 **[coding.net](http://coding.net)** 上

联系我, 请发送邮件到 [xjmarui@gmail.com](mailto:xjmarui@gmail.com)
